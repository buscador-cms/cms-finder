﻿<%@ Application Language="C#" %>
<script runat="server">
    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
    }

    protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
    {
        if (FormsAuthentication.CookiesSupported == true)
        {
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                try
                {
                    string uname = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                    string role = GetUserRoles(uname);
                    e.User = new System.Security.Principal.GenericPrincipal(
                        new System.Security.Principal.GenericIdentity(uname, "Forms"), role.Split(';')
                    );
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }
    }
    
    public string GetUserRoles(string uname)
    {
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection();
        SQLWrapper wrap = new SQLWrapper();
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
        cmd.Connection = wrap.Connect();
        StringBuilder msg = new StringBuilder();
        string r = "";
        
        try
        {
            cmd.CommandText = "SELECT AccountRoles FROM FinderSchema.Account WHERE AccountUser = @User";
            cmd.Parameters.Add("@User", System.Data.SqlDbType.VarChar).Value = uname;
            System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();
                r = reader.GetString(reader.GetOrdinal("AccountRoles"));
            }

            reader.Close();
        }
        catch (Exception e)
        {
            Response.Write(e.Message);
        }

        return r;
    }
</script>
