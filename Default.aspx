﻿<%@ Page Title="Búsqueda de Artículos y Quiénes Somos" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="SearchParser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Meta" Runat="Server">
    <meta name="description" content="Busque artículos publicados en Cuadernos Médico Sociales" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Stylesheets" Runat="Server">
    <script type="text/javascript" src="/js/jquery.cycle.all.js"></script>
    <style type="text/css">
        #container {
            width: 450px;
	        /*width: 4600px;/*5*920px ancho*/
            margin: 7px 0 0 0;
        }

        div#slideshow {
	        overflow: scroll; /* Permite que las slides se visualicen con el scrollbar si no hay Javascript disponible*/
            z-index: 5;
        }

        #slides img {
            height: 285px;
        }

        div#slideshow ul {
	        list-style: none;
        }

        #slideshow ul li {
            -webkit-margin-before: 0;
            -webkit-margin-after: 0;
        }

        #head-text {
            width: 430px;
            background-color: #444;
            color: white;
            margin: 0 0 0 0.8em;
        }

        #head-text p {
            -webkit-margin-before: 0;
            -webkit-margin-after: 0;
            font: bold 0.8em 'Lucida Sans Unicode';
            line-height: 1.3em;
            margin: 0.9em 0;
        }

</style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadScripts" Runat="Server">
    <script src="/Scripts/Buscar.js"></script>
    <script>
        $(document).ready(function () {
            $("#slideshow").css({
                overflow: 'hidden',
                height: 'auto'
            });

            $('#slideshow img').css('width', '450px');
		
		    $("ul#slides").cycle({
			    fx: 'fade',
			    pause: 1,
			    prev: '#prev',
			    next: '#next'
		    });

		    $("#slideshow").hover(function() {
			    $("ul#nav").fadeIn();
		    },
			    function() {
			    $("ul#nav").fadeOut();
		    });
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CPHBanner" Runat="Server">
    <div class="wrap">
        <div id="banner" class="cf">
            <div id="container" class="align-left">
                <div id="slideshow">
                    <ul id="slides">
                        <li><img src="/slider/1.jpg" /></li>
					    <li><img src="/slider/2.jpg" /></li>
					    <li><img src="/slider/3.jpg" /></li>
                        <li><img src="/slider/4.jpg" /></li>
                        <li><img src="/slider/5.jpg" /></li>
                        <li><img src="/slider/6.jpg" /></li>
                    </ul><!-- #slides -->
                </div> <!-- #slideshow -->
            </div><!-- #container -->
            <div id="head-text" class="align-left">
                <p>Cuadernos Médico Sociales es una revista de Salud Pública de más de 50 años de trayectoria, que tiene como objetivo estimular la reflexión y la investigación científica en el ámbito de la Salud Pública y de la Medicina Social, difundir temas relevantes a estas áreas del conocimiento, donde se integren quienes estudian o trabajan en las distintas disciplinas (ciencias naturales, biológicas, sociales y/o de la conducta) e instituciones relacionadas con la salud de la población. Hacer llegar a las autoridades, dirigentes, profesionales y estudiantes de postgrado aquellas experiencias, análisis y resultados de investigaciones que contribuyan a desarrollar una concepción integral de la salud y de la atención de salud.</p>
                <p>Se les invita a publicar con nosotros investigaciones y/o experiencias en torno a ámbitos específicos de la Salud Pública y de la Medicina Social que contribuyan a desarrollar una concepción de integra de la salud y de la Atención de Salud.</p>
                <p>¡Sean Bienvenidos!</p>
            </div><!-- #head-text -->
        </div><!-- #banner -->
    </div>
</asp:Content>

<asp:Content runat="server" id="SearchPageContent" ContentPlaceHolderID="MainContent">
    <section id="Finder" class="cf">
        <div class="align-left">
            <div>
                <h2 class="collapser text-left">¿Cómo Buscar?</h2>
                <div class="collapse-panel">
                    <% string port = Request["SERVER_PORT"] != "80" ? ":" + Request["SERVER_PORT"] : ""; %>
                    <p>Puede buscar ingresando un nombre de autor o el título del articulo que busca o haciendo clic sobre los filtros en la barra lateral derecha.</p>
                    <p>También puede buscar ingresando el término en la URL. Por ejemplo: <b><%= Request["SERVER_NAME"] + port %>/buscar/volumen/1/1994</b> mostrará el <i>volumen 1 del año 1994</i></p>
                </div>
            </div>
            <p>También puede revisar la lista completa por:</p>
            <ul class="filter-links">
                <li><a href="/lista/volumen/">Volumen</a></li>
                <li><a href="/lista/autor/">Autor</a></li>
                <!--<li><a href="/lista/titulo/">Palabra Clave</a></li>-->
            </ul>
            <h2 id="search-for"><span class="title"><%= (searchTitle.Length > 0 ? searchTitle : "Resultados de Búsqueda") %></span> <span><%=searchFor %></span></h2>
            <div id="results">
                <%= results %>
            </div><!-- #results -->
        </div><!-- .align-left -->
        <div class="align-right" id="search-sidebar">
            <h3>Buscar</h3>
            <span class="no-js-warning">Para poder realizar búsquedas por palabra clave, debe habilitar JavaScript.</span>
            <div id="search-box">
                <asp:TextBox id="SearchTxt" runat="server" ToolTip="Ingrese el nombre de un autor, el título o parte de la descripción o un término (palabra clave)" /><br />
                <asp:Button runat="server" id="SearchBtn" Text="Buscar" ToolTip="Comenzar la búsqueda" /><br />
                <asp:RadioButton ID="SearchInTitles" ToolTip="Marque esta casilla para buscar SÓLO en títulos (artículos)" runat="server" Checked="True" GroupName="SearchFilter" /> Palabra Clave<br />
                <asp:RadioButton ID="SearchInAuthors" ToolTip="Marque esta casilla para buscar SÓLO en autores" runat="server" GroupName="SearchFilter" /> Autores<br />
                <asp:RadioButton ID="SearchInKeywords" ToolTip="Marque esta casilla para buscar un término específico" runat="server" GroupName="SearchFilter" /> Término
            </div>
        </div><!-- .align-right -->
    </section>
</asp:Content>

