﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Cache
/// </summary>
public class CacheHelper
{
    public const string CACHE_FOLDER = "Cache";
    public const string VOLUME_CACHE_FILE = "volume_{0}-{1}.cache";
    public const string AUTHOR_CACHE_FILE = "author_{0}-{1}.cache";
    public const string ARTICLE_CACHE_FILE = "article_{0}-{1}.cache";
    public const string KEYWORD_CACHE_FILE = "keyword_{0}-{1}.cache";
    private string FolderPath = "";

	public CacheHelper()
	{
        FolderPath = HttpContext.Current.Server.MapPath(String.Format("~/{0}", CACHE_FOLDER));

        if (!Directory.Exists(FolderPath))
        {
            Directory.CreateDirectory(FolderPath);
        }
	}

    public string ReadCache(string file)
    {
        if (file != "")
        {
            string tmp = Path.Combine(FolderPath, file);

            if (File.Exists(tmp))
            {
                return File.ReadAllText(tmp);
            }
        }

        return "";
    }

    public void WriteCache(string relativePath, string content, bool append = false)
    {
        if (relativePath != "" && content != "")
        {
            string tmp = Path.Combine(FolderPath, relativePath);
            StreamWriter writer = new StreamWriter(tmp, append);
            writer.Write(content);
            writer.Close();
            writer.Dispose();
            tmp = null;
        }
    }

    public void DeleteCache(string filename)
    {
        if (filename != "")
        {
            string tmp = Path.Combine(FolderPath, filename);

            if (File.Exists(tmp))
                File.Delete(tmp);
        }
    }

    public void DeleteAllCache(string pattern = "")
    {
        IEnumerable<string> files = Directory.EnumerateFiles(FolderPath);

        if (files.Count() > 0)
        {
            foreach (string fpath in files)
            {
                string fname = Path.GetFileName(fpath);
                bool delete = true;

                if (pattern != "")
                    delete &= Regex.IsMatch(fname, pattern, RegexOptions.IgnoreCase);

                //throw new Exception(string.Format("fname: {0} - delete: {1} - pattern: {2}", fname, delete.ToString(), pattern));
                if (delete)
                {
                    DeleteCache(fname);
                }
            }
        }
    }
}