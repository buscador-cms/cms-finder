﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Paginate
/// </summary>
public class Paginate
{
    public const string PARAM_NAME = "page";
    private int PageRange = 3;
    private static int LeftRange = 0;
    private static int RightRange = 0;
    private static int Rpp = 0;
    private int ResultCount = 0;
    private int PageCount = 0;
    private StringBuilder Html = new StringBuilder();
    private bool JumpPageControls;

    public int ResultsPerPage
    {
        get
        {
            return Rpp;
        }
    }

    public String Pagination
    {
        get
        {
            return Html.ToString();
        }
    }

    public int MaxPages
    {
        get
        {
            return PageCount;
        }
    }

    public int CurrentPage { get; set; }
    public string URLPathFormat { get; set; }

	public Paginate(int resultCount, int currentPage = 1, string urlFormat = "", bool addJumpPage = false)
	{
        JumpPageControls = addJumpPage;

        if (Rpp == 0)
        {
            string rpp = Helper.GetPreference("results_per_page");

            if (rpp.Length > 0 && Helper.IsNumeric(rpp))
                Rpp = int.Parse(rpp);
            else
                Rpp = 15;
        }

        ResultCount = resultCount;

        if (currentPage > 0)
        {
            CurrentPage = currentPage;
        }
        else
        {
            CurrentPage = 1;
        }

        PageCount = (ResultCount + Rpp) / Rpp;

        if (CurrentPage > PageCount)
            CurrentPage = PageCount;

        URLPathFormat = string.Format("?{0}={{0}}", PARAM_NAME);

        if (urlFormat.Length > 0)
        {
            URLPathFormat = urlFormat;
        }

        if (PageCount >= 2)
        {
            Generate();
        }
	}

    private void Generate()
    {
        Html.Append("<div class='pagination'>");

        if (LeftRange == 0 || RightRange == 0 || (CurrentPage) <= LeftRange || (CurrentPage) >= RightRange)
        {
            LeftRange = CurrentPage - PageRange;
            RightRange = CurrentPage + PageRange;

            if (LeftRange <= 0)
                LeftRange = 1;

            if (RightRange >= MaxPages)
                RightRange = MaxPages;
        }

        int start = LeftRange;
        int end = RightRange;
        string pnLinkFormat = "<a class='pagination-link {3}' href='{0}' title='{2}'>{1}</a>";

        if (CurrentPage > 1)
        {
            Html.AppendFormat(pnLinkFormat,
                string.Format(URLPathFormat, (CurrentPage - 1)),
                "&lt;&lt;",
                string.Format("Ir a la página anterior ({0})", (CurrentPage - 1)),
                "prev-link");
        }
        else
        {
            Html.AppendFormat("<span class='html-link prev-link' title='No hay páginas previas'>{0}</span>", "&lt;&lt;");
        }

        for (int i = start; i <= end; i++)
        {
            if (i == CurrentPage)
            {
                Html.AppendFormat("<span class='pagination-link current-pagination-link' title='Página actual'>[{0}]</span>", i);
            }
            else
            {
                Html.AppendFormat("<a href='{0}' title='Ir a la página {1}' class='pagination-link'>{1}</a>",
                    string.Format(URLPathFormat, i),
                    i);
            }
        }

        if (CurrentPage < PageCount)
        {
            Html.AppendFormat(pnLinkFormat,
                String.Format(URLPathFormat, (CurrentPage + 1)),
                "&gt;&gt;",
                String.Format("Ir a la página siguiente ({0})", (CurrentPage + 1)),
                "next-link");
        }
        else
        {
            Html.AppendFormat("<span class='next-link pagination-link' title='No hay página siguiente'>{0}</span>", "&gt;&gt;");
        }

        if(MaxPages >= 6 && JumpPageControls)
            Html.AppendFormat("<p class='page-jump'>Ir a la página <input type='number' min='1' max='{0}' title='Ingrese un número entre 1 y {0} y luego presione el botón \"Ir\"' /><input type='submit' class='page-jump-submit' value='Ir' title='Saltar a la página ingresada' /> de <span>{0}</span>.</p>", MaxPages);

        Html.Append("</div>");
    }

    public List<T> LimitResults<T>(List<T> list)
    {
        int count = list.Count;
        if (list.Count > Rpp)
        {
            int offset = (CurrentPage - 1) * Rpp;
            int limit = Rpp;

            if (!(count > (offset + Rpp)))
            {
                limit = count - offset;
            }

            return list.GetRange(offset, limit);
        }

        return list;
    }

    public IEnumerable<T> LimitResults<T>(IEnumerable<T> results)
    {
        int offset = (CurrentPage - 1) * Rpp;
        int limit = Rpp;

        if (!(MaxPages > (offset + Rpp)))
        {
            limit = MaxPages - offset;
        }

        if (offset < 0)
            offset = 0;

        return results.Skip(offset).Take(limit);
    }
}