﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SearchHelper
/// </summary>
public class SearchHelper
{
    public RequestedAction Action
    {
        get;
        set;
    }

    public RequestedItem Item
    {
        get;
        set;
    }

    public string Query
    {
        get;
        set;
    }

    public Order Sort
    {
        get;
        set;
    }

    public string OrderBy
    {
        get;
        set;
    }

	public SearchHelper(RequestedAction action, RequestedItem item = RequestedItem.NONE, string query = "", string orderby = "", Order sortOrder = Order.DESCENDING)
	{
        Action = action;
        Item = item;
        Query = query;
        OrderBy = orderby;
        Sort = sortOrder;
	}

    public static string Translate(RequestedAction action)
    {
        string t = "";

        switch (action)
        {
            case RequestedAction.LIST:
                t = "Lista";
                break;

            case RequestedAction.SHOW:
                t = "Mostrar";
                break;

            case RequestedAction.SEARCH:
                t = "Buscar";
                break;
        }

        return t;
    }

    public static string Translate(RequestedItem item)
    {
        string t = "";

        switch (item)
        {
            case RequestedItem.AUTHOR:
                t = "Autor";
                break;

            case RequestedItem.TITLE:
                t = "Título";
                break;

            case RequestedItem.VOLUME:
                t = "Volumen";
                break;
        }

        return t;
    }

    public enum Order
    {
        ASCENDING, DESCENDING
    }

    [Flags]
    public enum RequestedAction
    {
        SHOW, LIST, SEARCH
    }

    [Flags]
    public enum RequestedItem
    {
        NONE, AUTHOR, TITLE, VOLUME, MENU, KEYWORD
    }
}