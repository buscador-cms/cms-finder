﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for DBSearch
/// </summary>
public class DBSearch
{
    #region Class Properties

    private LinqToSql.FinderSchemaDataContext finder = new LinqToSql.FinderSchemaDataContext();
    private int page = 1;
    private string pagPrefix = "";
    private int pageCount = 0;
    private SearchHelper SHelper;

    public long MaxPages
    {
        get
        {
            return pageCount;
        }
    }

    #region ArticleFormat

    //private static string articleFormat = "<div class='article cf' id='article-{0}'>" +
    //        "<h4 class='article-title collapser'>{1}</h4><!-- .article-title -->" +
    //        "<div class='collapse-panel clear'>" +
    //            "<div class='article-meta'>" +
    //                "<p class='article-desc'>Descripción: {2}</p><!-- .article-desc -->" +
    //                "<p class='article-pcount'>Número de páginas: {3}</p>" +
    //                "<p class='article-authors'>Autores: {4}</p><!-- .article-authors -->" +
    //                "<p class='article-download'><a href='{5}' title='Haga clic aquí para descargar el artículo' class='art-download' target='_blank'>Descargar Artículo</a>" +
    //            "</div><!-- .article-meta -->" +
    //        "</div><!-- .collapse-panel -->" +
    //    "</div><!-- #article-{0} -->";

    private static string articleFormat = "<div class='article cf' id='article-{0}'>" +
            "<h4 class='article-title'><a href='{1}' title='Haga clic aquí para descargar este artículo' target='_blank'>{2}</a></h4><!-- .article-title -->" +
        "</div><!-- #article-{0} -->";

    //private static string authorContentFormat = "<div class='article' id='article-{0}'>" +
    //    "<h4 class='article-title collapser'>{1}</h4>" +
    //    "<div class='collapse-panel clear'>" +
    //        "<div class='article-meta'>" +
    //            "<p class='article-desc'>Descripción: {2}</p><!-- .article-desc -->" +
    //            "<p class='article-pcount'>Número de páginas: {3}</p>" +
    //            "<p class='article-download'><a href='{4}' title='Haga clic aquí para descargar el artículo' class='art-download' target='_blank'>Descargar Artículo</a><br ><a href='{5}' class='art-no-download' title='Haga clic aquí para descargar el número completo' target='_blank'>Descargar Número</a></p><!-- .article-download -->" +
    //        "</div><!-- .article-meta -->" +
    //    "</div><!-- .collapse-panel -->" +
    //"</div><!-- #article-{0} -->";

    #endregion ArticleFormat

    #endregion Class Properties

    #region Constructors
    public DBSearch(int page = 1)
	{
        this.page = page;
	}
    #endregion Constructors

    #region Class Members

    public string Search(SearchHelper helper, int page = 1)
    {
        if (helper == null)
            throw new NoNullAllowedException("SearchHelper cannot be null!");

        SHelper = helper;
        StringBuilder o = new StringBuilder();

        if (SHelper.Query.Length > 0)
            SHelper.Query = Helper.SanitizeString(SHelper.Query);

        if (page > 1)
            this.page = page;


        if (SHelper.Item != SearchHelper.RequestedItem.NONE)
        {
            pagPrefix = string.Format("/{0}/{1}/",
                SearchHelper.Translate(SHelper.Action).ToLower(),
                SearchHelper.Translate(SHelper.Item).ToLower());
        }
        else
        {
            pagPrefix = SearchHelper.Translate(SHelper.Action).ToLower();
        }

        if (SHelper.Query != "")
            pagPrefix = string.Format("{0}{1}/", pagPrefix, SHelper.Query);

        try
        {
            switch (SHelper.Action)
            {
                case SearchHelper.RequestedAction.LIST:
                    o.Append(ListQuery());
                    break;

                case SearchHelper.RequestedAction.SHOW:
                    o.Append(ShowQuery());
                    break;

                case SearchHelper.RequestedAction.SEARCH:
                    o.Append(SearchQuery());
                    break;

                default:
                    o.Append("<p class='warning'><i>Acción</i> desconocida...</p>");
                    break;
            }
        }
        catch (Exception e)
        {
            o.AppendFormat("<p class='error'>ocurrió un error al procesar la búsqueda: {0}</p>", e.Message);
        }

        return o.ToString();
    }

    private string ListQuery()
    {
        StringBuilder o = new StringBuilder();
        Paginate pages = null;
        string tmp = "",
            format = "{0}{1}{0}",
            fname = "",
            urlFormat = string.Format("{0}p{{0}}/", pagPrefix);
        CacheHelper ch = new CacheHelper();

        switch (SHelper.Item)
        {
            case SearchHelper.RequestedItem.AUTHOR:
                {
                    fname = string.Format(CacheHelper.AUTHOR_CACHE_FILE, page, "list");
                    tmp = ch.ReadCache(fname).Trim();

                    if (tmp == "")
                    {
                        List<LinqToSql.Author> l = FilterByAuthor();

                        if (l.Count > 0)
                        {
                            pages = new Paginate(l.Count, page, urlFormat, true);
                            l = pages.LimitResults(l);
                            o.AppendFormat(format, pages.Pagination, FormatByAuthor(l));
                            ch.WriteCache(fname, o.ToString());
                        }
                    }
                    else
                    {
                        o.Append(tmp);
                    }
                }
                break;

            case SearchHelper.RequestedItem.TITLE:
                {
                    fname = string.Format(CacheHelper.ARTICLE_CACHE_FILE, page, "list");
                    tmp = ch.ReadCache(fname).Trim();

                    if (tmp == "")
                    {
                        List<LinqToSql.Article> l = FilterByArticle();
						SHelper.Sort = SearchHelper.Order.ASCENDING;

                        if (l.Count > 0)
                        {
                            pages = new Paginate(l.Count, page, urlFormat, true);
                            l = pages.LimitResults(l);
                            o.AppendFormat(format, pages.Pagination, FormatByArticle(l));
                            ch.WriteCache(fname, o.ToString());
                        }
                    }
                    else
                    {
                        o.Append(tmp);
                    }
                }
                break;

            case SearchHelper.RequestedItem.VOLUME:
                {
                    SHelper.Sort = SearchHelper.Order.DESCENDING;
                    fname = string.Format(CacheHelper.VOLUME_CACHE_FILE, page, "volume");
                    tmp = ch.ReadCache(fname);

                    if (tmp == "")
                    {
                        List<LinqToSql.Magazine> l = FilterByMagazine();

                        if (l.Count > 0)
                        {
                            pages = new Paginate(l.Count, page, urlFormat, true);
                            l = pages.LimitResults(l);
                            o.AppendFormat(format, pages.Pagination, FormatByMagazine(l));
                            ch.WriteCache(fname, o.ToString());
                        }
                    }
                    else
                    {
                        o.Append(tmp);
                    }
                    //o.Append(Magazines());
                }
                break;

            default:
                o.Append("<p class='warning'><i>Lista</i> desconocida...</p>");
                break;
        }

        if (pages != null)
            pageCount = pages.MaxPages;

        return o.ToString();
    }

    private string ShowQuery()
    {
        StringBuilder o = new StringBuilder();
        string format = "{0}{1}{0}",
            tmp = "",
            fname = "",
            query = Regex.Replace(SHelper.Query, @"\s", "_"),
            urlFormat = string.Format("{0}p{{0}}/", pagPrefix);
        Paginate pages = null;
        CacheHelper ch = new CacheHelper();

        switch (SHelper.Item)
        {
            case SearchHelper.RequestedItem.AUTHOR:
                {
                    fname = string.Format(CacheHelper.AUTHOR_CACHE_FILE, page, query);
                    tmp = ch.ReadCache(fname).Trim();

                    if (tmp == "")
                    {
                        List<LinqToSql.Author> l = null;

                        if (SHelper.Query.Length == 1)
                            l = FilterByAuthor(SHelper.Query[0]);
                        else if (Helper.IsNumeric(SHelper.Query))
                            l = FilterByAuthor(long.Parse(SHelper.Query));
                        else
                            l = FilterByAuthor(SHelper.Query);

                        if (l.Count > 0)
                        {
                            pages = new Paginate(l.Count, page, urlFormat, true);
                            l = pages.LimitResults(l);
                            o.AppendFormat(format, pages.Pagination, FormatByAuthor(l));
                            ch.WriteCache(fname, o.ToString());
                        }
                    }
                    else
                    {
                        o.Append(tmp);
                    }
                }
                break;

            case SearchHelper.RequestedItem.TITLE:
                {
                    fname = string.Format(CacheHelper.ARTICLE_CACHE_FILE, page, query);
                    tmp = ch.ReadCache(fname).Trim();

                    if (tmp == "")
                    {
                        List<LinqToSql.Article> l = null;
						SHelper.Sort = SearchHelper.Order.ASCENDING;

                        if (Helper.IsNumeric(SHelper.Query))
                            l = FilterByArticle(int.Parse(SHelper.Query));
                        else if (SHelper.Query.Length == 1)
                            l = FilterByArticle(SHelper.Query[0]);
                        else
                            l = FilterByArticle(SHelper.Query);

                        if (l.Count > 0)
                        {
                            pages = new Paginate(l.Count, page, urlFormat, true);
                            l = pages.LimitResults(l);
                            o.AppendFormat(format, pages.Pagination, FormatByArticle(l));
                            ch.WriteCache(fname, o.ToString());
                        }
                    }
                    else
                    {
                        o.Append(tmp);
                    }
                }
                break;

            case SearchHelper.RequestedItem.VOLUME:
                {
                    fname = string.Format(CacheHelper.VOLUME_CACHE_FILE, page, query);
                    tmp = ch.ReadCache(fname).Trim();

                    if (tmp == "")
                    {
                        List<LinqToSql.Magazine> l = null;

                        if (Regex.IsMatch(SHelper.Query, @"^\d+_\d{4,}$"))
                        {
                            string[] split = SHelper.Query.Split('_');
                            l = FilterByMagazine(long.Parse(split[0]), int.Parse(split[1]));
                        }
                        else if (Regex.IsMatch(SHelper.Query, @"^\d{4,}$"))
                        {
                            l = FilterByMagazine(int.Parse(SHelper.Query));
                        }
                        else if (Helper.IsNumeric(SHelper.Query))
                        {
                            l = FilterByMagazine(long.Parse(SHelper.Query));
                        }

                        if (l.Count > 0)
                        {
                            pages = new Paginate(l.Count, page, urlFormat, true);
                            l = pages.LimitResults(l);
                            o.AppendFormat(format, pages.Pagination, FormatByMagazine(l));
                            ch.WriteCache(fname, o.ToString());
                        }
                    }
                    else
                    {
                        o.Append(tmp);
                    }
                }
                break;

            //case SearchHelper.RequestedItem.KEYWORD:
            //    {
            //        List<LinqToSql.Article> l = FilterByKeyword(SHelper.Query);

            //        if (l.Count > 0)
            //        {
            //            pages = new Paginate(l.Count, page, urlFormat, true);
            //            l = pages.LimitResults(l);
            //            o.AppendFormat(format, pages.Pagination, FormatByArticle(l));
            //        }
            //    }
            //    break;

            default:
                o.Append("<p class='warning'><i>Petición</i> desconocida...</p>");
                break;
        }

        return o.ToString();
    }

    private string SearchQuery()
    {
        StringBuilder o = new StringBuilder();

        if (SHelper.Item == SearchHelper.RequestedItem.KEYWORD)
        {
            CacheHelper ch = new CacheHelper();
            string query = Regex.Replace(SHelper.Query, @"\s", "_"),
                urlFormat = string.Format("{0}p{{0}}/", pagPrefix),
                fname = string.Format(CacheHelper.KEYWORD_CACHE_FILE, page, query),
                tmp = ch.ReadCache(fname);

            if (tmp == "")
            {
                List<LinqToSql.Article> l = FilterByKeyword(SHelper.Query);
                Paginate pages = null;

                if (l.Count > 0)
                {
                    pages = new Paginate(l.Count, page, urlFormat, true);
                    l = pages.LimitResults(l);
                    o.AppendFormat("{0}{1}{0}", pages.Pagination, FormatByArticle(l));
                    ch.WriteCache(fname, o.ToString());
                }
            }
            else
            {
                o.Append(tmp);
            }
        }
        else
        {
            o.Append("<p class='warning'><i>Petición</i> desconocida...");
        }

        return o.ToString();
    }

    /*public string Search(string type, string query)
    {
        if (query.Length <= 0)
            return "";

        string o = "";
        Regex reg;

        if (query.Length > 1)
            query = Helper.SanitizeString(query);

        type = type.ToLower();
        pagPrefix = String.Format("{0}/{1}/{2}", Helper.SEARCH_PREFIX, type, query);
        Paginate paginate = null;
        string pagination = "";

        try
        {
            switch (type)
            {
                case "autor":
                case "author":
                    List<LinqToSql.Author> authors = null;
                    reg = new Regex(@"^\d+$");

                    if (reg.IsMatch(query))
                    {
                        authors = FilterByAuthor(long.Parse(query));
                    }
                    else if (query.Length == 1)
                    {
                        authors = FilterByAuthor(query[0]);
                    }
                    else
                    {
                        authors = FilterByAuthor(query);
                    }

                    if (authors != null && authors.Count > 0)
                    {
                        int count = authors.Count;
                        paginate = new Paginate(count, page, string.Format("{0}/{{0}}", pagPrefix), true);
                        authors = paginate.LimitResults(authors);
                        pagination = paginate.Pagination;
                        o = pagination;
                        o += FormatByAuthor(authors);
                        o += pagination;
                        count = 0;
                    }

                    authors = null;
                    break;

                case "titulo":
                case "título":
                    List<LinqToSql.Article> titles = null;

                    if (query.Length == 1)
                    {
                        titles = FilterByArticle(query[0]);
                    }
                    else
                    {
                        titles = FilterByArticle(query);
                    }

                    if (titles != null && titles.Count > 0)
                    {
                        int count = titles.Count;
                        paginate = new Paginate(count, page, string.Format("{0}/{{0}}", pagPrefix), true);
                        titles = paginate.LimitResults(titles);
                        pagination = paginate.Pagination;
                        o = pagination;
                        o += FormatByArticle(titles);
                        o += pagination;
                        count = 0;
                    }

                    titles = null;

                    break;

                case "volumen":
                case "volume":
                    List<LinqToSql.Magazine> magazines = null;

                    if (Regex.IsMatch(query, @"\d+/\d+"))
                    {
                        string[] split = query.Split('/');
                        magazines = FilterByMagazine(long.Parse(split[0]), int.Parse(split[1]));
                    }

                    if (magazines != null && magazines.Count > 0)
                    {
                        int count = magazines.Count;
                        paginate = new Paginate(count, page, string.Format("{0}/{{0}}", pagPrefix), true);
                        magazines = paginate.LimitResults(magazines);
                        pagination = paginate.Pagination;
                        o = pagination;
                        o += FormatByMagazine(magazines);
                        o += pagination;
                        count = 0;
                    }

                    magazines = null;
                    break;

                case "full":
                    if (query.Length >= 3)
                    {
                        pagPrefix = String.Format("{0}/{1}", Helper.SEARCH_PREFIX, query);
                        List<LinqToSql.Article> mixed = FullSearch(query);

                        if (mixed != null && mixed.Count > 0)
                        {
                            int count = mixed.Count;
                            paginate = new Paginate(count, page, string.Format("{0}/{{0}}", pagPrefix), true);
                            mixed = paginate.LimitResults(mixed);
                            pagination = paginate.Pagination;
                            o = pagination;
                            o += FormatByArticle(mixed);
                            o += pagination;
                        }
                    }
                    else
                    {
                        o = "Por favor, ingrese mas de 3 caracteres al realizar una búsqueda.";
                    }
                    break;
            }
        }
        catch (Exception e)
        {
            o = String.Format("Ocurrió un error al procesar la petición: {0}", e.Message);
        }

        if (paginate != null)
            pageCount = paginate.MaxPages;

        return o;
    }*/

    public string Magazines()
    {
        StringBuilder htmlList = new StringBuilder();
        CacheHelper c = new CacheHelper();
        string fname = string.Format(CacheHelper.VOLUME_CACHE_FILE, page, "list");
        string content = c.ReadCache(fname);

        if (content == "")
        {
            List<LinqToSql.Magazine> magazines = (from mags in finder.Magazines
                                                      orderby mags.PublishYear descending
                                                      select mags).ToList();

            if (magazines.Count > 0)
            {
                Paginate p = new Paginate(magazines.Count(), page, "/lista/volumen/p{0}/", true);
                magazines = p.LimitResults(magazines);
                htmlList.Append(p.Pagination);

                foreach (LinqToSql.Magazine mview in magazines)
                {
                    htmlList.AppendFormat("<p><a href='/mostrar/volumen/{0}_{1}/' id='magazine-{0}-{1}' class='volume-list-item'>Volumen {0}, año {1}</a></p>", mview.MagazineNumber, mview.PublishYear);
                }

                htmlList.Append(p.Pagination);
                c.WriteCache(fname, htmlList.ToString());
            }
        }
        else
        {
            htmlList.Append(content);
        }

        return htmlList.ToString();
    }

    #region Filter Functions

    #region Magazine Filters

    private List<LinqToSql.Magazine> FilterByMagazine(int publishYear)
    {
        if (SHelper.Sort == SearchHelper.Order.ASCENDING)
            return (from mag in finder.Magazines
                    where mag.MagazineID == publishYear
                    orderby mag.PublishYear ascending
                    select mag).ToList();

        return (
            from magazine in finder.Magazines
            where magazine.PublishYear == publishYear
            orderby magazine.PublishYear descending
            select magazine
            ).ToList();
    }

    private List<LinqToSql.Magazine> FilterByMagazine(long number, int publishYear)
    {
        if (SHelper.Sort == SearchHelper.Order.ASCENDING)
            return (from mag in finder.Magazines
                    where mag.MagazineID == number && mag.PublishYear == publishYear
                    orderby mag.PublishYear ascending
                    select mag).ToList();

        return (from mag in finder.Magazines
                where mag.MagazineNumber == number && mag.PublishYear == publishYear
                orderby mag.PublishYear descending
                select mag).ToList();
    }

    private List<LinqToSql.Magazine> FilterByMagazine(long id)
    {
        if (SHelper.Sort == SearchHelper.Order.ASCENDING)
            return (from mag in finder.Magazines
                    where mag.MagazineID == id
                    orderby mag.PublishYear ascending
                    select mag).ToList();

        return (from mag in finder.Magazines
                where mag.MagazineID == id
                orderby mag.PublishYear descending
                select mag).ToList();
    }

    private List<LinqToSql.Magazine> FilterByMagazine()
    {
        if (SHelper.Sort == SearchHelper.Order.ASCENDING)
        {
            return (from mag in finder.Magazines
                    orderby mag.PublishYear ascending
                    select mag).ToList();
        }

        return (from mag in finder.Magazines
                orderby mag.PublishYear descending
                select mag).ToList();
    }

    #endregion Magazine Filters

    #region Author Filters

    private List<LinqToSql.Author> FilterByAuthor(long id)
    {
        if (SHelper.Sort == SearchHelper.Order.DESCENDING)
            return (from author in finder.Authors
                    where author.AuthorID == id
                    orderby author.AuthorFName, author.AuthorMName descending
                    select author).ToList();

        return (from author in finder.Authors
                where author.AuthorID == id
                orderby author.AuthorFName, author.AuthorMName ascending
                select author).ToList();
    }

    private List<LinqToSql.Author> FilterByAuthor(string name)
    {
        name = Helper.ReplaceDiacritics(name);

        if (SHelper.Sort == SearchHelper.Order.DESCENDING)
            return (from author in finder.Authors
                    let fullName = string.Concat(author.AuthorFName, " ", author.AuthorMName, " ", author.AuthorName)
                    where fullName.Contains(name)
                    orderby author.AuthorFName, author.AuthorMName descending
                    select author).ToList();

        return (from author in finder.Authors
                let fullName = string.Concat(author.AuthorFName, " ", author.AuthorMName, " ", author.AuthorName)
                where fullName.Contains(name)
                orderby author.AuthorFName, author.AuthorMName ascending
                select author).ToList();
    }

    private List<LinqToSql.Author> FilterByAuthor(char firstLetter)
    {
        if (SHelper.Sort == SearchHelper.Order.DESCENDING)
            return (from author in finder.Authors
                    where author.AuthorFName.StartsWith(firstLetter.ToString())
                    orderby author.AuthorFName, author.AuthorMName descending
                    select author).ToList();

        return (from author in finder.Authors
                where author.AuthorFName.StartsWith(firstLetter.ToString())
                orderby author.AuthorFName, author.AuthorMName ascending
                select author).ToList();
    }

    private List<LinqToSql.Author> FilterByAuthor()
    {
        if(SHelper.Sort == SearchHelper.Order.DESCENDING)
            return (from author in finder.Authors
                    orderby author.AuthorFName, author.AuthorMName descending
                    select author).ToList();

        return (from author in finder.Authors
                orderby author.AuthorFName, author.AuthorMName ascending
                select author).ToList();
    }

    #endregion Author Filters

    #region Article Filters

    private List<LinqToSql.Article> FilterByArticle(string title)
    {
        title = Helper.ReplaceDiacritics(title);

        if(SHelper.Sort == SearchHelper.Order.DESCENDING)
            return (from article in finder.Articles
                    join akJoin in finder.ArticleKeywords on article.ArticleID equals akJoin.ArticleID into akInto
                    from ak in akInto.DefaultIfEmpty()
                    join kJoin in finder.Keywords on ak.KeywordID equals kJoin.KeywordID into kJoin
                    from k in kJoin.DefaultIfEmpty()
                    where article.ArticleTitle.Contains(title) || k.KeywordValue.Contains(title) //|| k.KeywordValue.Contains(title) //&& article.ShowInResults == 1
                    orderby article.ArticleTitle descending
                    select article).ToList();

        return (from article in finder.Articles
                join akJoin in finder.ArticleKeywords on article.ArticleID equals akJoin.ArticleID into akInto
                from ak in akInto.DefaultIfEmpty()
                join kJoin in finder.Keywords on ak.KeywordID equals kJoin.KeywordID into kJoin
                from k in kJoin.DefaultIfEmpty()
                where article.ArticleTitle.Contains(title) || k.KeywordValue.Contains(title) //|| k.KeywordValue.Contains(title) //&& article.ShowInResults == 1
                orderby article.ArticleTitle ascending
                select article).ToList();
    }

    private List<LinqToSql.Article> FilterByArticle(char firstLetter)
    {
        if(SHelper.Sort == SearchHelper.Order.DESCENDING)
            return (from article in finder.Articles
                    where article.ArticleTitle.StartsWith(firstLetter.ToString()) //&& article.ShowInResults == 1
                    orderby article.ArticleTitle descending
                    select article).ToList();

        return (from article in finder.Articles
                where article.ArticleTitle.StartsWith(firstLetter.ToString()) //&& article.ShowInResults == 1
                orderby article.ArticleTitle ascending
                select article).ToList();
    }

    private List<LinqToSql.Article> FilterByArticle(int number)
    {
        if (SHelper.Sort == SearchHelper.Order.DESCENDING)
        {
            return (from article in finder.Articles
                    where article.ArticleNumber == number //&& article.ShowInResults == 1
                    orderby article.ArticleNumber descending
                    select article).ToList();
        }

        return (from article in finder.Articles
                where article.ArticleNumber == number //&& article.ShowInResults == 1
                orderby article.ArticleNumber ascending
                select article).ToList();
    }

    private List<LinqToSql.Article> FilterByArticle()
    {
        if(SHelper.Sort == SearchHelper.Order.DESCENDING)
            return (from article in finder.Articles
					//where article.ShowInResults == 1
                    orderby article.ArticleTitle descending
                    select article).ToList();

        return (from article in finder.Articles
				//where article.ShowInResults == 1
                orderby article.ArticleTitle ascending
                select article).ToList();
    }

    #endregion Article Filters

    #region Keyword Filter

    private List<LinqToSql.Article> FilterByKeyword(string value)
    {
        value = Helper.ReplaceDiacritics(Helper.SanitizeString(value));
        if(SHelper.Sort == SearchHelper.Order.DESCENDING)
            return (from k in finder.Keywords
                    join akJoin in finder.ArticleKeywords on k.KeywordID equals akJoin.KeywordID into akInto
                    from ak in akInto.DefaultIfEmpty()
                    join aJoin in finder.Articles on ak.ArticleID equals aJoin.ArticleID into aInto
                    from a in aInto.DefaultIfEmpty()
                    where k.KeywordValue.Contains(value) || a.ArticleDesc.Contains(value) || a.ArticleTitle.Contains(value) //&& a.ShowInResults == 1
                    orderby a.ArticleTitle descending
                    select a).ToList();

        return (from k in finder.Keywords
                join akJoin in finder.ArticleKeywords on k.KeywordID equals akJoin.KeywordID into akInto
                from ak in akInto.DefaultIfEmpty()
                join aJoin in finder.Articles on ak.ArticleID equals aJoin.ArticleID into aInto
                from a in aInto.DefaultIfEmpty()
                where k.KeywordValue.Contains(value) || a.ArticleDesc.Contains(value) || a.ArticleTitle.Contains(value) //&& a.ShowInResults == 1
                orderby a.ArticleTitle ascending
                select a).ToList();
    }

    #endregion Keyword Filter

    private List<LinqToSql.Article> FullSearch(string search, bool inTitles = true, bool inAuthors = true)
    {
        search = Helper.ReplaceDiacritics(search.ToLower());
        List<LinqToSql.Article> list = null;

        if (inAuthors)
        {
            list = (from aa in finder.ArticleAuthors
                    where (aa.Author.AuthorFName + " " + aa.Author.AuthorMName + " " + aa.Author.AuthorName).Contains(search)
                    select aa.Article).ToList();
        }

        if (inTitles)
        {
            if (list != null)
            {
                list.AddRange((from a in finder.Articles
                               where a.ArticleTitle.Contains(search)
                               select a).ToList());
            }
        }

        return list;
    }

    #endregion

    #region Format functions

    private string FormatByArticle(List<LinqToSql.Article> list, bool addMagInfo = false)
    {
        StringBuilder html = new StringBuilder();

        if (list.Count > 0)
        {
            IEnumerable<IGrouping<int, LinqToSql.Article>> articles = list.GroupBy(article => article.ArticleNumber);

            foreach (IGrouping<int, LinqToSql.Article> articleGroups in articles)
            {
                LinqToSql.Article tmp = articleGroups.ElementAt(0);

                if (addMagInfo)
                {
                    html.AppendFormat("<div class='article-number article-number-{0}'>", articleGroups.Key);
                    /*html.AppendFormat("<h3 class='collapser article-title' title='Este número pertenece al volumen {1} del año {2}'>Número {0}</h3><div class='collapse-panel clear'>", //<h4 class='title-meta'>Volumen {1} del año {2}</h4>",
                        articleGroups.Key,
                        tmp.Magazine.MagazineNumber,
                        tmp.Magazine.PublishYear);*/
                }
                //else
                //{
                //    //html.AppendFormat("<h3 class='collapser article-title'>Número {0}</h3>", articleGroups.Key);
                //}

                foreach (LinqToSql.Article article in articleGroups)
                {
                    //string description = article.ArticleDesc;

                    //if (description.Length < 1)
                    //{
                    //    description = "sin descripción";
                    //}

                    string title = article.ArticleTitle;

                    if (title.Length < 1)
                    {
                        title = "Sin título";
                    }

                    html.AppendFormat(articleFormat,
                        article.ArticleID,
                        GetArticleDownloadURL(article.ArticleFile,
                            article.Magazine.PublishYear.ToString(),
                            article.Magazine.MagazineNumber.ToString(),
                            article.ArticleNumber.ToString()),
                        title);

                    //html.AppendFormat(articleFormat,
                    //    article.ArticleID,
                    //    title,
                    //    description,
                    //    article.ArticlePageCount,
                    //    FormatAuthors(article.ArticleAuthors),
                    //    GetArticleDownloadURL(article.ArticleFile,
                    //        article.Magazine.PublishYear.ToString(),
                    //        article.Magazine.MagazineNumber.ToString(),
                    //        article.ArticleNumber.ToString()));

                    if (tmp == null)
                        tmp = article;
                }

                /*string volURL = Path.Combine("/",
                    Helper.UPLOAD_PATH,
                    tmp.Magazine.PublishYear.ToString(),
                    tmp.Magazine.MagazineNumber.ToString(),
                    tmp.ArticleNumber.ToString(),
                    Regex.Replace(tmp.ArticleFile, @"_\d+(\.pdf)", "$1"));

                html.AppendFormat("</div><!-- .collapse-panel --><a href='{1}' class='art-no-download' title='Haga clic aquí para descargar el número completo' target='_blank'>Descargar Número</a></p><!-- .article-download --></div><!-- .article-number-{0} -->",
                    articleGroups.Key,
                    volURL);*/

                if (addMagInfo)
                {
                    //html.Append("</div><!-- .collapse-panel -->");
                    html.AppendFormat("</div><!-- .article-number-{0} -->", articleGroups.Key);
                }

                tmp = null;
            }
        }
        else
        {
            html.Append("<p class='no-articles'>Este volumen no tiene ningún artículo aún.</p>");
        }

        return html.ToString();
    }

    private string FormatByAuthor(List<LinqToSql.Author> list)
    {
        StringBuilder html = new StringBuilder();

        foreach (LinqToSql.Author author in list)
        {
            EntitySet<LinqToSql.ArticleAuthor> artAutSet = author.ArticleAuthors;

            if (artAutSet.Count > 0)
            {
                string name = FormatAuthorName(author.AuthorFName, author.AuthorMName, author.AuthorName);
                html.AppendFormat("<div class='author cf' id='author-{0}'>", author.AuthorID);
                html.AppendFormat("<h4 class='author-title article-title collapser'>{0}</h4>", name);
                html.AppendFormat("<div class='collapse-panel clear'>");

                foreach (LinqToSql.ArticleAuthor artAutLink in artAutSet)
                {
                    //string desc = artAutLink.Article.ArticleDesc;
                    string title = artAutLink.Article.ArticleTitle;

                    //if (desc.Length < 1)
                    //    desc = "sin descripción";

                    if (title.Length < 1)
                        title = "sin título";

                    html.AppendFormat(articleFormat, artAutLink.Article.ArticleID, GetArticleDownloadURL(
                            artAutLink.Article.ArticleFile,
                            artAutLink.Article.Magazine.PublishYear.ToString(),
                            artAutLink.Article.Magazine.MagazineNumber.ToString(),
                            artAutLink.Article.ArticleNumber.ToString()
                        ), title);

                    //string volURL = Path.Combine(
                    //        "/",
                    //        Helper.UPLOAD_PATH,
                    //        artAutLink.Article.Magazine.PublishYear.ToString(),
                    //        artAutLink.Article.Magazine.MagazineNumber.ToString(),
                    //        artAutLink.Article.ArticleNumber.ToString(),
                    //        Regex.Replace(artAutLink.Article.ArticleFile, @"_\d+(\.pdf)", "$1")
                    //);

                    //html.AppendFormat(authorContentFormat,
                    //    artAutLink.Article.ArticleID,
                    //    title,
                    //    desc,
                    //    artAutLink.Article.ArticlePageCount,
                    //    GetArticleDownloadURL(
                    //        artAutLink.Article.ArticleFile,
                    //        artAutLink.Article.Magazine.PublishYear.ToString(),
                    //        artAutLink.Article.Magazine.MagazineNumber.ToString(),
                    //        artAutLink.Article.ArticleNumber.ToString()
                    //    ),
                    //    volURL);
                }

                html.AppendFormat("</div><!-- .collapse-panel --></div><!-- #author-{0} -->", author.AuthorID);
            }
        }

        return html.ToString();
    }

    private string FormatByMagazine(List<LinqToSql.Magazine> list)
    {
        StringBuilder html = new StringBuilder();

        foreach (LinqToSql.Magazine magazine in list)
        {
            html.AppendFormat("<div id='magazine-{0}' class='magazine'>", magazine.MagazineID);
            html.AppendFormat("<h2 class='title collapser'>Volumen {0} del año {1}</h2>", magazine.MagazineNumber, magazine.PublishYear);
            html.AppendFormat("<div class='collapse-panel clear'>");
            html.Append(FormatByArticle(magazine.Articles.ToList(), true));
            html.AppendFormat("</div><!-- .collapse-panel --></div><!-- #magazine-{0} -->", magazine.MagazineID);
        }

        return html.ToString();
    }

    private string FormatAuthors(EntitySet<LinqToSql.ArticleAuthor> artAut, string search = "", long id = -1)
    {
        if (search.Length > 0)
            search = Helper.ReplaceDiacritics(search).ToLower();

        StringBuilder html = new StringBuilder();
        string format = "<a title='Haga clic aquí para ver todos los artículos de {1}' href='/buscar/autor/{1}' id='a_{0}' class='author-filter {2}'>{1}</a>, ";

        foreach (LinqToSql.ArticleAuthor author in artAut)
        {
            string fullname = FormatAuthorName(author.Author.AuthorFName, author.Author.AuthorMName, author.Author.AuthorName);

            string toCompare = Helper.ReplaceDiacritics(fullname).ToLower();
            string highlight = "";

            if (fullname.Length > 0)
            {
                if (search.Length > 0)
                {
                    if (toCompare.Contains(search))
                    {
                        highlight = "highlight";
                    }
                }
                else if (id > 0)
                {
                    if (id == author.AuthorID)
                        highlight = "highlight";
                }
            }

            html.AppendFormat(format, author.Author.AuthorID, fullname, highlight);
        }

        return html.ToString().TrimEnd(", ".ToCharArray());
    }

    public string FormatAuthorName(string fname, string mname = "", string name = "")
    {
        StringBuilder r = new StringBuilder(fname.Trim());

        if (mname.Trim().Length > 0)
            r.AppendFormat(" {0}", mname.Trim());

        if (name.Trim().Length > 0)
            r.AppendFormat(", {0}", name.Trim());

        return r.ToString();
    }

    #endregion Format Functions

    public static string GetArticleDownloadURL(string file, string year, string volNo, string artNo)
    {
        return Path.Combine("/", Helper.UPLOAD_PATH, year, volNo, artNo, file);
    }

    #region Helper Functions

    public static List<LinqToSql.Author> GetAuthors(long articleID, LinqToSql.FinderSchemaDataContext context = null)
    {
        if(context == null)
            context = new LinqToSql.FinderSchemaDataContext();
        
        return (from articleAuthor in context.ArticleAuthors
                join author in context.Authors on articleAuthor.AuthorID equals author.AuthorID
                where articleAuthor.ArticleID == articleID
                select author).ToList();
    }

    public static LinqToSql.Magazine GetMagazine(long magazineID, LinqToSql.FinderSchemaDataContext context = null)
    {
        if (context == null)
            context = new LinqToSql.FinderSchemaDataContext();

        return (from magazine in context.Magazines
                where magazine.MagazineID == magazineID
                select magazine).FirstOrDefault();
    }

    public static LinqToSql.Author GetAuthor(long id, LinqToSql.FinderSchemaDataContext context = null)
    {
        if (context == null)
            context = new LinqToSql.FinderSchemaDataContext();

        return (from author in context.Authors
                where author.AuthorID == id
                select author).First();
    }

    public static bool ArticleExists(string fileName, LinqToSql.FinderSchemaDataContext context = null)
    {
        if (fileName.Length > 0)
        {
            if (!Regex.IsMatch(fileName, @"^\d+_\d+_\d+.pdf$", RegexOptions.IgnoreCase))
                throw new Exception(string.Format(
                    "Invalid file name: the name must match the regular expression ^\\d+_\\d+_\\d+.pdf$ (input: {0})",
                    fileName));

            if (context == null)
                context = new LinqToSql.FinderSchemaDataContext();

            var r = (from a in context.Articles
                     where a.ArticleFile == fileName
                     select a);

            return (r != null && r.Count() > 0);
        }

        return false;
    }

    public static bool MagazineExists(int year, long num, LinqToSql.FinderSchemaDataContext context = null)
    {
        if (year > 0 && num > 0)
        {
            if (context == null)
                context = new LinqToSql.FinderSchemaDataContext();

            var r = (from mags in context.Magazines
                     where mags.PublishYear == year && mags.MagazineNumber == num
                     select mags);

            return (r != null && r.Count() > 0);
        }

        return false;
    }

    public static bool KeywordExists(string value, LinqToSql.FinderSchemaDataContext context = null)
    {
        if (value.Length > 0)
        {
            if (context == null)
                context = new LinqToSql.FinderSchemaDataContext();

            var r = (from mags in context.Keywords
                     where mags.KeywordValue == value
                     select mags);

            return (r != null && r.Count() > 0);
        }

        return false;
    }

    public static bool AuthorExists(string fname, string mname = "", string name = "", LinqToSql.FinderSchemaDataContext context = null)
    {
        if (fname.Length > 0)
        {
            if (context == null)
                context = new LinqToSql.FinderSchemaDataContext();

            string fullname = string.Format("{0} {1} {2}", name.Trim(), fname.Trim(), mname.Trim());
            var a = (from r in context.Authors
                     where (r.AuthorName.Trim() + " " + r.AuthorFName.Trim() + " " + r.AuthorMName.Trim()) == fullname
                     select r);

            return (a != null && a.Count() > 0);
        }

        return false;
    }

    #endregion Helper Functions

    #endregion Class Members
}