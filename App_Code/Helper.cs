﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Globalization;
using System.IO;
using iTextSharp.text.pdf;

/// <summary>
/// Summary description for Constants
/// </summary>
public class Helper
{
    public const string UPLOAD_PATH = "Magazine";
    public const string SEARCH_PREFIX = "/Buscar";
    public const string DB_SCHEMA = "FinderSchema.";
    private static Regex StripTags = new Regex(@"</?.+/?>", RegexOptions.Multiline);

    public static List<string> Error
    {
        get;
        set;
    }
    
	public Helper()
	{
        Error.Clear();
	}

    public static bool IsNumeric(string value)
    {
        return Regex.IsMatch(value, @"^\d+$");
    }

    public static string GetPreference(string name)
    {
        string r = "";
        SQLWrapper wrap = new SQLWrapper();
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = wrap.Connect();

        try
        {
            cmd.Parameters.Add("@prefName", System.Data.SqlDbType.VarChar, 200).Value = name;
            cmd.CommandText = String.Format("SELECT PrefValue FROM {0}PreferenceView WHERE PrefName = @prefName", Helper.DB_SCHEMA);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                r = reader.GetString(reader.GetOrdinal("PrefValue"));
            }

            reader.Close();
        }
        catch (Exception e)
        {
            Helper.InitializeErrorList();
            Helper.Error.Add(String.Format("Error leyendo las preferencias: {0}", e.Message));
        }

        wrap.Close();
        cmd.Parameters.Clear();

        return r;
    }

    public static long AddPreference(string name, string value)
    {
        long r = 0L;
        SQLWrapper wrap = new SQLWrapper();
        SqlCommand cmd = new SqlCommand();

        try
        {
            SqlParameter prefID = new SqlParameter("@prefID", SqlDbType.BigInt);
            prefID.Direction = ParameterDirection.Output;
            cmd.Connection = wrap.Connect();
            cmd.Parameters.Add("@prefName", System.Data.SqlDbType.VarChar, 100).Value = name;
            cmd.Parameters.Add("@prefValue", System.Data.SqlDbType.VarChar, 500).Value = value;
            cmd.Parameters.Add(prefID);
            cmd.CommandText = String.Format("EXECUTE {0}cms_UpdatePreference @PrefName = @prefName, @PrefValue = @prefValue, @PrefID = @prefID OUTPUT;", Helper.DB_SCHEMA);
            cmd.ExecuteNonQuery();
            r = (long)cmd.Parameters["@prefID"].Value;
        }
        catch (Exception e)
        {
            Helper.InitializeErrorList();
            Helper.Error.Add(String.Format("Ocurrió un error al extraer una preferencia: {0}", e.Message));
        }

        return r;
    }

    private static void InitializeErrorList()
    {
        if (Helper.Error == null)
            Helper.Error = new List<string>();
    }

    public static string FormatErrors(bool forHtml = true)
    {
        StringBuilder sb = new StringBuilder();

        if (Helper.Error != null)
        {
            if (Helper.Error.Count > 0)
            {
                if (forHtml)
                    sb.Append("<ul class='helper-error-list'>");

                foreach (string error in Helper.Error)
                {
                    if (forHtml)
                        sb.AppendFormat("<li>{0}</li>", error);
                    else
                        sb.AppendFormat("{0}\r\n", error);
                }

                if (forHtml)
                    sb.Append("</ul>");
            }
        }

        return sb.ToString();
    }

    public static string ReplaceDiacritics(string source)
    {
        //string sourceInFormD = source.Normalize(NormalizationForm.FormD);
        //StringBuilder output = new StringBuilder();

        //foreach (char c in sourceInFormD)
        //{
        //    UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(c);
        //    if (uc != UnicodeCategory.NonSpacingMark)
        //    {
        //        output.Append(c);
        //    }
        //}

        //return (output.ToString().Normalize(NormalizationForm.FormC));

        StringBuilder output = new StringBuilder();

        foreach (char c in source)
        {
            char tmp;
            UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(c);

            switch (c.ToString().ToLower()[0])
            {
                case 'á':
                case 'ä':
                case 'â':
                case 'à':
                    tmp = 'a';
                    break;

                case 'é':
                case 'ë':
                case 'ê':
                case 'è':
                    tmp = 'e';
                    break;

                case 'í':
                case 'ï':
                case 'î':
                case 'ì':
                    tmp = 'i';
                    break;

                case 'ó':
                case 'ö':
                case 'ô':
                case 'ò':
                    tmp = 'o';
                    break;

                case 'ú':
                case 'ü':
                case 'û':
                case 'ù':
                    tmp = 'u';
                    break;

                default:
                    tmp = c;
                    break;
            }

            if (uc == UnicodeCategory.UppercaseLetter)
            {
                output.Append(tmp.ToString().ToUpper());
            }
            else
            {
                output.Append(tmp);
            }
        }

        return output.ToString();
    }

    public static string SanitizeString(string userInput)
    {
        Regex reg = new Regex(@"\b((create .*)|delete|from|drop|select|insert|;)\b", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        userInput = reg.Replace(userInput, "[SQLInjection]");
        return userInput;
    }

    public static int CountPages(string file = "")
    {
        int count = 0;

        if (File.Exists(file))
        {
            PdfReader pdf;
            pdf = new PdfReader(file);
            count = pdf.NumberOfPages;
            pdf.Close();
        }
        else
        {
            throw new Exception(String.Format("El archivo '{0}' no existe.", file));
        }

        return count;
    }

    public static bool LinkAuthors(long articleID, long[] newAuthors)
    {
        if (articleID > 0 && newAuthors.Length > 0)
        {
            try
            {
                LinqToSql.FinderSchemaDataContext finder = new LinqToSql.FinderSchemaDataContext();
                List<LinqToSql.ArticleAuthor> list = (from artAut in finder.ArticleAuthors
                                                      where artAut.ArticleID == articleID
                                                      select artAut).ToList();

                foreach (LinqToSql.ArticleAuthor aa in list)
                {
                    finder.ArticleAuthors.DeleteOnSubmit(aa);
                }

                foreach (long id in newAuthors)
                {
                    LinqToSql.ArticleAuthor newAa = new LinqToSql.ArticleAuthor();
                    newAa.ArticleID = articleID;
                    newAa.AuthorID = id;
                    finder.ArticleAuthors.InsertOnSubmit(newAa);
                    newAa = null;
                }

                finder.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                Helper.InitializeErrorList();
                Error.Add("LinkAuthors: " + e.Message);
            }
        }
        else
        {
            Error.Add(String.Format("Either the article ID was zero or the author IDs array was empty (Article ID: {0}, Authors.Length: {1}).", articleID, newAuthors.Length));
        }

        return false;
    }

    public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs = true)
    {
        // Get the subdirectories for the specified directory.
        DirectoryInfo dir = new DirectoryInfo(sourceDirName);
        DirectoryInfo[] dirs = dir.GetDirectories();

        if (!dir.Exists)
        {
            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + sourceDirName);
        }

        // If the destination directory doesn't exist, create it. 
        if (!Directory.Exists(destDirName))
        {
            Directory.CreateDirectory(destDirName);
        }

        // Get the files in the directory and copy them to the new location.
        FileInfo[] files = dir.GetFiles();
        foreach (FileInfo file in files)
        {
            string temppath = Path.Combine(destDirName, file.Name);
            file.CopyTo(temppath, false);
        }

        // If copying subdirectories, copy them and their contents to new location. 
        if (copySubDirs)
        {
            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath, copySubDirs);
            }
        }
    }

    public static bool IsDirectoryEmpty(string path)
    {
        if(Directory.Exists(path) && IsDirectory(path))
            return !Directory.EnumerateFileSystemEntries(path).Any();

        return false;
    }

    /*public static bool IsDirectoryEmpty(string path)
    {
        if (Directory.Exists(path) && IsDirectory(path))
        {
            IEnumerable<string> files = Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories);

            if (files.Count() <= 0)
                return true;
        }

        return false;
    }*/

    public static bool IsDirectory(string path)
    {
        if (Directory.Exists(path))
        {
            FileAttributes att = File.GetAttributes(path);

            if ((att & FileAttributes.Directory) == FileAttributes.Directory)
                return true;
        }

        return false;
    }

    public static string CorrectFileName(string filename, string ext = ".pdf")
    {
        string newName = "";
        long magNo = 0,
            artNo = 0,
            fileNo = 0;

        if (filename.Length > 0)
        {
            if (!Regex.IsMatch(ext, @"^\.\w+$"))
                ext = ".pdf";

            string[] split = filename.Split('_');
            magNo = long.Parse(Regex.Replace(split[0], @"[^\d]+", "").Trim());
            artNo = long.Parse(Regex.Replace(split[1], @"[^\d]+", "").Trim());

            if (split.Length == 3)
            {
                fileNo = long.Parse(Regex.Replace(split[2], @"[^\d]+", "").Trim());
                newName = string.Format("{0}_{1}_{2}{3}", magNo, artNo, fileNo, ext);
            }
            else
            {
                newName = String.Format("{0}_{1}{2}", magNo, artNo, ext);
            }
        }

        return newName;
    }

    public static string BuildFileName(long magNo, long artNo, long fileNo = 0, string ext = ".pdf")
    {
        string name = "";

        if (magNo > 0 && artNo > 0)
        {
            name = string.Format("{0}_{1}", magNo.ToString(), artNo.ToString());

            if (fileNo > 0)
            {
                name = string.Format("{0}_{1}", name, fileNo.ToString());
            }

            name = string.Format("{0}{1}", name, ext);
        }

        return name;
    }

    public static string LastFileName(string path, string ext = ".pdf")
    {
        if (Directory.Exists(path))
        {
            if (!Regex.IsMatch(ext, @"^\.\w+$"))
                ext = ".pdf";

            IEnumerable<string> files = Directory.EnumerateFiles(path, "*" + ext, SearchOption.AllDirectories);
            long big = -1,
                counter = -1;
            string fname = "";

            foreach (string file in files)
            {
                string[] split = Path.GetFileNameWithoutExtension(file).Split('_');

                if (split.Length == 3)
                {
                    long num = long.Parse(split[2]);

                    if (num > big)
                    {
                        big = num;
                        fname = file;
                    }
                }
            }

            return Path.GetFileName(fname);
        }

        return "";
    }

    public static long GetNextFileNumber(string filename, bool zeroBased = false)
    {
        long number = GetCurrentFileNumber(filename);

        if (number < 0)
        {
            if (zeroBased)
                number = 0;
            else
                number = 1;
        }
        else
        {
            number++;
        }

        return number;
    }

    public static long GetCurrentFileNumber(string filename)
    {
        if (filename.Length > 0)
        {
            filename = Path.GetFileNameWithoutExtension(filename);

            if (Regex.IsMatch(filename, @"^\d+_\d+_\d+$"))
            {
                string[] split = filename.Split('_');

                if (split.Length == 3)
                {
                    return long.Parse(Regex.Replace(split[2], @"[^\d]+", ""));
                }
            }
        }

        return -1;
    }

    public static string GetFileName(string appPath, string year, string magno, string artno, string ext = "pdf")
    {
        string path = Path.Combine(appPath, Helper.UPLOAD_PATH, year, magno, artno);
        StringBuilder sb = new StringBuilder();
        long number = 1;
        sb.AppendFormat("{0}_{1}_", magno, artno);

        if (Directory.Exists(path))
        {
            string lastFile = Helper.LastFileName(path);
            number = Helper.GetNextFileNumber(lastFile);
        }

        sb.AppendFormat("{0}.{1}", number, Regex.Replace(ext, @"\.*", ""));

        return sb.ToString();
    }

    public static string Controls(string url, long id, Uri redirect, EditControls controls = EditControls.All, string path = "")
    {
        if (path.Length > 0)
        {
            path = Regex.Replace(path, @"^\?", "");

            if (Regex.IsMatch(path, @"^&"))
                path = HttpUtility.HtmlEncode(path);
            else
                path = "";
        }

        string format = "<a href='{0}?redirect={1}&amp;action={2}{3}{4}' class='edition-table-{2} control-{2} control'>{5}</a>";
        StringBuilder sb = new StringBuilder();
        EditControls c = controls;

        if (c >= EditControls.New)
        {
            sb.AppendFormat(format, url, HttpUtility.HtmlEncode(redirect), "new", "", path, "Nuevo");
            c -= EditControls.New;
        }

        if (c >= EditControls.Delete)
        {
            sb.AppendFormat(format, url, HttpUtility.HtmlEncode(redirect), "delete", String.Format("&amp;id={0}", id), path, "Borrar");
            c -= EditControls.Delete;
        }

        if (c >= EditControls.Edit)
        {
            sb.AppendFormat(format, url, HttpUtility.HtmlEncode(redirect), "edit", String.Format("&amp;id={0}", id), path, "Modificar");
            c -= EditControls.Edit;
        }

        return sb.ToString();
    }

    public static string RemoveHtml(string html, string replace = "")
    {
        return StripTags.Replace(html, replace);
    }

    public static bool RenameFiles(string folder, string volno)
    {
        Helper.InitializeErrorList();

        if (Directory.Exists(folder) && IsDirectory(folder) && Helper.IsNumeric(volno))
        {
            IEnumerable<string> files = Directory.EnumerateFiles(folder, "*.pdf", SearchOption.AllDirectories);      

            if (files.Count() > 0)
            {
                Regex regex = new Regex(@"^\d+_");
                volno += "_";
                string oldf = "",
                    newf = "",
                    fname = "";

                try
                {
                    foreach (string f in files)
                    {
                        fname = regex.Replace(Path.GetFileName(f), volno);
                        oldf = f;
                        newf = Path.Combine(Path.GetDirectoryName(f), fname);
                        //Error.Add(string.Format("name: {0} - old path: {1} - new path: {2}", fname, oldf, newf));
                        File.Move(oldf, newf);
                    }
                }
                catch (Exception ex)
                {
                    Error.Add(string.Format("No se pudo renombrar el archivo {0} (nuevo archivo: {1}): {2}", oldf, newf, ex.Message));
                    return false;
                }
            }
        }
        else
        {
            Error.Add("El directorio no existe o el volumen ingresado no es un número.");
        }

        return true;
    }

    public static string FormatAuthorName(string fname, string mname = "", string name = "")
    {
        StringBuilder sb = new StringBuilder();
        bool sep = false;

        if (fname.Trim().Length > 0)
        {
            sep = true;
            sb.Append(fname);
        }

        if (mname.Trim().Length > 0)
        {
            sep = true;
            sb.AppendFormat(" {0}", mname);
        }

        if (name.Trim().Length > 0)
        {
            if (sep)
                sb.Append(", ");

            sb.Append(name);
        }

        return sb.ToString().Trim();
    }
}

[Flags]
public enum EditControls : int
{
    Edit = 1,
    Delete = 2,
    New = 4,
    All = 7
}