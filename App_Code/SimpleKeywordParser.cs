﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for SimpleKeywordParser
/// </summary>
public class SimpleKeywordParser
{
    private PdfReader Pdf;
    private ITextExtractionStrategy Strategy;

    private Dictionary<string, int> Keywords;
    private StringBuilder FileContent;

    private Regex RegexConnector = new Regex(@"^en\s((prim|terc)er|(cuar|quin|sex)to|(segund|s[ée]ptim|octav|noven|[uú]ltim)o)\slugar$", RegexOptions.Multiline);
    private Regex RegexPunctuation = new Regex(@"[""'\+%/\-_\{\}\(\)\[\]“”0-9°]+", RegexOptions.Multiline);
    private Regex Skip = new Regex(@"(dando|para|saber|contrario|cuanto|seguida|otr[oa]|parte|[úu]ltimo|sobre|todo|puesto|visto|dado|debido|gracias|culpa|causa|ahora|menos|caso|siempre|suponiendo|consiguiente|tanto|manera|consecuencia|tarde|concluir|empezar|terminar|todo|nada|antes|efecto|otras|palabras|decir|ejemplo|pesar|cambio|obstante|embargo|respect[ao]|est[aoe]|poc[ao]s|resumen|resumir|pront[ao]|como|nuestr[ao]s|nuestros|d[íi]as|tiempo|desde|entonces|final|principio)", RegexOptions.IgnoreCase);

    #region MULTI_CONNECTOR
    private string[] MULTI_CONNECTOR =
    {
        "a saber",
        "de lo contrario",
        "en cuanto a",
        "en seguida",
        "por otra parte",
        "por último",
        "por una parte",
        "sobre todo",
        "puesto que",
        "visto que",
        "dado que",
        "ya que",
        "debido a",
        "gracias a",
        "por culpa de",
        "a causa de",
        "ahora que",
        "a menos que",
        "con la condición de que",
        "con tal que",
        "en caso de que",
        "siempre que",
        "suponiendo que",
        "por consiguiente",
        "por eso",
        "por lo tanto",
        "de tal manera que",
        "de manera que",
        "en consecuencia",
        "a consecuencia de",
        "a fin de",
        "más tarde",
        "para concluir",
        "para empezar",
        "para terminar",
        "ante todo",
        "antes que nada",
        "en efecto",
        "en otras palabras",
        "es decir",
        "o sea",
        "por ejemplo",
        "a pesar de",
        "al contrario",
        "en cambio",
        "mientras que",
        "no obstante",
        "sin embargo",
        "al respecto",
        "de esta manera",
        "en cuanto a",
        "en cuanto a esto",
        "en este caso",
        "en lo que respecta",
        "respecto a",
        "en pocas palabras",
        "en resumen",
        "para resumir",
        "mientras que",
        "mientras tanto",
        "tan pronto como",
        "una vez que",
        "hoy en día",
        "en nuestros días",
        "en otro tiempo",
        "desde entonces",
        "al final",
        "al principio"
    };
    #endregion

    #region SINGLE_CONNECTOR
    private string[] SINGLE_CONNECTOR =
    {
        "además",
        "como",
        "porque",
        "si",
        "entonces",
        "para",
        "así",
        "finalmente",
        "luego",
        "después",
        "así",
        "pero",
        "aunque",
        "de",
        "según",
        "actualmente",
        "ahora",
        "antes",
        "apenas",
        "cuando",
        "desde",
        "después",
        "durante",
        "entonces",
        "hasta",
        "mientras"
    };
    #endregion

    public SimpleKeywordParser(string file)
	{
        if (File.Exists(file))
        {
            Pdf = new PdfReader(file);
            Strategy = new SimpleTextExtractionStrategy();
            ReadFile();
            Keywords = new Dictionary<string, int>();
        }
        else
        {
            throw new Exception(String.Format("The file {0} does not exists!", file));
        }
	}

    private void ReadFile()
    {
        FileContent = new StringBuilder();

        for (int i = 0; i < Pdf.NumberOfPages; i++)
        {
            FileContent.Append(PdfTextExtractor.GetTextFromPage(Pdf, i + 1, Strategy));
        }

        Pdf.Close();
    }

    public Dictionary<string, int> FilterKeywords(bool skipWords = false)
    {
        List<string> keys = new List<string>();
        string tmp = FileContent.ToString();

        if(skipWords)
            tmp = Skip.Replace(tmp, "");
        string[] paragraphs = tmp.Split('\n');
        tmp = "";
        FileContent.Clear();

        foreach (string p in paragraphs)
        {
            ParseParagraph(p);
        }

        return Keywords;
    }

    private void ParseParagraph(string p)
    {
        p = Regex.Replace(p, "\t", " ");
        string[] words = p.Split(' ');
        Dictionary<string, int> k = new Dictionary<string, int>();

        foreach (string word in words)
        {
            string tmp = Regex.Replace(word, @"[<>\,;:+\s–“”%\(\)]+", "").TrimEnd('.');
            tmp = tmp.Replace("-", " ");

            if (tmp.Length > 3 && !Regex.IsMatch(tmp, @"[\d.,]"))
            {
                if (k.ContainsKey(tmp))
                {
                    k[tmp]++;
                }
                else
                {
                    k.Add(tmp, 1);
                }
            }
        }

        foreach (KeyValuePair<string, int> kvp in k)
        {
            if (kvp.Value >= 3)
            {
                if (Keywords.ContainsKey(kvp.Key))
                {
                    Keywords[kvp.Key] += kvp.Value;
                }
                else
                {
                    Keywords.Add(kvp.Key, kvp.Value);
                }
            }
        }
    }
}