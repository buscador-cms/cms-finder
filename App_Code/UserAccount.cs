﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserAccount
/// </summary>
public class UserAccount
{
    private string mUName;
    private string mRoles;
    private string mEmail;

    public string UserName {
        get
        {
            return mUName;
        }
        set
        {
            mUName = value;
        }
    }

    public string UserRole
    {
        get
        {
            return mRoles;
        }
        set
        {
            mRoles = value;
        }
    }

    public string UserEmail
    {
        get
        {
            return mEmail;
        }
        set
        {
            mEmail = value;
        }
    }

	public UserAccount(string userName, string roles, string email)
	{
        mUName = userName;
        mRoles = roles;
        mEmail = email;
	}

    public UserAccount(string userName, string roles)
    {
        mUName = userName;
        mRoles = roles;
    }

    public UserAccount(string userName)
    {
        mUName = userName;
    }

    public UserAccount()
    {
        mUName = "";
        mRoles = "";
        mEmail = "";
    }
}