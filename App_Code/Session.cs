﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for Session
/// </summary>
public class SessionHelper
{
    public const int MAX_SALT_LENGTH = 50;
    private SHA512 mAlgorithm;
    private byte[] mHash;

    public SessionHelper()
    {
        mAlgorithm = new SHA512Managed();
    }

    public byte[] MakeHash(string rawPassword, string salt)
    {
        return MakeHash(Encoding.UTF8.GetBytes(rawPassword), Encoding.UTF8.GetBytes(salt));
    }

    public byte[] MakeHash(byte[] rawPassword, byte[] salt)
    {
        byte[] buffer = new byte[rawPassword.Length + salt.Length];

        Array.Copy(rawPassword, buffer, rawPassword.Length);
        Array.Copy(salt, 0, buffer, rawPassword.Length, salt.Length);
        mHash = mAlgorithm.ComputeHash(buffer);

        return mHash;
    }

    public byte[] MakeHash(string rawPassword, byte[] salt)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(rawPassword);
        return MakeHash(bytes, salt);
    }

    public byte[] MakeHash(byte[] bytePassword, string salt)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(salt);
        return MakeHash(bytePassword, salt);
    }

    public string StringHash(byte[] hash, byte[] salt)
    {
        return Convert.ToBase64String(MakeHash(hash, salt));
    }

    public string StringHash(string hash, string salt)
    {
        return Convert.ToBase64String(MakeHash(hash, salt));
    }

    public byte[] MakeSalt(int length = 15)
    {
        if (length <= 0)
            length = 15;

        if (length > MAX_SALT_LENGTH)
            length = 50;

        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        byte[] salt = new byte[length];
        rng.GetNonZeroBytes(salt);
        rng.Dispose();

        return salt;
    }

    public string StringSalt(int length = 15)
    {
        return Convert.ToBase64String(MakeSalt(length));
    }

    public bool CompareHash(string password, string password1)
    {
        return CompareHash(Encoding.UTF8.GetBytes(password), Encoding.UTF8.GetBytes(password1));
    }

    public bool CompareHash(byte[] hash, byte[] hash1)
    {
        if (hash.Length != hash1.Length)
            return false;

        for (int i = 0; i < hash.Length; i++)
        {
            if (! hash[i].Equals(hash1[i]))
                return false;
        }

        return true;
    }

    public void StartSession()
    {

    }
}