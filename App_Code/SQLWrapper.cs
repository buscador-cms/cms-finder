﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Wraps some functions to execute commands.
/// </summary>
public class SQLWrapper {

    private SqlConnection mConnection;
    private Configuration mWebRoot;
    private ConnectionStringSettings mCSettings;
    private String mLastError;
    

	public SQLWrapper() {
        mConnection = new SqlConnection();
        mWebRoot = WebConfigurationManager.OpenWebConfiguration("~/");
        mLastError = "";
	}

    public SqlConnection Connect()
    {
        try
        {
            mCSettings = mWebRoot.ConnectionStrings.ConnectionStrings["FinderConnectionString"];

            if (mCSettings != null)
            {
                mConnection.ConnectionString = mCSettings.ConnectionString;
                mConnection.Open();
            }
            else
            {
                mLastError = "Could not read SQL Server connection parameters. They must be present in the Web.config file on the root folder.";
                return null;
            }
        }
        catch(Exception e)
        {
            mLastError = e.Message;
            return null;
        }

        return mConnection;
    }


    public bool Close()
    {
        if(mConnection != null)
        {
            try
            {
                mConnection.Close();
            }
            catch (Exception e)
            {
                mLastError = e.Message;
                return false;
            }
        }

        return true;
    }

    public string getLastError()
    {
        return mLastError;
    }
}