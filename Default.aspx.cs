﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Text.RegularExpressions;

public partial class SearchParser : System.Web.UI.Page
{
    private SearchHelper.RequestedAction Action;
    private SearchHelper.RequestedItem Item;
    protected string menu = "";
    protected char currentChar;
    protected string results = "";
    protected string searchFor = "";
    protected string searchTitle = "";
    protected static long totalPages;
    private char[] trim = { '/', ' ' };
    private DBSearch db = new DBSearch();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        totalPages = 0;
        SearchHelper sh;

        if (!IsPostBack)
        {
            if (Request["action"] != null && Request["action"] != "")
            {
                ParseAction(Request["action"]);
                ParseItem(Request["item"]);
                sh = new SearchHelper(Action, Item);
                string query = "";
                int page = 1;

                if (Action != SearchHelper.RequestedAction.LIST || (Action == SearchHelper.RequestedAction.LIST && Item != SearchHelper.RequestedItem.NONE))
                {

                    if (Request["query"] != null && Request["query"].Length >= 1)
                        query = Request["query"].ToLower().Trim(trim);

                    if (Action != SearchHelper.RequestedAction.SEARCH || (Action == SearchHelper.RequestedAction.SEARCH && query != ""))
                    {
                        SetText(query);
                        Title = string.Format("{0} {1}", searchTitle, searchFor);
                        sh.Query = query;
                        query = "";

                        if (Title.Trim().Length <= 0)
                            Title = "Buscador de Cuadernos Médico Sociales";

                        if (Request["page"] != null && Request["page"].Length > 0)
                        {
                            query = Request["page"].Trim(trim);

                            if (Helper.IsNumeric(query))
                                page = int.Parse(query);
                        }

                        //results = "page: " + Request["page"];
                        //results += "<br />query: " + Request["query"];
                        //results += "<br />action: " + Request["action"];
                        //results += "<br />item: " + Request["item"];
                        results += Search(sh, page);

                        if (results.Length <= 0)
                            results = string.Format("La búqueda \"{0}\" no produjo resultados.", searchFor);
                    }
                }
                else
                {
                    results = "<p class='error'>No se puede crear una lista si no se especifica de qué: <i>item was empty</i>.</p>";
                }
            }
        }
    }

    private void ParseAction(string action)
    {
        action = action.ToLower().Trim(trim);

        if (Regex.IsMatch(action, @"^(ver|mostrar|view|show|display)$"))
        {
            action = "mostrar";
            Action = SearchHelper.RequestedAction.SHOW;
        }
        else if (Regex.IsMatch(action, @"^(lista?)$"))
        {
            action = "lista";
            Action = SearchHelper.RequestedAction.LIST;
        }
        else if (Regex.IsMatch(action, @"^(search|buscar|find)$"))
        {
            action = "buscar";
            Action = SearchHelper.RequestedAction.SEARCH;
        }
    }

    private void ParseItem(string item)
    {
        Item = SearchHelper.RequestedItem.NONE;

        if (item != null)
        {
            item = item.ToLower().Trim(trim);

            if (item != "")
            {
                if (Regex.IsMatch(item, @"^(t[íi]t(ulo|le)s?|art[ií]culo|article)"))
                {
                    item = "titulo";
                    Item = SearchHelper.RequestedItem.TITLE;
                }
                else if (Regex.IsMatch(item, @"^(volumen?|mag(azine)?)$"))
                {
                    item = "volumen";
                    Item = SearchHelper.RequestedItem.VOLUME;
                }
                else if (Regex.IsMatch(item, @"^(palabras?\s?clave|keywords?|t[ée]rmino)$"))
                {
                    item = "termino";
                    Item = SearchHelper.RequestedItem.KEYWORD;
                }
                else
                {
                    item = "autor";
                    Item = SearchHelper.RequestedItem.AUTHOR;
                }
            }
        }
    }

    public string Search(SearchHelper helper, int page = 1)
    {
        string o = db.Search(helper, page);
        totalPages = db.MaxPages;

        return o;
    }

    private void SetText(string query = "")
    {
        searchFor = String.Format("\"{0}\"", query);

        switch (Action)
        {
            case SearchHelper.RequestedAction.LIST:
                searchTitle = "Lista ";
                break;

            case SearchHelper.RequestedAction.SHOW:
                searchTitle = "Mostrando ";
                break;
        }

        Regex startWith = new Regex(@"^[a-zñáéíóúäëïöü]$", RegexOptions.IgnoreCase);

        switch (Item)
        {
            case SearchHelper.RequestedItem.AUTHOR:
                if (startWith.IsMatch(query))
                {
                    searchFor = String.Format("Autores en la letra <b>{0}</b>", query);
                }
                else if (query.Length > 1)
                {
                    searchFor = string.Format("Autor \"{0}\"", query);
                }
                else
                {
                    searchFor = "de Autores";
                }

                break;

            case SearchHelper.RequestedItem.TITLE:
                if (startWith.IsMatch(query))
                {
                    searchFor = String.Format("en la letra <b>{0}</b>", query);
                }
                else if (query.Length > 1)
                {
                    searchFor = string.Format("Título \"{0}\"", query);
                }
                else
                {
                    searchFor = "de Títulos";
                }

                break;

            case SearchHelper.RequestedItem.VOLUME:
                if (Regex.IsMatch(query, @"^\d+/\d+$"))
                {
                    string[] split = query.Split('/');
                    searchFor = String.Format("\"Volumen {0} del Año {1}\"", split[0], split[1]);
                }
                else
                {
                    searchFor = "de Volumenes";
                }
                break;

            case SearchHelper.RequestedItem.KEYWORD:
                if (query != "")
                {
                    searchFor = string.Format(" \"{0}\"", query);
                    searchTitle = "Buscando";
                }
                break;

            default:
                searchTitle = "Buscando";
                searchFor = string.Format("\"{0}\"", query);
                break;
        }
    }

    protected void SearchBtn_Click(object sender, EventArgs e)
    {
        /*if (SearchTxt.Text.Length >= 3)
        {
            Response.Redirect(String.Format("/{0}/{1}", Helper.SEARCH_PREFIX, SearchTxt.Text));
        }
        else
        {
            results = "Por favor, ingrese al menos 3 caracteres para realizar una búsqueda.";
        }*/
    }
}