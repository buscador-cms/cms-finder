﻿<%@ Page Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="ArticleEditor.aspx.cs" Inherits="Admin_ArticleEditor" %>

<asp:Content ContentPlaceHolderID="HeadScripts" runat="server">
    <script>
        jQuery(document).ready(function ($) {
            var $aids = $('#MainContent_CurrentAuthorIDS'),
                $kids = $('#MainContent_CurrentKeywordIDS'),
                $newKeywds = $('#MainContent_NewKeywords'),
                $nsa = $('#no-selected-authors'),
                $nsk = $('#no-selected-keywords'),
                customAlert = new CustomAlert(),
                newKwCount = 0;

            $('body').on('click', '#add-keyword', function (e) {
                e.preventDefault();
                var kwValue = $('#MainContent_NewKeyword').val(),
                    exists = new RegExp(kwValue, 'gi');                

                if (kwValue.length >= 3) {
                    if (!exists.test($newKeywds.val())) {
                        $nsk.hide();
                        $('<label id="keyword-n' + newKwCount + '">' + kwValue + '</label>').appendTo($('#MainContent_CurrentKeywords'));
                        newKwCount++;

                        if ($newKeywds.val().length > 0)
                            $newKeywds.val($newKeywds.val() + ',' + kwValue);
                        else
                            $newKeywds.val(kwValue);

                        $('#MainContent_NewKeyword').val('');
                    }
                    else {
                        customAlert.show('La palabra clave ya fue añadida :)', 'Palabra Clave Duplicada', ['close']);
                    }
                }
                else {
                    customAlert.show('El largo de la palabra clave debe ser de al menos 3 caracteres :)', 'Palabra Muy Corta', ['close']);
                }
            });

            $('#MainContent_NewKeyword').keyup(function (e) {
                var code = e.keyCode;

                if (code > 46) {
                    e.preventDefault();
                    var regex = /^[a-zñáéíóúäëïöüâêîôû][a-zñáéíóúäëïöüâêîôû0-9\-_\s]*$/gi;

                    if (!regex.test($(this).val())) {
                        _this = $(this);
                        _this.on('keydown', consumeKeys);
                        customAlert.show('Por favor, solo utilice letras (con o sin tilde), guiones (-) y guiones bajos (_).', 'Caracteres No Válidos', ['close']);
                        $('#ca-wall').on('close', function (e) {
                            _this.off('keydown', consumeKeys);
                        });

                        $(this).val($(this).val().replace(/(^[^a-za-zñáéíóúäëïöüâêîôû])|[^a-zñáéíóúäëïöüâêîôû\s\-_]/gi, ''));
                    }
                    else {
                        __doPostBack('<%= NewKeyword.ClientID %>', '');
                    }
                }
            });

            $('body').on('click', '#MainContent_CurrentKeywords label', function (e) {

            });

            $('body').on('click', '#MainContent_AuthorPanel span, #MainContent_KeywordList span', function (e) {
                e.preventDefault();
                var target = getTarget(e),
                    id = $(this).attr('id').replace(target + '-', ''),
                    $appendTo = null,
                    $lbl = null,
                    $ids = null;

                switch (target) {
                    case 'author':
                        $appendTo = $('#MainContent_CurrentAuthorNames');
                        $lbl = $nsa;
                        $ids = $aids;
                        break;

                    case 'keyword':
                        $appendTo = $('#MainContent_CurrentKeywords');
                        $lbl = $nsk;
                        $ids = $kids;
                        break;
                }

                if ($appendTo != null && $ids != null && $lbl != null) {
                    addEntity(id, $(this).text(), target, $appendTo, $ids, $lbl);
                }
                else {
                    l('** An error ocurred adding an entity:');
                    l(e);
                }
                    
            });

            $('body').on('click', '#MainContent_CurrentAuthorNames label, #MainContent_CurrentKeywords label', function (e) {
                e.preventDefault();
                var target = getTarget(e),
                    id = null,
                    $ids = null,
                    $lbl = null;

                switch (target) {
                    case 'author':
                        $ids = $aids;
                        $lbl = $nsa;
                        break;

                    case 'keyword':
                        $ids = $kids;
                        $lbl = $nsk;
                        break;
                }

                $lbl.hide();

                if ($ids != null && $lbl != null) {
                    id = $(this).attr('id').replace(target + '-', '');
                    var checkBoth = false;

                    if (/^n[0-9]+$/.test(id)) {
                        id = $(this).text();
                        $ids = $newKeywds;
                        checkBoth = true;
                    }

                    $(this).remove();
                    removeEntity(id, $ids);

                    if ($ids.val().length <= 0)
                        $lbl.show();

                    if (checkBoth) {
                        if ($kids.val().length <= 0 && $newKeywds.val().length <= 0)
                            $lbl.show();
                        else
                            $lbl.hide();
                    }
                }
                else {
                    l('** An error ocurred trying to delete an entity:');
                    l(e);
                }
            });

            function getTarget(event) {
                var split = event.currentTarget.id.split('-');

                if (split.length >= 1)
                    return split[0];

                return null;
            }

            function addEntity(id, text, target, $appendTo, $ids, $label) {
                l("- addEntity(" + id + ", '" + text + "')");
                var exists = new RegExp(id, 'gm'),
                    val = $ids.val();
                l('- CurrentIDs: ' + val);

                if (!exists.test(val)) {
                    $label.hide();
                    var $names = $appendTo,
                        lbl = '<label class="selected-' + target + '" id="' + target + '-' + id + '">' + text + '</label>';

                    $names.append(lbl);

                    if (val.length > 0) {
                        val += "," + id;
                    }
                    else {
                        val += id;
                    }

                    $ids.val(val);

                    if ($ids.val().length <= 0)
                        $label.show();

                    l('- Modified IDs: ' + val);
                    return true;
                }
                else {
                    customAlert.show('El elemento seleccionado ya existe en la lista :)', 'Elemento Duplicado', [ 'close' ]);
                }

                return false;
            }

            function removeEntity(id, $ids) {
                l("- removeEntity(" + id + ")");
                var regex = new RegExp(',?' + id + ',?', 'gm'),
                    val = $ids.val();

                l('- Current IDs: ' + val);
                // Removes the ID
                val = val.replace(regex.exec(val), ',');
                // Removes double comma
                val = val.replace(',,', ',');
                // Trim commas
                val = trim(val, ',');
                $ids.val(val);
                l('- Modified IDs: ' + val);
            }

            $('#MainContent_MagazineID').on('change', function () {
                var $currentVolLabel = $('#MainContent_CurrentVolume');
                $currentVolLabel.text($(this).find(':selected').text());
            });

            $('#author-search a.reset').click(function (e) {
                e.preventDefault();
				$('#MainContent_AuthorSearchTxt').val('');
                $('#MainContent_AuthorPanel').html('Ingrese un nombre y presione Buscar');
            });

            function consumeKeys(e) {
                if (e.keyCode != 13)
                    return false;

                return true;
            }
        });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Stylesheets" runat="server">
    <style type="text/css">
        h5 {
            font-size: 1.1em;
        }

        fieldset {
            border: none;
            border-radius: 8px;
            box-shadow: 1px 0px 3px black;
        }

        fieldset legend {
            background-color: gold;
            color: darkviolet;
            padding: 5px 10px 5px 10px;
            border-radius: 5px;
            box-shadow: 1px 0 2px black;
        }
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Editor de Artículo</h2>
    <a class="go-back-link" href='<%:url %>'>Volver</a>
    <div id="Messages" runat="server" Visible="false"></div>
    <% if(action != "delete")
       { %>
    <div>
        <h2><%= Title %></h2>
        <fieldset>
            <legend>Número de Artículo</legend>
            <label>¿A que número pertenece este artículo? <span class="required">(requerido)</span></label>:<br />
            <asp:TextBox runat="server" ID="ArticleNumber" CssClass="number-input" />
        </fieldset>
        <!-- /Numero de Articulo -->

        <fieldset>
            <legend>Título</legend>
            <label>Escriba el título del artículo</label>: 
            <asp:TextBox runat="server" ID="ArticleTitle" />
        </fieldset>
        <!-- /Título -->

        <fieldset>
            <legend>Archivo PDF</legend>
            <% if(action == "edit")
               { %>
            <label>Actual <span class="required">(requerido)</span></label>: <asp:Label runat="server" ID="ArticleFile" /><br />
            <% } %>
            <label>Nuevo Archivo <span class="required">(requerido)</span></label>: <asp:FileUpload runat="server" ID="NewArticleFile" />
            <% if(action == "edit") { %>
            <p>
                <label><asp:CheckBox ID="DeleteOldFile" runat="server" Checked="false" /> ¿Eliminar el archivo antiguo?</label><br />
                <span>
                    Si modificó el número de artículo o el volumen al que pertenece este artículo, entonces el nuevo archivo (si es que eligió uno) será subido a una carpeta distinta,
                    por lo que el archivo antiguo no se eliminará, a no ser de que esta casilla este marcada.
                </span>
            </p>
            <% } %>
        </fieldset>
        <!-- /Archivo PDF -->

        <fieldset>
            <legend>Descripción</legend>
            <label>Ingrese una descripción breve de lo que trata el artículo</label>:<br /><asp:TextBox runat="server" ID="ArticleDesc" TextMode="MultiLine" />
        </fieldset>
        <!-- /Descripción -->

        <% if(action == "edit")
           { %>
        <fieldset>
            <legend>Cantidad de Páginas</legend>
            <asp:UpdatePanel ID="CountPagesUP" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="CountPagesLink" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <label>Ingrese el número de páginas o presione el botón <b>Contar Páginas</b></label>:<br />
                    <asp:TextBox runat="server" ID="ArticlePageCount" CssClass="number-input" />
                    <asp:Label ID="PageCountMsg" runat="server" Visible="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:LinkButton ID="CountPagesLink" runat="server" OnClick="CountPagesLink_Click">Contar Páginas</asp:LinkButton>
        </fieldset>
        <!-- /Páginas -->

        <% }
           else
           { %>
        <p>Las páginas se cuentan automáticamente al guardar los cambios.</p>
        <% } %>

        <fieldset>
            <legend>Volumen</legend>
            <label>Seleccione un volumen <span class="required">(requerido)</span></label>:
            <asp:DropDownList ID="MagazineID" runat="server"></asp:DropDownList>
            <p>
                Volumen Seleccionado: <span id="CurrentVolume" runat="server"></span>
            </p>
        </fieldset>
        <!-- /Volumen -->

        <fieldset>
            <legend>Autores</legend>
            <div style="background-color: antiquewhite; margin-bottom: 15px; padding: 8px;">
                <p>
                    Para añadir un autor:
                </p>
                <ol>
                    <li>Escriba el nombre del autor en el campo <i>Nombre</i> y presione <i>Buscar</i></li>
                    <li>Haga clic sobre el autor que desea añadir</li>
                 </ol>
                <p><u>Para eliminar un autor</u>, simplemente haga clic sobre el nombre del autor (bajo el bloque de <i>Autores Seleccionados</i>).</p>
            </div>
            <div id="current-authors">
                <h5>Autores Seleccionados</h5>
                <asp:Panel ID="CurrentAuthorNames" runat="server" CssClass="cf"></asp:Panel>
                <asp:HiddenField ID="CurrentAuthorIDS" runat="server" />
            </div>
            <h5>Lista de Autores</h5>
            <div id="author-search">
                Nombre: <asp:TextBox ID="AuthorSearchTxt" runat="server" CssClass="textfield" ToolTip="Ingrese el nombre del autor (completo o parcial) o una sola letra ('a', por ejemplo) para filtrar todos los autores cuyo apellido paterno comience por esta letra" /> <asp:Button ID="AuthorSearchBtn" runat="server" OnClick="AuthorSearchBtn_Click" Text="Buscar" CssClass="control search-btn" /> <a class="control reset" href="#">Limpiar Resultados</a>
            </div>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="AuthorSearchBtn" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <asp:Panel runat="server" ID="AuthorPanel" ToolTip="Haga clic sobre el autor que desea enlazar" CssClass="cf">
                        <p>Sin resultados...</p>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
        <!-- /Autores -->

        <fieldset>
            <legend>Palabras Clave</legend>
            <p style="background-color: antiquewhite; margin-bottom: 15px; padding: 8px;">
                Ingrese la palabra clave que desea asociar a este artículo y luego presione el botón <b>Agregar</b>, lo que añadirá la palabra a la lista.<br />
                Si la palabra clave aparece en el cuadro más abajo, haga clic sobre esta para añadirla.
            </p>
            <label>Nueva Palabra Clave: <asp:TextBox ID="NewKeyword" runat="server" OnTextChanged="NewKeyword_TextChanged" /> <a href="#" class="control ok" id="add-keyword" style="display: inline">Agregar</a></label>
            <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="NewKeyword" EventName="TextChanged" />
                </Triggers>
                <ContentTemplate>
                    <asp:Panel ID="KeywordList" runat="server" CssClass="cf">
                        <p>Comience a escribir y aparecerán palabras clave similares</p>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="current-keywords">
                <h5>Palabras Clave Actuales</h5>
                <asp:Panel ID="CurrentKeywords" runat="server" CssClass="cf"></asp:Panel>
                <asp:HiddenField ID="CurrentKeywordIDS" runat="server" />
                <asp:HiddenField ID="NewKeywords" runat="server" />
            </div>
        </fieldset>
        <!-- /Palabras Clave -->

        <fieldset>
            <legend>Artículo Completo</legend>
            <% if(action == "edit")
               { %>
            <label title="Este es el actual artículo completo actual">Actual</label>: <asp:Label ID="CurrentVolumeFile" runat="server" /><br />
            <% } %>
            <label title="Seleccione el artículo completo (sin recortar)">Nuevo</label>: <asp:FileUpload ID="NewVolumeFile" runat="server" />
        </fieldset>
        <!-- /Articulo Completo -->
    </div>

    <fieldset class="controls">
        <legend>Acciones</legend>
        <asp:Button ID="SubmitBtn" runat="server" CssClass="control-submit control" ToolTip="Guarda las modificaciones que ha hecho" />
        <input type="reset" value="Reiniciar" class="control-reset control" title="Reiniciar los campos con los valores originales" />
        <a href='<%:url %>' class="control-cancel control" title="Volver a la página anterior SIN guardar los cambios">Cancelar</a>
    </fieldset>
    <% } %>
</asp:Content>
