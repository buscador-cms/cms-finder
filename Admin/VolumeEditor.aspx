﻿<%@ Page Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="VolumeEditor.aspx.cs" Inherits="Admin_VolumeEditor" Debug="true" %>

<asp:Content ID="NewMag" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Editor de Volumen</h2>
    <a href='<%:url %>' class="go-back-link">Volver</a>
    <h4>Mensajes del Servidor</h4>
    <div id="Messages" runat="server"></div>
    <% if(action != "delete")
       { %>
    <fieldset>
        <legend id="LegendTxt" runat="server"></legend>
        <div>
            <label>Número del volumen <span class="required">(requerido)</span></label>: 
            <asp:TextBox ID="MagazineNumber" runat="server" CssClass="number-input" OnTextChanged="PublishYear_TextChanged" AutoPostBack="true" />
        </div>
        <div>
            <label title="Este es el año al que pertenece la revista, sin importar si se publicó en un año distinto">Año de Publicación <span class="required">(requerido)</span></label>: 
            <asp:TextBox ID="PublishYear" runat="server" CssClass="number-input" OnTextChanged="PublishYear_TextChanged" AutoPostBack="true" />
        </div>
        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="PublishYear" EventName="TextChanged" />
                <asp:AsyncPostBackTrigger ControlID="MagazineNumber" EventName="TextChanged" />
            </Triggers>
            <ContentTemplate>
                <p>
                    <asp:Label ID="CheckVolume" runat="server" />
                </p>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <label title="Este nombre es independiente del nombre del o los artículos que compongan el volumen">Nombre del volumen <span class="optional">(opcional)</span></label>: 
            <asp:TextBox ID="MagazineName" runat="server" />
        </div>
        <div>
            <asp:CheckBox ID="DeleteExistentFolder" runat="server" ToolTip="Si marca esta casilla, y el directorio donde se moverán los archivos ya existe, y no esta vacío, es posible que existan archivos con nombres idénticos, lo que hará que los nuevos archivos reemplacen a los antiguos." /> 
            <label>Forzar sobre-escritura</label>
            <asp:LinkButton ID="CheckFolderBtn" runat="server" Text="Comprobar" ToolTip="Comprueba si el directorio esta vacío." OnClick="CheckFolderBtn_Click" CssClass="check-btn" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="CheckFolderBtn" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <asp:Label ID="EmptyFolderLbl" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </fieldset>
    <fieldset class="controls">
        <legend>Controles</legend>
        <asp:Button ID="SubmitBtn" runat="server" CssClass="control-submit control" />
        <input type="reset" value="Reiniciar" class="control-reset control" />
        <a href='<%:url %>' class="control-cancel control">Cancelar</a>
    </fieldset>
    <% } %>
</asp:Content>