﻿<%@ Page Title="Lista de Volumenes" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="VolumeList.aspx.cs" Inherits="Admin_VolumeList" %>

<asp:Content ID="VolumeList" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2><%= Title %></h2>
    <div id="magazine-filter">
        Buscar: <asp:TextBox ID="VolumeFilterTxt" runat="server" ToolTip="Ingrese el número del volumen o el año de publicación y luego haga clic sobre Filtrar" /><asp:Button ID="MagFilterBtn" runat="server" Text="Filtrar" />
    </div>
    <h5>Mensajes del Servidor</h5>
    <div id="Messages" runat="server"></div>
    <% if(totalPages > 10)
       { %>
    <div class="go-to-page">
        <p>Ir a la página <asp:TextBox ID="GTTPageTxt" runat="server" CssClass="number-input" /><asp:Button ID="GTTPageBtn" runat="server" Text="Ir" /> de <%= totalPages %></p>
    </div>
    <% } %>
    <%= pagination %>
    <%= html %>
    <%= pagination %>
    <% if(totalPages > 10)
       { %>
    <div class="go-to-page">
        <p>Ir a la página <asp:TextBox ID="GTBPageTxt" runat="server" CssClass="number-input" /><asp:Button ID="GTBPageBtn" runat="server" Text="Ir" /> de <%= totalPages %></p>
    </div>
    <% } %>
</asp:Content>