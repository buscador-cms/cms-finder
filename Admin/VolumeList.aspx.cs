﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_VolumeList : System.Web.UI.Page
{
    private LinqToSql.FinderSchemaDataContext finder = new LinqToSql.FinderSchemaDataContext();
    private static StringBuilder error = new StringBuilder();
    protected StringBuilder html = new StringBuilder();
    protected static string s = "";
    protected long totalPages = 0;
    private Paginate paginate;
    protected string pagination = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        int page = 1;

        if (Request["page"] != null && Helper.IsNumeric(Request["page"]))
        {
            page = int.Parse(Request["page"]);
        }

        if (GTTPageTxt.Text.Length > 0 && Helper.IsNumeric(GTTPageTxt.Text))
        {
            page = int.Parse(GTTPageTxt.Text);
        }
        else if (GTBPageTxt.Text.Length > 0 && Helper.IsNumeric(GTBPageTxt.Text))
        {
            page = int.Parse(GTBPageTxt.Text);
        }

        if (IsPostBack)
        {
            if (VolumeFilterTxt.Text.Length < 1 && s != "")
            {
                s = "";
                page = 1;
            }
            else if (VolumeFilterTxt.Text.Length > 0)
            {
                s = VolumeFilterTxt.Text;
                page = 1;
            }
        }

        Start(page, s);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (s.Length > 0)
        {
            VolumeFilterTxt.Text = s;
        }

        if (error.Length > 0)
        {
            Messages.InnerHtml = error.ToString();
            error.Clear();
        }

        if (paginate != null)
            GTBPageTxt.Text = GTTPageTxt.Text = paginate.CurrentPage.ToString();
        else
            GTBPageTxt.Text = GTTPageTxt.Text = "1";
    }

    private void Start(int currentPage, string search = "")
    {
        List<LinqToSql.Magazine> list = null;

        if (search.Length > 0 && search != "*")
        {
            if (Helper.IsNumeric(search))
            {
                long s = long.Parse(search);
                list = (from a in finder.Magazines where a.PublishYear == s || a.MagazineNumber == s orderby a.PublishYear descending select a).ToList();
            }
            else
            {
                list = (from a in finder.Magazines where a.MagazineName.Contains(search) orderby a.PublishYear descending select a).ToList();
            }
        }
        else
        {
            list = (from a in finder.Magazines orderby a.PublishYear descending select a).ToList();
        }

        paginate = new Paginate(list.Count, currentPage);
        list = paginate.LimitResults(list);
        pagination = paginate.Pagination;
        totalPages = paginate.MaxPages;
        ParseList(list);
    }

    private void ParseList(List<LinqToSql.Magazine> list)
    {
        string url = "/Admin/VolumeEditor.aspx";
        string newLink = Helper.Controls(url, 0, Request.Url, EditControls.New);
        html.AppendFormat("<div class='controls'>{0}</div>", newLink);

        if (list != null && list.Count > 0)
        {

            StringBuilder header = new StringBuilder();
            html.Append("<table class='edition-table'>");
            html.Append("<thead><tr><th>Año de Publicación</th><th>Número</th><th>Nombre</th><th>Acciones</th></tr></thead>");
            html.Append("<tbody>");

            foreach (LinqToSql.Magazine m in list)
            {
                html.Append("<tr>");
                html.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td>", m.PublishYear, m.MagazineNumber, m.MagazineName);
                html.AppendFormat("<td class='edition-table-controls'>{0}</td>", Helper.Controls(url, m.MagazineID, Request.Url, EditControls.Edit));
                html.Append("</tr>");
            }

            html.Append("</tbody>");
            html.Append("</table>");
            html.AppendFormat("<div class='controls'>{0}</div>", newLink);
        }
        else
        {
            html.Append("<p>No hay resultados...</p>");
        }
    }
}