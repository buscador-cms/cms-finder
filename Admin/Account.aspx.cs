﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public partial class Admin_Account : System.Web.UI.Page
{
    private SqlConnection conn = new SqlConnection();
    private SQLWrapper wrap = new SQLWrapper();
    private SqlCommand cmd;
    private StringBuilder msg = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        conn = wrap.Connect();

        if (conn == null)
        {
            Messages.InnerHtml = wrap.getLastError();
        }
        else
        {
            cmd = new SqlCommand();
            cmd.Connection = conn;
        }

        if (!IsPostBack)
        {
            SetUserData();
            SetPreferenceData();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        if(cmd != null)
            cmd.Dispose();

        if (wrap != null)
            wrap.Close();
    }

    protected void ModifyUser_Click(object sender, EventArgs e)
    {
        Messages.InnerHtml = "";
        UpdateUser();
        UpdatePreferences();
        Messages.InnerHtml += Helper.FormatErrors();
    }

    private void UpdateUser()
    {
        StringBuilder insert = new StringBuilder();
        try
        {
            bool modify = true;

            if (OldPass.Text.Length < 1)
            {
                modify = false;
            }
            else
            {
                if (OldPass.Text.Length > 0)
                {
                    SessionHelper sh = new SessionHelper();
                    List<byte[]> data = GetHashData((string)Session["userName"]);
                    byte[] pass = sh.MakeHash(OldPass.Text, data[1]);

                    if (!sh.CompareHash(pass, data[0]))
                    {
                        modify = false;
                        PasswordValidator.ErrorMessage = "La contraseña ingresada es incorrecta.";
                        return;
                    }
                    else
                    {
                        byte[] salt = sh.MakeSalt(50);
                        byte[] newPass = sh.MakeHash(NewPass1.Text, salt);
                        insert.Append("AccountPassword = @NewPassword, AccountSalt = @NewSalt,");
                        cmd.Parameters.Add("@NewPassword", SqlDbType.VarBinary, 256).Value = newPass;
                        cmd.Parameters.Add("@NewSalt", SqlDbType.VarBinary, 256).Value = salt;
                    }
                }
            }

            if (modify)
            {
                cmd.CommandText = "UPDATE FinderSchema.Account SET ";
                cmd.CommandText += insert.ToString().TrimEnd(',');

                if (cmd.ExecuteNonQuery() == 1)
                {
                    Messages.InnerHtml = "Cambios Guardados!<br />";
                }
                else
                {
                    Messages.InnerHtml = "No se pudo modificar la información.<br />";
                }
            }
        }
        catch (Exception ex)
        {
            Messages.InnerHtml = ex.Message;
        }

        cmd.Parameters.Clear();
    }

    private void UpdatePreferences()
    {
        if (Helper.IsNumeric(ResultsPPTxt.Text) && Helper.IsNumeric(PageRange.Text))
        {
            if (Helper.AddPreference("results_per_page", ResultsPPTxt.Text) <= 0 || Helper.AddPreference("pagination_range", PageRange.Text) <= 0)
            {
                Messages.InnerHtml += "No se pudieron actualizar las preferencias.<br />";
            }
            else
            {
                Messages.InnerHtml += "Preferencias actualizadas!<br />";
            }
        }
    }

    private List<byte[]> GetHashData(string user)
    {
        List<byte[]> data = new List<byte[]>();
        cmd.Parameters.Clear();
        cmd.CommandText = "SELECT AccountSalt, AccountPassword FROM FinderSchema.Account WHERE AccountUser = @User";
        cmd.Parameters.Add("@User", SqlDbType.VarChar, 100).Value = user;

        try
        {
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();
                data.Add((byte[])reader["AccountPassword"]);
                data.Add((byte[])reader["AccountSalt"]);
                Messages.InnerHtml = "Se crearon los indices";
            }
            else
            {
                Messages.InnerHtml = "No se encontró al usuario '" + user + "'";
            }

            reader.Close();
        }
        catch (Exception e)
        {
            Messages.InnerHtml = e.Message;
        }

        cmd.Parameters.Clear();
        return data;
    }

    protected void SetUserData(bool refresh = false)
    {
        UserAccount ua = new UserAccount();
        cmd.CommandText = "SELECT * FROM FinderSchema.Account WHERE AccountUser = @AccUser";
        cmd.Parameters.Add("@AccUser", SqlDbType.VarChar, 100).Value = System.Web.Security.FormsAuthentication.Decrypt(
            Request.Cookies[System.Web.Security.FormsAuthentication.FormsCookieName].Value).Name;

        try
        {
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();
                OldUName.Text = reader.GetString(reader.GetOrdinal("AccountUser"));

                if(!reader.IsDBNull(reader.GetOrdinal("AccountEmail")))
                    Email.Text = reader.GetString(reader.GetOrdinal("AccountEmail"));
            }
            else
            {
                Messages.InnerHtml = "El usuario no existe? No se devolvió ningún resultado.";
            }

            reader.Close();
        }
        catch (Exception e)
        {
            Messages.InnerHtml = e.Message;
        }

        cmd.Parameters.Clear();
    }

    protected void SetPreferenceData()
    {
        string rpp = Helper.GetPreference("results_per_page");

        if (rpp.Length > 0)
        {
            ResultsPPTxt.Text = rpp;
        }
        else
        {
            ResultsPPTxt.Text = "15";
        }

        string pagerange = Helper.GetPreference("pagination_range");

        if (pagerange.Length > 0)
        {
            PageRange.Text = pagerange;
        }
        else
        {
            PageRange.Text = "3";
        }
    }

    protected void FlushCacheBtn_Click(object sender, EventArgs e)
    {
        StringBuilder builder = new StringBuilder();

        try
        {
            new CacheHelper().DeleteAllCache();
            builder.Append("<span class='success'>Caché eliminada!</span>");
        }
        catch (Exception ex)
        {
            builder.AppendFormat("<span class='error'>Error</span>: {0} - Stack: {1}", ex.Message, ex.StackTrace);
        }

        FlushCacheLbl.Text = builder.ToString();
    }
}