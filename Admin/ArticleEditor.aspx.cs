﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Admin_ArticleEditor : System.Web.UI.Page
{
    private static long id = 0;
    private LinqToSql.FinderSchemaDataContext finder;
    private LinqToSql.Article article;
    protected static string action;
    protected static string url;
    private StringBuilder error = new StringBuilder();

    #region Page Events

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            ResetProperties();

            if (action == "" && !IsPostBack)
                ParseParams();

            if (finder == null)
                finder = new LinqToSql.FinderSchemaDataContext();
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error <b>antes</b> de cargar la página: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (action == "delete")
            {
                Delete();
            }
            else
            {
                AddEvents();
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error <b>al cargar</b> la página: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (id > 0)
        {
            finder = new LinqToSql.FinderSchemaDataContext();
            article = (from a in finder.Articles
                       where a.ArticleID == id
                       select a).Single();
        }

        if (action == "edit")
        {
            FillFields();
        }

        if (action != "delete")
        {
            FillMagazineDDL();
            FillAuthorLB();
        }

        article = null;

        if (error.Length > 0)
        {
            Messages.InnerHtml = "<h4>Respuesta</h4>";
            Messages.Visible = true;
            Messages.InnerHtml += error.ToString();
            error.Clear();
        }
    }
    #endregion Page Events

    private void ParseParams()
    {
        if (Request["action"] != null && Regex.IsMatch(Request["action"], @"^(edit|new|delete)$", RegexOptions.IgnoreCase))
        {
            if (Request["action"].ToLower() != action)
            {
                ResetProperties(true);
                action = Request["action"].ToLower();
            }
        }
        else
        {
            Response.Redirect(url);
        }

        if (Request["redirect"] != null)
        {
            url = Request["redirect"];
        }

        if (action == "edit" || action == "delete")
        {
            if (Request["id"] != null)
            {
                if (!Helper.IsNumeric(Request["id"]))
                {
                    Response.Redirect(url);
                }

                id = long.Parse(Request["id"]);
            }
            else
            {
                Response.Redirect(url);
            }
        }
    }

    private void ResetProperties(bool force = false)
    {
        if (!IsPostBack || force)
        {
            url = "/";
            action = "";
            article = null;
            id = 0;
        }
    }

    private void AddEvents()
    {
        if (action == "edit")
        {
            SubmitBtn.Text = "Guardar Cambios";
            SubmitBtn.Click += OnUpdate;
            SubmitBtn.Click -= OnCreate;
        }
        else
        {
            Title = "Crear Artículo";
            SubmitBtn.Text = "Listo";
            SubmitBtn.Click += OnCreate;
            SubmitBtn.Click -= OnUpdate;
        }
    }

    private void FillFields()
    {
        NewKeyword.Text = "";
        AuthorSearchTxt.Text = "";
        ArticleNumber.Text = article.ArticleNumber.ToString();
        ArticleTitle.Text = article.ArticleTitle.Trim();
        ArticleFile.Text = article.ArticleFile.Trim();
        ArticleDesc.Text = article.ArticleDesc.Trim();
        ArticlePageCount.Text = article.ArticlePageCount.ToString();
        CurrentVolumeFile.Text = GetFullArticleFile(article.Magazine.PublishYear, article.ArticleFile);
        Title = string.Format("Editando el artículo '<b>{0} - Archivo: {1}</b>'", article.ArticleTitle, article.ArticleFile);
        FillCurrentAuthorPanel();
        FillCurrentKeywordPanel();
    }

    private void FillCurrentAuthorPanel()
    {
        if (article != null)
        {
            if (article.ArticleAuthors.Count > 0)
            {
                //error.Append("articleAuthorCount > 0 = true");
                StringBuilder authorIds = new StringBuilder();
                HtmlGenericControl lbl = null;

                foreach (LinqToSql.ArticleAuthor aa in article.ArticleAuthors.OrderBy(a => a.Author.AuthorFName))
                {
                    lbl = new HtmlGenericControl("label");
                    lbl.InnerText = Helper.FormatAuthorName(aa.Author.AuthorFName,
                        aa.Author.AuthorMName,
                        aa.Author.AuthorName);
                    lbl.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    lbl.ID = string.Format("author-{0}", aa.Author.AuthorID);
                    lbl.Attributes.Add("class", "selected-author");
                    CurrentAuthorNames.Controls.Add(lbl);
                    authorIds.AppendFormat("{0},", aa.Author.AuthorID);
                }

                CurrentAuthorIDS.Value = authorIds.ToString().TrimEnd(',');
            }
            else
            {
                Label l = new Label();
                l.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                l.ID = "no-selected-authors";
                l.Text = "No hay autores seleccionados";
                l.CssClass = "highlight";
                CurrentAuthorNames.Controls.Add(l);
            }
        }
        else
        {
            error.Append("<p class='error'>Article was NULL!</p>");
        }
    }

    private void FillCurrentKeywordPanel()
    {
        if (article != null)
        {
            if (article.ArticleKeywords.Count > 0)
            {
                StringBuilder kwIds = new StringBuilder();
                HtmlGenericControl lbl = new HtmlGenericControl("label");

                foreach (LinqToSql.ArticleKeyword ak in article.ArticleKeywords)
                {
                    lbl = new HtmlGenericControl("label");
                    lbl.InnerText = ak.Keyword.KeywordValue;
                    lbl.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    lbl.ID = string.Format("keyword-{0}", ak.Keyword.KeywordID);
                    lbl.Attributes.Add("class", "selected-keyword");
                    CurrentKeywords.Controls.Add(lbl);
                    kwIds.AppendFormat("{0},", ak.Keyword.KeywordID);
                }

                CurrentKeywordIDS.Value = kwIds.ToString().TrimEnd(',');
            }
            else
            {
                Label l = new Label();
                l.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                l.ID = "no-selected-keywords";
                l.Text = "No hay palabras clave asociadas";
                l.CssClass = "highlight";
                CurrentKeywords.Controls.Add(l);
            }
        }
        else
        {
            error.Append("<p class='error'>Article was NULL!</p>");
        }
    }

    private void FillMagazineDDL()
    {
        if (MagazineID.Items.Count < 1)
        {
            List<LinqToSql.Magazine> list = (from m in finder.Magazines
                                             orderby m.PublishYear descending
                                             select m).ToList();

            if (list != null && list.Count > 0)
            {
                foreach (LinqToSql.Magazine mag in list)
                {
                    string label = string.Format("Volumen {0} del año {1}", mag.MagazineNumber, mag.PublishYear);
                    ListItem li = new ListItem(label, mag.MagazineID.ToString());

                    if (action == "edit")
                    {
                        li.Selected = article.MagazineID == mag.MagazineID;

                        if (li.Selected)
                            CurrentVolume.InnerHtml = label;
                    }

                    MagazineID.Items.Add(li);
                }
            }
        }
        else
        {
            if (article != null)
            {
                MagazineID.Items.FindByValue(article.MagazineID.ToString()).Selected = true;
                CurrentVolume.InnerHtml = string.Format("Volumen {0} del año {1}", article.Magazine.MagazineNumber, article.Magazine.PublishYear);
            }
        }
    }

    private void FillAuthorLB()
    {
        // Fills a ListBox called Authors - unfriendly; because it's hard to search for an author (using Javascript is slow)
        //if (Authors.Items.Count < 1)
        //{
        //    List<LinqToSql.Author> list = (from authors in finder.Authors
        //                                   select authors).ToList();

        //    if (list != null && list.Count > 0)
        //    {
        //        char last = ' ',
        //            current = ' ';

        //        foreach (LinqToSql.Author a in list)
        //        {
        //            if (last == ' ' || current == ' ')
        //            {
        //                current = last = a.AuthorFName.Trim().ToLower()[0];
        //            }

        //            string fullName = string.Format("{0} {1} {2}", a.AuthorFName, a.AuthorMName, a.AuthorName).Trim();
        //            ListItem li = new ListItem(fullName, a.AuthorID.ToString());

        //            if (article != null)
        //            {
        //                foreach (LinqToSql.ArticleAuthor ent in article.ArticleAuthors)
        //                {
        //                    if (a.AuthorID == ent.AuthorID)
        //                        li.Selected = true;
        //                }
        //            }

        //            Authors.Items.Add(li);
        //        }
        //    }
        //}
        //else
        //{
        //    if(article != null)
        //        foreach (LinqToSql.ArticleAuthor aa in article.ArticleAuthors)
        //        {
        //            Authors.Items.FindByValue(aa.AuthorID.ToString()).Selected = true;
        //        }
        //}

        // Fills a Panel control called AuthorPanel (slow to render when there are hundreds of authors).
        /*if (AuthorPanel.Controls.Count <= 0)
        {
            List<LinqToSql.Author> authorList = (from authors in finder.Authors
                                           orderby authors.AuthorFName
                                           select authors).ToList();
            bool emptyAuthorList = true;

            if (authorList != null && authorList.Count > 0)
            {
                char index = '\0';
                string name = "",
                    fname = "",
                    mname = "",
                    aid = "";

                StringBuilder caid = new StringBuilder();
                StringBuilder fullName = new StringBuilder();
                CheckBox chk = null;
                Panel p = null;

                foreach (LinqToSql.Author a in authorList)
                {
                    name = a.AuthorName.Trim();
                    fname = a.AuthorFName.Trim();
                    mname = a.AuthorMName.Trim();
                    aid = a.AuthorID.ToString();
                    char tmpIndex = '\0';

                    try
                    {
                        tmpIndex = Helper.ReplaceDiacritics(fname).ToLower().First(firstChar => Regex.IsMatch(firstChar.ToString(), @"[a-zñáéíóúäëïöü]{1}", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase));
                    }
                    catch
                    {
                        Response.Write("<p class='error'>Ocurrió un error procesando la lista de autores. Asegúrese de que no hayan autores sin un <s>apellido paterno</s>.</p>");
                    }

                    if (tmpIndex != index)
                    {
                        index = tmpIndex;
                        HtmlGenericControl h5 = new HtmlGenericControl("h5");
                        h5.InnerHtml = index.ToString().ToUpper();
                        p = new Panel();
                        p.CssClass = "index-panel cf";
                        p.ID = "Index" + index.ToString();
                        p.Controls.Add(h5);
                        AuthorPanel.Controls.Add(p);
                    }

                    bool sep = false;

                    if (fname.Length > 0)
                    {
                        sep = true;
                        fullName.Append(fname);
                    }

                    if (mname.Length > 0)
                    {
                        sep = true;
                        fullName.AppendFormat(" {0}", mname);
                    }

                    if (name.Length > 0)
                    {
                        if (sep)
                            fullName.Append(", ");

                        fullName.Append(name);
                    }

                    chk = new CheckBox();
                    chk.ID = "author" + aid;
                    chk.InputAttributes.Add("value", aid);
                    chk.Text = fullName.ToString();
                    Panel inner = new Panel();
                    HtmlGenericControl lbl = null;

                    // Check if the current author was linked to this article or not before (only for edit mode).
                    if (article != null)
                    {
                        foreach (LinqToSql.ArticleAuthor aa in article.ArticleAuthors)
                        {
                            if (aa.AuthorID == a.AuthorID)
                            {
                                chk.Checked = true;
                                inner.CssClass = "selected-author";
                                caid.AppendFormat("{0},", a.AuthorID);
                                lbl = new HtmlGenericControl("label");
                                lbl.InnerText = fullName.ToString();
                                lbl.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                                lbl.ID = string.Format("author-{0}", a.AuthorID);
                                lbl.Attributes.Add("class", "selected-author");
                                CurrentAuthorNames.Controls.Add(lbl);
                                emptyAuthorList = false;
                            }
                        }
                    }

                    inner.Controls.Add(chk);
                    p.Controls.Add(inner);

                    // Reset variables
                    inner = null;
                    chk = null;
                    fullName.Clear();
                    fname = mname = name = "";
                }

                CurrentAuthorIDS.Value = caid.ToString().TrimEnd(',');
            }

            if(emptyAuthorList)
            {
                Label l = new Label();
                l.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                l.ID = "no-selected-authors";
                l.Text = "No hay autores seleccionados";
                l.CssClass = "highlight";
                CurrentAuthorNames.Controls.Add(l);
            }
        }*/
    }

    protected void OnCreate(object sender, EventArgs e)
    {
        Create();
    }

    protected void OnUpdate(object sender, EventArgs e)
    {
        Update();
    }

    private void Create()
    {
        if (ValidateData())
        {
            long magID = long.Parse(MagazineID.SelectedValue);
            int artNo = int.Parse(ArticleNumber.Text);
            LinqToSql.Magazine magazine = DBSearch.GetMagazine(magID, finder);

            string path = Path.Combine(
                Request.PhysicalApplicationPath,
                Helper.UPLOAD_PATH,
                magazine.PublishYear.ToString(),
                magazine.MagazineNumber.ToString(),
                artNo.ToString());

            string fileName = Helper.GetFileName(
                Request.PhysicalApplicationPath,
                magazine.PublishYear.ToString(),
                magazine.MagazineNumber.ToString(),
                artNo.ToString());

            if (!DBSearch.ArticleExists(fileName))
            {
                if (!Directory.Exists(path))
                {
                    if (!Directory.CreateDirectory(path).Exists)
                    {
                        error.AppendFormat("<p class='error'>No se pudo crear el directorio {0}.</p>", path);
                        return;
                    }
                }

                string basePath = path;
                path += "\\" + fileName;

                if (!File.Exists(path))
                {
                    NewArticleFile.SaveAs(path);

                    if (File.Exists(path))
                    {
                        int pc = Helper.CountPages(path);
                        string title = ArticleTitle.Text;
                        string desc = ArticleDesc.Text;

                        try
                        {
                            LinqToSql.Article newArticle = new LinqToSql.Article();
                            newArticle.ArticleNumber = artNo;
                            newArticle.ArticleFile = fileName;
                            newArticle.ArticlePageCount = pc;
                            newArticle.ArticleTitle = title;
                            newArticle.ArticleDesc = desc;
                            newArticle.MagazineID = magID;
                            finder.Articles.InsertOnSubmit(newArticle);
                            finder.SubmitChanges();

                            if (newArticle.ArticleID > 0)
                            {
                                UpdateFullArticleFile(basePath, artNo, magazine.MagazineNumber);
                                error.Append("<p class='success'>Artículo creado sin problemas.</p>");
                                LinkAuthors(newArticle);
                                LinkKeywords(newArticle);
                                RefreshCache();
                                ResetFields();
                            }
                            else
                            {
                                error.Append("<p class='error'>No se pudo crear el artículo.</p>");
                            }
                        }
                        catch (Exception e)
                        {
                            error.AppendFormat("<p class='error'>Ocurrió un error intentando crear el artículo: {0}<br />{1}.</p>", e.Message, e.StackTrace);
                        }
                    }
                }
                else
                {
                    error.AppendFormat("<p class='error'>El archivo {0} ya existe en servidor. Por favor, comuníquese con el soporte técnico.</p>", fileName);
                }
            }
            else
            {
                error.AppendFormat("<p class='error'>El archivo <b>{0}</b> ya existe en la base de datos. En lugar de crear un nuevo artículo, edite el existente, así evita perder información.</p>", fileName);
            }
        }
    }

    private void ResetFields()
    {
        ArticleNumber.Text = "";
        ArticleTitle.Text = "";
        ArticleDesc.Text = "";
        ArticlePageCount.Text = "";
        MagazineID.ClearSelection();
    }

    private void Update()
    {
        if (ValidateData())
        {
            if (article == null)
                article = (from a in finder.Articles
                           where a.ArticleID == id
                           select a).Single();

            string basePath = Path.Combine(Request.PhysicalApplicationPath, Helper.UPLOAD_PATH);
            string tmp = "";
            bool moveoldfile = false;
            LinqToSql.Magazine mag = article.Magazine;
            long magID = long.Parse(MagazineID.SelectedValue);
            int oldNo = article.ArticleNumber,
                newNo = int.Parse(ArticleNumber.Text);

            if (magID != article.MagazineID)
            {
                mag = (from m in finder.Magazines
                       where m.MagazineID == magID
                       select m).Single();
                moveoldfile = true;
            }

            if (oldNo == newNo)
            {
                newNo = oldNo;
            }
            else
            {
                moveoldfile = true;
            }

            string filename = article.ArticleFile;
            string dir = Path.Combine(basePath, mag.PublishYear.ToString(), mag.MagazineNumber.ToString(), newNo.ToString());
            bool update = false;
            int pageCount = 0;

            if (Helper.IsNumeric(ArticlePageCount.Text))
            {
                pageCount = int.Parse(ArticlePageCount.Text);
            }

            if (NewArticleFile.HasFile)
            {
                if (DeleteOldFile.Checked)
                {
                    tmp = Path.Combine(dir, article.ArticleFile);

                    try
                    {
                        File.Delete(tmp);
                    }
                    catch { }

                    if (File.Exists(tmp))
                    {
                        error.AppendFormat("<p class='error'>No se pudo eliminar el archivo antiguo (ruta: {0}).</p>", tmp);
                        return;
                    }
                }

                if (oldNo != newNo || magID != article.MagazineID)
                    filename = Helper.GetFileName(Request.PhysicalApplicationPath, mag.PublishYear.ToString(), mag.MagazineNumber.ToString(), newNo.ToString());

                UploadState r = MoveUploadedFile(NewArticleFile, dir, filename);

                if (!r.Uploaded)
                {
                    error.AppendFormat("<p class='error'>No se pudo subir el archivo.</p>");
                    return;
                }
                else
                {
                    pageCount = r.PageCount;
                    error.Append("<p class='success'>Artículo subido sin problemas!</p>");
                    moveoldfile = false;
                }
            }

            if (oldNo != newNo)
            {
                article.ArticleNumber = newNo;
                update = true;
            }

            if (ArticleTitle.Text != article.ArticleTitle)
            {
                article.ArticleTitle = ArticleTitle.Text;
                update = true;
            }

            if (Regex.IsMatch(filename, @"\d+_\d+(_\d+)?\.pdf") && article.ArticleFile != filename)
            {
                update = true;
            }

            if (article.ArticleDesc != ArticleDesc.Text)
            {
                article.ArticleDesc = ArticleDesc.Text;
                update = true;
            }

            if (article.ArticlePageCount != pageCount)
            {
                article.ArticlePageCount = pageCount;
                update = true;
            }

            if (magID != article.MagazineID)
            {
                //article.MagazineID = magID;
                update = true;
            }

            try
            {
                if (update)
                {
                    if (moveoldfile)
                    {
                        filename = Helper.GetFileName(Request.PhysicalApplicationPath, mag.PublishYear.ToString(), mag.MagazineNumber.ToString(), newNo.ToString());
                        tmp = Path.Combine(Request.PhysicalApplicationPath, Helper.UPLOAD_PATH,
                            article.Magazine.PublishYear.ToString(), article.Magazine.MagazineNumber.ToString(), oldNo.ToString());

                        State r = MoveOldFile(article.ArticleFile, tmp, filename, dir, true);

                        if (!r.Moved)
                        {
                            error.AppendFormat("<p class='error'>No se pudo mover el archivo antiguo ({0}).</p>", filename);
                            return;
                        }
                    }

                    article.Magazine = mag;
                    article.ArticleFile = filename;
                    finder.SubmitChanges();
                }
                else
                {
                    error.Append("<p class='success'>Al parecer no ha modificado nada sobre el artículo!</p>");
                }

                UpdateFullArticleFile(dir, article.ArticleNumber, mag.MagazineNumber);
                LinkAuthors(article);
                LinkKeywords(article);
                error.Append("<p class='success'>Cambios Guardados!</p>");
                RefreshCache();
            }
            catch (Exception ex)
            {
                error.AppendFormat("<p class='error'>Ocurrió un error actualizando el artículo: {0}</p>", ex.Message);
            }
        }
    }

    private void Delete()
    {
        try
        {
            article = (from a in finder.Articles
                       where a.ArticleID == id
                       select a).Single();
            string path = Path.Combine(Request.PhysicalApplicationPath,
                Helper.UPLOAD_PATH,
                article.Magazine.PublishYear.ToString(),
                article.Magazine.MagazineNumber.ToString(),
                article.ArticleNumber.ToString(),
                article.ArticleFile);
            finder.Articles.DeleteOnSubmit(article);
            finder.SubmitChanges();

            if (File.Exists(path))
                File.Delete(path);

            RefreshCache();
            Response.Redirect(url);
        }
        catch (Exception e)
        {
            error.AppendFormat("<p class='error'>No se pudo eliminar el artículo: {0}<br />Trace: {1}<br /><br /><a class='go-back-link' href='{2}'>Volver</a></p>", e.Message, e.StackTrace, url);
        }
    }

    private bool ValidateData()
    {
        Regex noInterDots = new Regex(@"[a-záéíóúäëïöüñ\-]\.[a-záéíóúäëïöüñ\-]", RegexOptions.IgnoreCase);

        string t = ArticleNumber.Text;

        if (t.Length <= 0 || !Helper.IsNumeric(t))
        {
            error.Append("<p class='error'>Por favor, ingrese un número de artículo válido</p>");
            return false;
        }

        if (action == "new" && !NewArticleFile.HasFile)
        {
            error.Append("<p class='error'>No ha elegido ningún archivo PDF para subir!</p>");
            return false;
        }

        if (NewArticleFile.HasFile && Path.GetExtension(NewArticleFile.FileName) != ".pdf")
        {
            error.Append("<p class='error'>El archivo seleccionado no es un documento PDF.</p>");
            return false;
        }

        t = MagazineID.SelectedValue;

        if (t == null || !Helper.IsNumeric(t))
        {
            error.Append("<p class='error'>Aún no ha elegido el volumen al que pertenece este artículo.</p>");
            return false;
        }

        return true;
    }

    #region User Events

    protected void CountPagesLink_Click(object sender, EventArgs e)
    {
        try
        {
            CountPages();
        }
        catch (Exception ex)
        {
            PageCountMsg.Text = string.Format("{0}<br />{1}", ex.Message, ex.StackTrace);
            PageCountMsg.Visible = true;
        }
    }

    protected void AuthorSearchBtn_Click(object sender, EventArgs e)
    {
        SearchForAuthor();
    }

    protected void NewKeyword_TextChanged(object sender, EventArgs e)
    {
        FindKeyword();
    }

    #endregion User Events

    #region Helper Functions

    private void CountPages()
    {
        PageCountMsg.Text = "";
        PageCountMsg.Visible = false;

        if (article == null)
            article = (from a in finder.Articles
                       where a.ArticleID == id
                       select a).First();

        string artNo = article.ArticleNumber.ToString();
        string path = Path.Combine(
                Request.PhysicalApplicationPath,
                Helper.UPLOAD_PATH,
                article.Magazine.PublishYear.ToString(),
                article.Magazine.MagazineNumber.ToString(),
                artNo,
                article.ArticleFile
            );

        if (File.Exists(path))
        {
            int pages = Helper.CountPages(path);

            ArticlePageCount.Text = pages.ToString();

            if (PageCountMsg.Text.Length < 1)
            {
                PageCountMsg.Text = "Listo!";
            }
            else
            {
                PageCountMsg.Text = "<p class='error'>No se pudieron contar las páginas.</p>";
            }
        }
        else
        {
            PageCountMsg.Text = string.Format("<p class='error'>El archivo {0} no existe.</p>", path);
        }

        PageCountMsg.Visible = true;
    }

    private string GetFullArticleFile(long year, string articleFile)
    {
        if (articleFile.Length > 0 && year > 0)
        {
            string[] split = articleFile.Split('_');
            int magNo = int.Parse(split[0]);
            int artNo = int.Parse(split[1]);
            string file = String.Format("{0}_{1}.pdf", split[0], split[1]);
            string path = Path.Combine(Request.PhysicalApplicationPath,
                Helper.UPLOAD_PATH,
                year.ToString(),
                magNo.ToString(),
                artNo.ToString(),
                file);

            if (File.Exists(path))
            {
                return file;
            }
        }
        return "No se ha definido";
    }

    private bool LinkAuthors(LinqToSql.Article entity)
    {
        bool r = false,
            c = false;

        try
        {
            List<long> ids = new List<long>();
            string[] numbers = CurrentAuthorIDS.Value.Split(',');

            foreach (string s in numbers)
            {
                if (s.Trim().Length > 0)
                    ids.Add(long.Parse(s.Trim()));
            }

            DeleteAuthors(entity);

            if (ids.Count > 0 && entity != null)
            {

                foreach (long id in ids)
                {
                    LinqToSql.ArticleAuthor tmp = new LinqToSql.ArticleAuthor();
                    tmp.ArticleID = entity.ArticleID;
                    tmp.AuthorID = id;
                    c = entity.ArticleAuthors.Contains(tmp);

                    if (!c)
                    {
                        finder.ArticleAuthors.InsertOnSubmit(tmp);
                    }
                }

                finder.SubmitChanges();
                error.Append("<p class='success'>Autores enlazados!</p>");
                r = true;
            }
            else
            {
                error.Append("<p class='warning'>No se seleccionó ningún autor.</p>");
            }

            CurrentAuthorIDS.Value = String.Join(",", numbers);
        }
        catch (Exception e)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error al enlazar autores<br />{0}<br />{1}</p>", e.Message, e.StackTrace);
        }

        return r;
    }

    private void DeleteAuthors(LinqToSql.Article entity)
    {
        if (entity.ArticleAuthors.Count > 0)
        {
            foreach (LinqToSql.ArticleAuthor aa in entity.ArticleAuthors)
            {
                finder.ArticleAuthors.DeleteOnSubmit(aa);
            }

            finder.SubmitChanges();
        }
    }

    private void LinkKeywords(LinqToSql.Article entity)
    {
        bool r = false;
        LinqToSql.Keyword keyword = null;
        List<long> keywordIds = new List<long>();

        try {
            
            if (NewKeywords.Value.Length > 0)
            {
                string[] newKewds = NewKeywords.Value.Split(',');

                foreach (string s in newKewds)
                {
                    if (s.Length >= 3 && !finder.Keywords.Any(keywd => keywd.KeywordValue == s))
                    {
                        //error.AppendFormat("<p>New Keyword: {0}</p>", s);
                        keyword = new LinqToSql.Keyword();
                        keyword.KeywordValue = s;
                        finder.Keywords.InsertOnSubmit(keyword);
                        finder.SubmitChanges();
                        keywordIds.Add(keyword.KeywordID);
                        //error.AppendFormat("<p>New KeywordID: {0}</p>", keyword.KeywordID);
                        keyword = null;
                    }
                }

                error.Append("<p class='success'>Nuevas Palabras Clave Creadas Sin Problemas!</p>");
            }
        }
        catch(Exception e)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error al crear nuevas palabras clave: {0}<br /><br />Stack: {1}</p>", e.Message, e.StackTrace);
        }
        
        if (CurrentKeywordIDS.Value.Length > 0)
        {
            string[] newIds = CurrentKeywordIDS.Value.Split(',');

            foreach (string s in newIds)
            {
                keywordIds.Add(long.Parse(s));
            }
        }

        try
        {
            DeleteKeywords(entity);

            if (keywordIds.Count > 0)
            {
                //error.AppendFormat("KeywordIds.count: {0}", keywordIds.Count);
                LinqToSql.ArticleKeyword ak = null;

                foreach (long id in keywordIds)
                {
                    ak = new LinqToSql.ArticleKeyword();
                    ak.KeywordID = id;
                    ak.ArticleID = entity.ArticleID;
                    finder.ArticleKeywords.InsertOnSubmit(ak);
                    ak = null;
                }

                finder.SubmitChanges();
                error.Append("<p class='success'>Palabras Clave enlazadas sin problemas!</p>");
            }
            else
            {
                error.Append("<p class='success'>No hay palabras clave que procesar.</p>");
            }
        }
        catch (Exception e)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error enlazando las palabras clave: {0}<br /><br />Stack: {1}</p>", e.Message, e.StackTrace);
        }
    }

    private void DeleteKeywords(LinqToSql.Article entity)
    {
        if (entity.ArticleKeywords.Count > 0)
        {
            foreach (LinqToSql.ArticleKeyword ak in entity.ArticleKeywords)
            {
                finder.ArticleKeywords.DeleteOnSubmit(ak);
            }

            finder.SubmitChanges();
        }
        else
        {
            //error.Append("No keywords to delete!");
        }
    }

    private State MoveOldFile(string oldFile, string oldPath, string newFile, string newPath, bool overwrite = false)
    {
        State r = new State();
        r.Moved = false;
        r.Name = oldFile;
        oldPath = Path.Combine(oldPath, oldFile);

        if (File.Exists(oldPath))
        {
            if (!Directory.Exists(newPath))
            {
                if (!Directory.CreateDirectory(newPath).Exists)
                {
                    error.AppendFormat("<p class='error'>No se pudo crear la ruta {0}.</p>", newPath);
                    return r;
                }
            }

            newPath = Path.Combine(newPath, newFile);

            try
            {
                bool exists = File.Exists(newPath);

                if (!exists || overwrite)
                {
                    if (exists)
                        File.Delete(newPath);

                    File.Move(oldPath, newPath);

                    if (File.Exists(newPath))
                    {
                        File.Delete(oldPath);
                        r.Moved = true;
                        r.Name = newFile;
                        return r;
                    }
                }
                else
                {
                    r.Moved = false;
                    error.AppendFormat("<p class='warning'>El archivo {0} ya existe!</p>", newPath);
                }
            }
            catch (Exception e)
            {
                error.AppendFormat("<p class='error'>Ocurrió un error intentado mover el archivo :{0}<br />{1}</p>", e.Message, e.StackTrace);
            }
        }
        else
        {
            error.AppendFormat("<p class='warning'>El archivo antiguo '{0}' no existe.</p>", oldPath);
            r.Moved = true;
        }

        return r;
    }

    private UploadState MoveUploadedFile(FileUpload fu, string path, string fileName)
    {
        UploadState r = new UploadState();

        if (!Directory.Exists(path))
        {
            if (Directory.CreateDirectory(path).Exists == false)
            {
                error.AppendFormat("<p class='error'>No se pudo crear la estructura de directorios ({0}), lo que impide guardar el nuevo archivo.</p>", path);
            }
        }

        path = Path.Combine(path, fileName);
        fu.SaveAs(path);
        r.Uploaded = File.Exists(path);

        if (r.Uploaded)
        {
            r.FilePath = path;

            if (File.Exists(path))
                r.PageCount = Helper.CountPages(path);
            else
                r.PageCount = 0;
        }

        return r;
    }

    private void UpdateFullArticleFile(string basePath, int artNo, long magNo)
    {
        if (NewVolumeFile.HasFile)
        {
            if (Path.GetExtension(NewVolumeFile.FileName) == ".pdf")
            {
                string fafPath = Path.Combine(basePath, Helper.BuildFileName(magNo, artNo));
                NewVolumeFile.SaveAs(fafPath);

                if (!File.Exists(fafPath))
                {
                    error.AppendFormat("<p class='warning'>No se pudo guardar el artículo completo (archivo: {0}).</p>", fafPath);
                }
                else
                {
                    error.Append("<p class='success'><b>Artículo completo</b> creado sin problemas.</p>");
                }
            }
            else
            {
                error.Append("<p class='warning'>El artículo completo no es un archivo PDF, por ende, no se guardó, pero podrá hacerlo en cualquier momento.</p>");
            }
        }
    }

    private void RefreshCache()
    {
        try
        {
            CacheHelper c = new CacheHelper();
            c.DeleteAllCache(string.Format(CacheHelper.ARTICLE_CACHE_FILE, ".*", ".*"));
        }
        catch (Exception e)
        {
            error.AppendFormat("<p class='error'>Error borrando cache: {0}<br />{1}.</p>", e.Message, e.StackTrace);
        }
    }

    private string GetAuthorList()
    {
        string cacheFile = "authorlist.cache";
        CacheHelper ch = new CacheHelper();
        StringBuilder sb = new StringBuilder(ch.ReadCache(cacheFile));

        if (sb.Length <= 0)
        {

            List<LinqToSql.Author> list = (from a in finder.Authors
                                           select a).ToList();

            foreach (LinqToSql.Author a in list)
            {
                string tmp = a.AuthorFName.Trim();
                sb.AppendFormat("");
            }

            ch.WriteCache(cacheFile, sb.ToString());
        }

        return sb.ToString();
    }

    private void SearchForAuthor()
    {
        AuthorPanel.Controls.Clear();
        string search = Helper.ReplaceDiacritics(AuthorSearchTxt.Text.Trim());
        HtmlGenericControl p = null;

        if (search.Length > 0)
        {
            List<LinqToSql.Author> authorList = null;

            switch (search.Length)
            {
                case 1:
                    authorList = (from a in finder.Authors
                                  where a.AuthorFName.StartsWith(search)
                                  orderby a.AuthorFName
                                  select a).ToList();
                    break;

                default:
                    authorList = (from a in finder.Authors
                                  where (a.AuthorFName + " " + a.AuthorMName + " " + a.AuthorName).Contains(search)
                                  orderby a.AuthorFName
                                  select a).ToList();
                    break;
            }

            if (authorList.Count > 0)
            {
                Label lbl;

                foreach (LinqToSql.Author a in authorList)
                {
                    lbl = new Label();
                    lbl.Text = Helper.FormatAuthorName(a.AuthorFName, a.AuthorMName, a.AuthorName);
                    lbl.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    lbl.ID = "author-" + a.AuthorID;
                    AuthorPanel.Controls.Add(lbl);
                }

                lbl = null;
            }
            else
            {

                string url = string.Format("/Admin/AuthorEditor.aspx?action=new&redirect={0}", HttpUtility.UrlEncode(Request.Url.ToString()));
                p = new HtmlGenericControl("p");
                p.InnerHtml = string.Format("Sin resultados... Intente nuevamente.<br />Si cree que no existe, haga clic <a href='{0}'>aquí</a> para crear un nuevo autor.", url);
                AuthorPanel.Controls.Add(p);
            }
        }
        else
        {
            p = new HtmlGenericControl("p");
            p.InnerText = "Sin resultados...";
            AuthorPanel.Controls.Add(p);
        }
    }

    private void FindKeyword()
    {
        string key = Helper.ReplaceDiacritics(NewKeyword.Text.Trim());
        KeywordList.Controls.Clear();

        if (key.Length > 0)
        {
            List<LinqToSql.Keyword> list = (from k in finder.Keywords
                                            where k.KeywordValue.StartsWith(key)
                                            select k).ToList();


            if (list.Count > 0)
            {
                Label l = null;
                foreach (LinqToSql.Keyword kwrd in list)
                {
                    l = new Label();
                    l.Text = kwrd.KeywordValue;
                    l.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                    l.ID = string.Format("keyword-{0}", kwrd.KeywordID);
                    KeywordList.Controls.Add(l);
                }
            }
            else
            {
                HtmlGenericControl p = new HtmlGenericControl("p");
                p.InnerHtml = "Sin coincidencias...";
                KeywordList.Controls.Add(p);
            }
        }
    }

    #endregion Helper Functions

    #region Helper Classes
    class State
    {
        public bool Moved { set; get; }
        public string Name { set; get; }
    }

    class UploadState
    {
        public bool Uploaded { set; get; }
        public int PageCount { set; get; }
        public string FilePath { set; get; }
    }
    #endregion Helper Classes
}
