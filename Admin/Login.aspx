﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="LoginPage" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
	<h2>Area de Administración</h2>
	<fieldset id="login-panel">
		<legend>Iniciar Sesión</legend>
		<p>
		    <asp:Label ID="Label1" runat="server">Usuario:</asp:Label> <asp:TextBox ID="LoginUser" runat="server" />
		</p>
		<p>
		    <asp:Label ID="Label2" runat="server">Password:</asp:Label> <asp:TextBox ID="LoginPass" runat="server" TextMode="Password" />
		</p>
		<p>No Cerrar sesión <asp:CheckBox ID="KeepOpen" runat="server" /></p>
		<asp:Button Text="Login" runat="server" ID="LoginBtn" OnClick="LoginBtn_Click" CssClass="control submit" />
	    <div runat="server" id="Messages"></div>
	</fieldset>
</asp:Content>
