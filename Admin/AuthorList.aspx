﻿<%@ Page Title="Lista de Autores" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="AuthorList.aspx.cs" Inherits="Admin_AuthorList" %>

<asp:Content ContentPlaceHolderID="HeadScripts" runat="server">
    <script>
        jQuery(document).ready(function ($) {
            var ca = new CustomAlert();

            $('a.article-list').click(function (e) {
                var $cont = $(this).next().html();
                ca.show($cont, 'Artículos Asociados', ['close']);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="AuthorList" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2><%= Title %></h2>
    <div id="author-filter">
        Buscar: <asp:TextBox ID="AuthorFilterTxt" runat="server" ToolTip="Ingrese el apellido paterno, apellido materno y nombre, en ese orden o sólo uno de ellos"></asp:TextBox><asp:Button ID="AuthorFilterBtn" runat="server" Text="Filtrar" />
    </div>
    <h5>Mensajes del Servidor</h5>
    <div id="Messages" runat="server"></div><!-- #Messages -->
    <% if(totalPages > 10)
       { %>
    <div class="go-to-page">
        <p>Ir a la página <asp:TextBox ID="GTTPageTxt" runat="server" CssClass="number-input" /><asp:Button ID="GTTPageBtn" runat="server" Text="Ir" /> de <%= totalPages %></p>
    </div>
    <% } %>
    <%= pagination %>
    <%= html %>
    <%= pagination %>
    <% if(totalPages > 10)
       { %>
    <div class="go-to-page">
        <p>Ir a la página <asp:TextBox ID="GTBPageTxt" runat="server" CssClass="number-input" /><asp:Button ID="GTBPageBtn" runat="server" Text="Ir" /> de <%= totalPages %></p>
    </div>
    <% } %>
</asp:Content>