﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ArticleList : System.Web.UI.Page
{
    private LinqToSql.FinderSchemaDataContext finder = new LinqToSql.FinderSchemaDataContext();
    private static StringBuilder error = new StringBuilder();
    protected static string s = "";
    protected StringBuilder html = new StringBuilder();
    private Paginate paginate;
    protected string pagination = "";
    protected long totalPages = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        int page = 1;

        if (Request["page"] != null && Helper.IsNumeric(Request["page"]))
        {
            page = int.Parse(Request["page"]);
        }

        if (GTTPageTxt.Text.Length > 0 && Helper.IsNumeric(GTTPageTxt.Text))
        {
            page = int.Parse(GTTPageTxt.Text);
        }
        else if (GTBPageTxt.Text.Length > 0 && Helper.IsNumeric(GTBPageTxt.Text))
        {
            page = int.Parse(GTBPageTxt.Text);
        }

        if (IsPostBack && s != "")
        {
            s = "";
            page = 1;
        }

        if (ArticleFilterTxt.Text.Length > 0)
        {
            s = ArticleFilterTxt.Text;
            page = 1;
        }

        Start(page, s);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (s.Length > 0)
        {
            ArticleFilterTxt.Text = s;
        }

        if (error.Length > 0)
        {
            Messages.InnerHtml = error.ToString();
            error.Clear();
        }

        if (paginate != null)
            GTBPageTxt.Text = GTTPageTxt.Text = paginate.CurrentPage.ToString();
        else
            GTBPageTxt.Text = GTTPageTxt.Text = "1";
    }

    private void Start(int currentPage, string search = "")
    {
        List<LinqToSql.Article> list = null;

        try
        {
            StringBuilder format = new StringBuilder();

            if (search.Length > 0)
            {
                list = ParseSearch(search);
            }
            else
            {
                list = (from a in finder.Articles orderby a.Magazine.PublishYear descending, a.ArticleFile, a.ArticleNumber select a).ToList();
            }

            paginate = new Paginate(list.Count, currentPage, format.ToString());
            list = paginate.LimitResults(list);
            pagination = paginate.Pagination;
            totalPages = paginate.MaxPages;
            ParseList(list);
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error procesando la búsqueda: {0}<br />Trace: {1}", ex.Message, ex.StackTrace);
        }
    }

    private List<LinqToSql.Article> ParseSearch(string search)
    {
        search = search.ToLower();
        List<LinqToSql.Article> articles = null;
        Regex regex = new Regex(@"^([vy]\d+)\s?([vy]\d+)?", RegexOptions.IgnoreCase);
        Match match = regex.Match(search);

        if (match.Length > 0)
        {
            GroupCollection gc = match.Groups;

            string year = "",
                number = "";
            int i = 0;

            foreach (Group g in gc)
            {
                if (i > 0)
                {
                    string val = g.Value.Trim();

                    if (val.Length > 0)
                    {
                        char first = val[0];

                        switch (first)
                        {
                            case 'v':
                                if (number.Length < 1)
                                    number = Regex.Replace(val, @"[^\d]", "");
                                break;

                            case 'y':
                                if (year.Length < 1)
                                    year = Regex.Replace(val, @"[^\d]", "");
                                break;
                        }
                    }
                }
                i++;
            }

            int iyear = 0;

            if(year.Length > 0)
                iyear = int.Parse(year);

            long lnumber = 0;

            if(number.Length > 0)
                lnumber = long.Parse(number);


            if (iyear > 0 && lnumber > 0)
            {
                articles = (from a in finder.Articles
                            where a.Magazine.PublishYear == iyear && a.Magazine.MagazineNumber == lnumber
                            orderby a.Magazine.PublishYear descending
                            select a).ToList();
            }
            else if (iyear <= 0 && lnumber > 0)
            {
                articles = (from a in finder.Articles
                            where a.Magazine.MagazineNumber == lnumber
                            orderby a.Magazine.PublishYear descending
                            select a).ToList();
            }
            else if (iyear > 0 && lnumber <= 0)
            {
                articles = (from a in finder.Articles
                            where a.Magazine.PublishYear == iyear
                            orderby a.Magazine.PublishYear descending
                            select a).ToList();
            }
        }
        else if (Helper.IsNumeric(search))
        {
            articles = (from a in finder.Articles
                        where a.ArticleNumber == int.Parse(search)
                        orderby a.Magazine.PublishYear descending
                        select a).ToList();
        }
        else
        {
            if (Regex.IsMatch(search, @"^\d+[_\-]+\d+[_\-]+\d+(\.pdf)?"))
            {
                search = Regex.Replace(search, @"[-_]+", "_");

                if(!search.EndsWith(".pdf"))
                    search += ".pdf";

                articles = (from a in finder.Articles
                            where a.ArticleFile.Contains(search)
                            orderby a.Magazine.PublishYear descending
                            select a).ToList();
            }
            else
            {
                articles = (from a in finder.Articles
                            where a.ArticleTitle.Contains(search)
                            orderby a.Magazine.PublishYear descending
                            select a).ToList();
            }
        }

        return articles;
    }

    private void ParseList(List<LinqToSql.Article> list)
    {
        string url = "/Admin/ArticleEditor.aspx";
        string newLink = Helper.Controls(url, 0, Request.Url, EditControls.New);
        html.AppendFormat("<div class='controls'><p>{0}</p></div>", newLink);

        if (list != null && list.Count > 0)
        {
            html.Append("<div id='article-list'>");

            foreach (LinqToSql.Article a in list)
            {
                string fileUrl = DBSearch.GetArticleDownloadURL(a.ArticleFile,
                    a.Magazine.PublishYear.ToString(),
                    a.Magazine.MagazineNumber.ToString(),
                    a.ArticleNumber.ToString());

                html.Append("<div class='item cf'>");
                string title = a.ArticleTitle.Length > 0 ? a.ArticleTitle : "No tiene";
                html.AppendFormat("<h4 title='Título del artículo'>{0}</h4><div class='article'>", title);
                html.AppendFormat("<p title='Número de artículo al que pertenece este archivo'>Número: {0}.</p>", a.ArticleNumber);
                html.AppendFormat("<p title='Archivo PDF actual'>Archivo PDF: <a href='{0}' target='_blank' title='Descargar artículo'>{1}</a>.</p>", fileUrl, a.ArticleFile);
                html.AppendFormat("<p title='Nombre del volumen: {2}'>Pertenece al volumen: <b>{0} del año {1}</b>.</p>", a.Magazine.MagazineNumber, a.Magazine.PublishYear, a.Magazine.MagazineName);
                html.Append("</div><div class='meta'><h5 class='collapser'>Otra información</h5>");
                html.Append("<div class='collapse-panel'><div class='meta-authors'>Autores: ");

                if (a.ArticleAuthors.Count > 0)
                {
                    html.Append("<a href='#'>Autores</a>");
                    html.AppendFormat("<div class='hidden'>{0}</div>", BuildAuthorList(a.ArticleAuthors));
                }
                else
                {
                    html.Append("<i>nadie vinculado</i>");
                }

                html.Append("</div><!-- .meta-authors -->");
                html.Append("<div class='description'>Descripción: ");

                if (a.ArticleDesc.Trim().Length > 0)
                {
                    html.Append("<a href='#' title='Haga clic aquí para leer la descripción'>Descripción</a>");
                    html.AppendFormat("<p class='hidden'>{0}</p>", a.ArticleDesc.Trim());
                }
                else
                {
                    html.Append("<i>no tiene</i>");
                }

                html.Append("</div><!-- .description -->");
                html.AppendFormat("<p class='pages'>Páginas: {0}</p>", a.ArticlePageCount);
                html.Append("<div class='clear keywords'>Palabras clave: ");

                if (a.ArticleKeywords.Count > 0)
                {
                    html.Append(BuildKeywordList(a.ArticleKeywords));
                }
                else
                {
                    html.Append("<i>no tiene palabras clave asociadas</i>");
                }

                html.Append("</div><!-- .keywords-->");
                html.Append("</div><!-- .collapse-panel --></div><!-- .meta -->");
                html.AppendFormat("<div class='controls clear'>{0}</div>", Helper.Controls(url, a.ArticleID, Request.Url, EditControls.Edit | EditControls.Delete));
                html.Append("</div><!-- .item -->");
            }

            html.Append("</div><!-- #article-list-->");
            html.AppendFormat("<div class='controls'><p>{0}</p></div>", newLink);
        }
        else
        {
            html.Append("<p>No hay resultados...</p>");
        }
    }

    private string BuildAuthorList(EntitySet<LinqToSql.ArticleAuthor> list)
    {
        StringBuilder output = new StringBuilder();

        if (list != null && list.Count > 0)
        {
            output.Append("<ul class='author-list'>");
            string url = "";

            foreach (LinqToSql.ArticleAuthor r in list)
            {
                url = HttpUtility.UrlEncode(Request.Url.ToString());
                output.AppendFormat("<li><a href='/Admin/AuthorEditor.aspx?action=edit&id={0}&redirect={1}'>{2}</a></li>",
                    r.Author.AuthorID,
                    url,
                    Helper.FormatAuthorName(r.Author.AuthorFName, r.Author.AuthorMName, r.Author.AuthorName));
            }

            output.Append("</ul>");
        }

        return output.ToString();
    }

    private string BuildKeywordList(EntitySet<LinqToSql.ArticleKeyword> list)
    {
        StringBuilder output = new StringBuilder();

        if (list != null && list.Count > 0)
        {
            output.Append("<ul class='keyword-list'>");
            string url = "";

            foreach (LinqToSql.ArticleKeyword keyword in list)
            {
                url = HttpUtility.UrlEncode(Request.Url.ToString());
                output.AppendFormat("<li><a href='/Admin/KeywordEditor.aspx?action=edit&id={0}&redirect={1}'>{2}</a></li>",
                    keyword.Keyword.KeywordID,
                    url,
                    keyword.Keyword.KeywordValue);
            }

            output.Append("</ul>");
        }

        return output.ToString();
    }
}