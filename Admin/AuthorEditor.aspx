﻿<%@ Page Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="AuthorEditor.aspx.cs" Inherits="Admin_AuthorEditor" %>

<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Editor de Autor</h2>
    <a class="go-back-link" href='<%:url %>'>Volver</a>
    <h4>Mensajes del Servidor</h4>
    <div id="Messages" runat="server"></div>
    <% if(action != "delete")
       { %>
    <fieldset>
        <legend><%= Title %></legend>
        <div>
            <asp:Label runat="server" Text="Apellido Paterno" /> <span class="required">(obligatorio)</span>:
            <asp:TextBox runat="server" ID="AuthorFName" />
        </div>
        <div>
            <asp:Label runat="server" Text="Apellido Materno" />:
            <asp:TextBox runat="server" ID="AuthorMName" />
        </div>
        <div>
            <asp:Label runat="server" Text="Nombre" />:
            <asp:TextBox runat="server" ID="AuthorName" />
        </div>
    </fieldset>
    <fieldset class="controls">
        <legend>Controles</legend>
        <asp:Button ID="SubmitBtn" runat="server" CssClass="control-submit control" />
        <input type="reset" value="Reiniciar" class="control-reset control" />
        <a href='<%:url %>' class="control-cancel control">Cancelar</a>
    </fieldset>
    <% } %>
</asp:Content>

