﻿<%@ Page Title="Modificar Cuenta" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="Account.aspx.cs" Inherits="Admin_Account" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2><%=Title %></h2>
    <p>Aquí podrá modificar la contraseña de la cuenta.</p>
    <fieldset>
        <legend>Modificar Cuenta</legend>
        <p><b>Usuario Actual</b>: <asp:TextBox ReadOnly="true" ID="OldUName" runat="server" /> (no se puede modificar).</p>
        <p><label><b>Contraseña Antigua</b>:</label> <asp:TextBox ID="OldPass" TextMode="Password" runat="server" /><br />
            <label><b>Nueva Contraseña</b>: </label> <asp:TextBox ID="NewPass1" runat="server" TextMode="Password" /><br />
            <label><b>Repita Contraseña</b>: </label> <asp:TextBox ID="NewPass2" runat="server" TextMode="Password" />
            <asp:CompareValidator ControlToCompare="NewPass2" ControlToValidate="NewPass1" runat="server" ErrorMessage="Las Contraseñas no coinciden." />
            <asp:CustomValidator ID="PasswordValidator" runat="server" />
        </p>
        <!--<p>Email: <asp:TextBox ID="Email" runat="server" /></p>-->
    </fieldset>
    <fieldset>
        <legend>Preferencias</legend>
        <p>
            <b>Resultados por página</b>: <asp:TextBox ToolTip="Cuántos resultados mostrar por página al buscar. Esto afecta a todos los buscadores, tanto en el adminitrador como en el área del cliente" ID="ResultsPPTxt" runat="server" />
        </p>
        <div>
            <b>Rango de páginas</b>: <asp:TextBox ID="PageRange" runat="server" /> <a href="#" class="collapser" style="display: inline; background: none;">¿Qué es esto?</a><br />
            <div class="collapse-panel">
                <p>Este rango sirve para delimitar las páginas que se muestran al paginar los resultados. El valor especificado se suma a la página actual hacia ambos lados.<br />
                Por ejemplo: el valor inicial es 3, por lo que se mostrarán 3 páginas hacia la derecha, 3 hacia la izquierda más la página actual. Si la página actual es, por ejemplo, 5,
                entonces se mostrarán las páginas 2 3 4 5 6 7 8 (un rango entre página actual - rango y página actual + rango).<br />
                Esta opción afecta a todos los paginadores.</p>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Operaciones</legend>
        <asp:UpdatePanel UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:Button ID="FlushCacheBtn" runat="server" Text="Vaciar Cache" ToolTip="Si sospecha que los resultados de búsqueda no son correctos, intente borrando la caché." OnClick="FlushCacheBtn_Click" />
                <asp:Label ID="FlushCacheLbl" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="FlushCacheBtn" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </fieldset>
    <asp:Button ID="ModifyUser" runat="server" Text="Modificar" OnClick="ModifyUser_Click" />
    <div id="Messages" runat="server"></div>
</asp:Content>