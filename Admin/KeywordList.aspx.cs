﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_KeywordList : System.Web.UI.Page
{
    private LinqToSql.FinderSchemaDataContext finder = new LinqToSql.FinderSchemaDataContext();
    private static StringBuilder error = new StringBuilder();
    protected StringBuilder html = new StringBuilder();
    protected static string s = "";
    protected long totalPages = 0;
    private Paginate paginate;
    protected string pagination = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        int page = 1;

        if (Request["page"] != null && Helper.IsNumeric(Request["page"]))
        {
            page = int.Parse(Request["page"]);
        }

        if (GTTPageTxt.Text.Length > 0 && Helper.IsNumeric(GTTPageTxt.Text))
        {
            page = int.Parse(GTTPageTxt.Text);
        }
        else if (GTBPageTxt.Text.Length > 0 && Helper.IsNumeric(GTBPageTxt.Text))
        {
            page = int.Parse(GTBPageTxt.Text);
        }

        if (IsPostBack)
        {
            if (KeywordFilterTxt.Text.Length < 1 && s != "")
            {
                s = "";
                page = 1;
            }
            else if (KeywordFilterTxt.Text.Length > 0)
            {
                s = KeywordFilterTxt.Text;
                page = 1;
            }
        }

        Start(page, s);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (s.Length > 0)
        {
            KeywordFilterTxt.Text = s;
        }

        if (error.Length > 0)
        {
            Messages.InnerHtml = error.ToString();
            error.Clear();
        }

        if (paginate != null)
            GTBPageTxt.Text = GTTPageTxt.Text = paginate.CurrentPage.ToString();
        else
            GTBPageTxt.Text = GTTPageTxt.Text = "1";
    }

    private void Start(int currentPage, string search = "")
    {
        List<LinqToSql.Keyword> list = null;

        if (search.Length > 0 && search != "*")
        {
            list = (from a in finder.Keywords where (a.KeywordValue).Contains(search) orderby a.KeywordValue select a).ToList();
        }
        else
        {
            list = (from a in finder.Keywords orderby a.KeywordValue select a).ToList();
        }

        paginate = new Paginate(list.Count, currentPage);
        list = paginate.LimitResults(list);
        pagination = paginate.Pagination;
        totalPages = paginate.MaxPages;
        ParseList(list);
    }

    private void ParseList(List<LinqToSql.Keyword> list)
    {
        string url = "/Admin/KeywordEditor.aspx";
        string newControl = Helper.Controls(url, 0, Request.Url, EditControls.New);
        html.Append(newControl);

        if (list != null && list.Count > 0)
        {
            html.Append("<table class='edition-table'>");
            html.Append("<thead><tr><th>Palabra</th><th title='Muestra una lista con los artículos asociados a este término'>Articulos Enlazados</th><th>Acciones</th></tr></thead>");
            html.Append("<tbody>");

            foreach (LinqToSql.Keyword a in list)
            {
                html.Append("<tr>");
                html.AppendFormat("<td>{0}</td><td>", a.KeywordValue);
                html.Append(BuildArticleAssocList(a.ArticleKeywords));
                html.AppendFormat("</td><td class='edition-table-controls'>{0}</td>", Helper.Controls(url, a.KeywordID, Request.Url, (EditControls.Edit | EditControls.Delete)));
                html.Append("</tr>");
            }

            html.Append("</tbody>");
            html.Append("</table>");
            html.Append(newControl);
        }
        else
        {
            html.Append("<p>No hay resultados...</p>");
        }
    }

    private string BuildArticleAssocList(EntitySet<LinqToSql.ArticleKeyword> list)
    {
        StringBuilder output = new StringBuilder();
        
        if (list.Count > 0)
        {
            string url = HttpUtility.HtmlEncode(Request.Url.ToString());
            output.Append("<a href='#' class='article-list'>Ver Lista</a><div class='hidden'><ul>");

            foreach (LinqToSql.ArticleKeyword ak in list)
            {
                string title = string.Format("'{0}' del volumen {1}, año {2} - Haga clic aquí para editar el artículo", ak.Article.ArticleTitle, ak.Article.Magazine.MagazineNumber, ak.Article.Magazine.PublishYear);
                output.AppendFormat("<li><a href='/Admin/EditArticle.aspx?action=edit&id={0}redirect={1}' title='{2}'>Archivo: {3}</a></li>", ak.ArticleID, url, title, ak.Article.ArticleFile);
            }

            output.Append("</ul></div>");
        }
        else
        {
            output.Append("No tiene");
        }

        return output.ToString();
    }
}