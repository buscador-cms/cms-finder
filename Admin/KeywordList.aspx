﻿<%@ Page Title="Lista de Palabas Clave" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="KeywordList.aspx.cs" Inherits="Admin_KeywordList" %>

<asp:Content ContentPlaceHolderID="HeadScripts" runat="server">
    <script>
        jQuery(document).ready(function ($) {
            var ca = new CustomAlert();

            $('a.article-list').click(function (e) {
                var $cont = $(this).next().html();
                ca.show($cont, 'Artículos Asociados', ['close']);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="KeywordList" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2><%= Title %></h2>
    <div id="keyword-filter">
        Buscar: <asp:TextBox ID="KeywordFilterTxt" runat="server" ToolTip="Ingrese la palabra que busca"></asp:TextBox><asp:Button ID="AuthorFilterBtn" runat="server" Text="Filtrar" />
    </div>
    <h5>Mensajes del Servidor</h5>
    <div id="Messages" runat="server"></div><!-- #Messages -->
    <% if(totalPages > 10)
       { %>
    <div class="go-to-page">
        <p>Ir a la página <asp:TextBox ID="GTTPageTxt" runat="server" CssClass="number-input" /><asp:Button ID="GTTPageBtn" runat="server" Text="Ir" /> de <%= totalPages %></p>
    </div>
    <% } %>
    <%= pagination %>
    <%= html %>
    <%= pagination %>
    <% if(totalPages > 10)
       { %>
    <div class="go-to-page">
        <p>Ir a la página <asp:TextBox ID="GTBPageTxt" runat="server" CssClass="number-input" /><asp:Button ID="GTBPageBtn" runat="server" Text="Ir" /> de <%= totalPages %></p>
    </div>
    <% } %>
</asp:Content>