﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

public partial class Admin_VolumeEditor : System.Web.UI.Page
{
    private static long id = 0;
    private LinqToSql.FinderSchemaDataContext finder;
    private LinqToSql.Magazine mag;
    protected static string action;
    protected static string url;
    private StringBuilder error = new StringBuilder();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            ResetProperties();

            if (action == "" && !IsPostBack)
                ParseParams();

            if (finder == null)
            {
                finder = new LinqToSql.FinderSchemaDataContext();
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error <b>antes</b> de cargar la página: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (action == "delete")
            {
                Delete();
            }
            else
            {
                AddEvents();
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error <b>al cargar</b> la página: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (action == "edit")
        {
            mag = (from m in finder.Magazines
                   where m.MagazineID == id
                   select m).Single();

            FillFields(mag);
        }

        mag = null;

        if (error.Length > 0)
        {
            Messages.InnerHtml = error.ToString();
            error.Clear();
        }
    }

    private void ParseParams()
    {
        if (Request["action"] != null && Regex.IsMatch(Request["action"], @"^(edit|new|delete)$", RegexOptions.IgnoreCase))
        {
            if (Request["action"].ToLower() != action)
            {
                ResetProperties(true);
                action = Request["action"].ToLower();
            }
        }
        else
        {
            Response.Redirect(url);
        }

        if (Request["redirect"] != null)
        {
            url = Request["redirect"];
        }

        if (action == "edit" || action == "delete")
        {
            if (Request["id"] != null)
            {
                if (!Helper.IsNumeric(Request["id"]))
                {
                    Response.Redirect(url);
                }

                id = long.Parse(Request["id"]);
            }
            else
            {
                Response.Redirect(url);
            }
        }
    }

    private void ResetProperties(bool force = false)
    {
        if (!IsPostBack || force)
        {
            url = "/";
            action = "";
            mag = null;
            id = 0;
        }
    }

    private void AddEvents()
    {
        if (action == "edit")
        {
            SubmitBtn.Text = "Guardar Cambios";
            SubmitBtn.Click += OnUpdate;
            SubmitBtn.Click -= OnCreate;
        }
        else
        {
            Title = "Crear Volumen";
            SubmitBtn.Text = "Listo";
            SubmitBtn.Click += OnCreate;
            SubmitBtn.Click -= OnUpdate;
        }
    }

    private void FillFields(LinqToSql.Magazine mag)
    {
        if (mag != null)
        {
            MagazineName.Text = mag.MagazineName.Trim();
            MagazineNumber.Text = mag.MagazineNumber.ToString();
            PublishYear.Text = mag.PublishYear.ToString();
            string name = mag.MagazineName.Trim();
            name = name.Length > 0 ? string.Format("<br /><span>\"{0}\"</span>", name) : "";
            string fullName = string.Format("Volumen {0} del año {1}{2}", mag.MagazineNumber, mag.PublishYear, name).Trim();
            name = string.Format("Editando '{0}'", fullName);
            Title = Helper.RemoveHtml(name, " ");
            LegendTxt.InnerHtml = name;
        }
    }

    protected void OnCreate(object sender, EventArgs e)
    {
        try
        {
            if (!DBSearch.MagazineExists(int.Parse(PublishYear.Text), long.Parse(MagazineNumber.Text), finder))
            {
                if (ValidateData())
                {
                
                        LinqToSql.Magazine newMag = new LinqToSql.Magazine();
                        newMag.MagazineNumber = long.Parse(MagazineNumber.Text);
                        newMag.PublishYear = int.Parse(PublishYear.Text);
                        newMag.MagazineName = MagazineName.Text.Trim();
                        finder.Magazines.InsertOnSubmit(newMag);
                        finder.SubmitChanges();

                        if (newMag != null && newMag.MagazineID > 0)
                        {
                            error.Append("<p class='success'>Volumen creado sin problemas.</p>");
                            RefreshCache();
                        }
                        else
                        {
                            error.Append("<p class='error'>No se pudo crear el volumen.</p>");
                        }
                }
            }
            else
            {
                error.AppendFormat("<p class='error'>El volumen {0} del año {1} ya existe!</p>", MagazineNumber.Text, PublishYear.Text);
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error intentado crear el volumen: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void OnUpdate(object sender, EventArgs e)
    {
        try
        {
            if (!DBSearch.MagazineExists(int.Parse(PublishYear.Text), long.Parse(MagazineNumber.Text), finder))
            {
                if (ValidateData())
                {
                    if (UpdateVolume())
                    {
                        mag.MagazineNumber = long.Parse(MagazineNumber.Text);
                        mag.PublishYear = int.Parse(PublishYear.Text);
                        mag.MagazineName = MagazineName.Text;
                        finder.SubmitChanges();
                        error.Append("<p class='success'>Cambios Guardados!</p>");
                        RefreshCache();
                    }
                }
            }
            else
            {
                error.AppendFormat("<p class='error'>El volumen {0} del año {1} ya existe!</p>", MagazineNumber.Text, PublishYear.Text);
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error intentado actualizar el volumen: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    private void Delete()
    {
        throw new Exception("Unimplemented method!");
        //mag = (from m in finder.Magazines
        //       where m.MagazineID == id
        //       select m).Single();
    }

    private bool ValidateData()
    {
        string name = MagazineName.Text;
        Regex noInterDots = new Regex(@"[a-záéíóúäëïöüñ\-]\.[a-záéíóúäëïöüñ\-]", RegexOptions.IgnoreCase);

        if (name.Length > 0 && noInterDots.IsMatch(name))
        {
            error.Append("<p class='error'>No utilice puntos entre las letras. Sepárelos por un espacio de ser necesario.</p>");
            return false;
        }

        string magNo = MagazineNumber.Text;

        if (!Helper.IsNumeric(magNo.Trim()))
        {
            error.Append("<p class='error'>- El número de volumen ingresado no es un número.</p>");
            return false;
        }
        else
        {
            long no = long.Parse(magNo);

            if (no <= 0)
            {
                error.Append("<p class='error'>- El número de volumen no es válido. Por favor, ingrese un número mayor a 0.</p>");
                return false;
            }
        }

        string year = PublishYear.Text;

        if (!Helper.IsNumeric(year))
        {
            error.Append("<p class='error'>- El año ingresado no es un número.</p>");
            return false;
        }
        else
        {
            if (year.Length < 4)
            {
                error.Append("<p class='error'>- El año de publicación debe tener al menos 4 cifras.</p>");
                return false;
            }
        }

        return true;
    }

    private bool UpdateVolume()
    {
        mag = (from m in finder.Magazines
               where m.MagazineID == id
               select m).Single();

        if(Helper.Error != null)
            Helper.Error.Clear();

        bool r = true;
        int newYear = int.Parse(PublishYear.Text),
            oldYear = mag.PublishYear;
        long newNumber = long.Parse(MagazineNumber.Text),
            oldNumber = mag.MagazineNumber;

        string uploadFolder = Path.Combine(Request.PhysicalApplicationPath,
                Helper.UPLOAD_PATH);
        string oldFolder = Path.Combine(uploadFolder,
                oldYear.ToString(),
                oldNumber.ToString());
        string newRoot = Path.Combine(uploadFolder,
            newYear.ToString());
        string newFolder = Path.Combine(newRoot,
                newNumber.ToString());

        if (oldFolder != newFolder)
        {
            if (!DBSearch.MagazineExists(newYear, newNumber, finder))
            {
                if (Directory.Exists(oldFolder))
                {
                    string backupPath = Path.Combine(uploadFolder, "tmp");

                    if (!Directory.Exists(backupPath))
                        Directory.CreateDirectory(backupPath);

                    backupPath = Path.Combine(backupPath, oldYear.ToString(), oldNumber.ToString());

                    if (Directory.Exists(backupPath))
                        Directory.Delete(backupPath, true);

                    Directory.CreateDirectory(backupPath);
                    Helper.DirectoryCopy(oldFolder, backupPath, true);
                    bool exists = Directory.Exists(newFolder);

                    if (!exists || (exists && Helper.IsDirectoryEmpty(newFolder)) || (exists && DeleteExistentFolder.Checked))
                    {
                        if (exists)
                        {
                            Directory.Delete(newRoot, true);
                            Directory.CreateDirectory(newRoot);
                        }
                        else
                        {
                            Directory.CreateDirectory(newRoot);
                        }

                        Directory.Move(backupPath, newFolder);

                        if (Helper.IsDirectoryEmpty(newFolder))
                        {
                            error.AppendFormat("<p class='error'>No se pudieron mover los archivos desde <b>{0}</b> a <b>{1}</b>.</p>", oldFolder, newFolder);

                            if (!Directory.Exists(oldFolder))
                                Directory.CreateDirectory(oldFolder);

                            Helper.DirectoryCopy(backupPath, oldFolder, true);
                            r = false;
                        }
                        else
                        {
                            Directory.Delete(oldFolder, true);

                            if (oldNumber != newNumber)
                            {
                                if (!Helper.RenameFiles(newFolder, newNumber.ToString()))
                                {
                                    error.AppendFormat("<p class='error'>Ocurrió un error renombrando los archivos<br />{0}</p>", Helper.FormatErrors());
                                    Directory.Move(newFolder, oldFolder);
                                    r = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        error.Append("<p class='error'>El directorio <i>{0}</i> ya existe.<br />Si desea reemplazar el directorio, entonces marque la casilla <b>Forzar sobre-escritura</b>, pero tenga en cuenta que puede eliminar todo el contenido. Se aconseja realizar una copia de respaldo del directorio <i>{0}</i> para evitar pérdida de información.</p>");
                        r = false;
                    }
                }
                else
                {
                    error.AppendFormat("<p class='warning'>¡El directorio <b>{0}</b> no existe!<br />Esto puede deberse a que no se han asociado artículos a este volumen o que los archivos no se hayan copiado correctamente al momento de crear el/los artículos.</p>", oldFolder);
                }
            }
            else
            {
                error.AppendFormat("<p class='error'>Ya existe un volumen <i>{0}</i> del año <i>{1}</i>.</p>", newNumber, newYear);
                r = false;
            }
        }

        return r;
    }

    protected void CheckFolderBtn_Click(object sender, EventArgs e)
    {
        string year = PublishYear.Text;
        string number = MagazineNumber.Text;
        StringBuilder sb = new StringBuilder();

        if (year.Length > 0 && number.Length > 0)
        {
            string path = Path.Combine(Request.PhysicalApplicationPath,
                Helper.UPLOAD_PATH,
                year,
                number);

            sb.AppendFormat("El directorio <span class='highlight'>{0}</span> ", path);

            if (Directory.Exists(path))
            {
                if (Helper.IsDirectoryEmpty(path))
                {
                    sb.Append("<b><i>esta<i> vacío</b>");
                }
                else
                {
                    sb.Append("<b><u>no esta vacío</u></b>");
                }
            }
            else
            {
                sb.Append("no existe");
            }
        }
        else
        {
            sb.Append("Primero debe ingresar el año y el número");
        }

        sb.Append(".");

        EmptyFolderLbl.Text = sb.ToString();
    }

    private void RefreshCache()
    {
        try
        {
            CacheHelper c = new CacheHelper();
            c.DeleteAllCache(string.Format(CacheHelper.VOLUME_CACHE_FILE, ".*", ".*"));
        }
        catch(Exception e)
        {
            error.AppendFormat("<p class='error'>Error borrando cache: {0}<br />{1}.</p>", e.Message, e.StackTrace);
        }
    }

    protected void PublishYear_TextChanged(object sender, EventArgs e)
    {
        string year = PublishYear.Text,
            number = MagazineNumber.Text;

        if(year.Length > 0 && Helper.IsNumeric(year) &&
            number.Length > 0 && Helper.IsNumeric(number))
        {
            if (DBSearch.MagazineExists(int.Parse(year), long.Parse(number)))
            {
                CheckVolume.Text = string.Format("El volumen {0} del año {1} ya existe!", number, year);
                CheckVolume.CssClass = "error";
            }
            else
            {
                CheckVolume.Text = string.Format("El volumen {0} del año {1} <b>no</b> existe :)", number, year);
                CheckVolume.CssClass = "success";
            }
        }
    }
}