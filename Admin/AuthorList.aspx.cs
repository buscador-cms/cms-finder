﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AuthorList : System.Web.UI.Page
{
    private LinqToSql.FinderSchemaDataContext finder = new LinqToSql.FinderSchemaDataContext();
    private static StringBuilder error = new StringBuilder();
    protected StringBuilder html = new StringBuilder();
    protected static string s = "";
    protected long totalPages = 0;
    private Paginate paginate;
    protected string pagination = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        int page = 1;

        if (Request["page"] != null && Helper.IsNumeric(Request["page"]))
        {
            page = int.Parse(Request["page"]);
        }

        if (GTTPageTxt.Text.Length > 0 && Helper.IsNumeric(GTTPageTxt.Text))
        {
            page = int.Parse(GTTPageTxt.Text);
        }
        else if (GTBPageTxt.Text.Length > 0 && Helper.IsNumeric(GTBPageTxt.Text))
        {
            page = int.Parse(GTBPageTxt.Text);
        }

        if (IsPostBack)
        {
            if (AuthorFilterTxt.Text.Length < 1 && s != "")
            {
                s = "";
                page = 1;
            }
            else if (AuthorFilterTxt.Text.Length > 0)
            {
                s = AuthorFilterTxt.Text;
                page = 1;
            }
        }

        Start(page, s);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (s.Length > 0)
        {
            AuthorFilterTxt.Text = s;
        }

        if (error.Length > 0)
        {
            Messages.InnerHtml = error.ToString();
            error.Clear();
        }

        if (paginate != null)
            GTBPageTxt.Text = GTTPageTxt.Text = paginate.CurrentPage.ToString();
        else
            GTBPageTxt.Text = GTTPageTxt.Text = "1";
    }

    private void Start(int currentPage, string search = "")
    {
        List<LinqToSql.Author> list = null;

        if (search.Length > 0 && search != "*")
        {
            list = (from a in finder.Authors where (a.AuthorFName + " " + a.AuthorMName + " " + a.AuthorName).Contains(search) orderby a.AuthorFName, a.AuthorMName, a.AuthorName select a).ToList();
        }
        else
        {
            list = (from a in finder.Authors orderby a.AuthorFName, a.AuthorMName, a.AuthorName select a).ToList();
        }
        
        paginate = new Paginate(list.Count, currentPage);
        list = paginate.LimitResults(list);
        pagination = paginate.Pagination;
        totalPages = paginate.MaxPages;
        ParseList(list);
    }

    private void ParseList(List<LinqToSql.Author> list)
    {
        string url = "/Admin/AuthorEditor.aspx";
        string newLink = Helper.Controls(url, 0, Request.Url, EditControls.New);
        html.AppendFormat("<div class='controls'>{0}</div>", newLink);

        if (list != null && list.Count > 0)
        {
            html.Append("<table class='edition-table'>");
            html.Append("<thead><tr><th>Nombre</th><th>Apellido Paterno</th><th>Apellido Materno</th><th title='Muestra la lista de artículos asociados con este autor'>Lista de Artículos</th><th>Acciones</th></tr></thead>");
            html.Append("<tbody>");

            foreach (LinqToSql.Author a in list)
            {
                html.Append("<tr>");
                html.AppendFormat("<td>{0}</td><td>{1}</td><td>{2}</td><td>", a.AuthorName, a.AuthorFName, a.AuthorMName);
                html.Append(BuildArticleAssocList(a.ArticleAuthors));
                html.AppendFormat("</td><td class='edition-table-controls'>{0}</td>", Helper.Controls(url, a.AuthorID, Request.Url, (EditControls.Edit | EditControls.Delete)));
                html.Append("</tr>");
            }

            html.Append("</tbody>");
            html.Append("</table>");
            html.AppendFormat("<div class='controls'>{0}</div>", newLink);
        }
        else
        {
            html.Append("<p>No hay resultados...</p>");
        }
    }

    private string BuildArticleAssocList(EntitySet<LinqToSql.ArticleAuthor> list)
    {
        StringBuilder output = new StringBuilder();

        if (list.Count > 0)
        {
            string url = HttpUtility.HtmlEncode(Request.Url.ToString());
            output.Append("<a href='#' class='article-list'>Ver Lista</a><div class='hidden'><ul>");

            foreach (LinqToSql.ArticleAuthor ak in list)
            {
                string title = string.Format("'{0}' del volumen {1}, año {2} - Haga clic aquí para editar el artículo", ak.Article.ArticleTitle, ak.Article.Magazine.MagazineNumber, ak.Article.Magazine.PublishYear);
                output.AppendFormat("<li><a href='/Admin/EditArticle.aspx?action=edit&id={0}redirect={1}' title='{2}'>Archivo: {3}</a></li>", ak.ArticleID, url, title, ak.Article.ArticleFile);
            }

            output.Append("</ul></div>");
        }
        else
        {
            output.Append("No tiene");
        }

        return output.ToString();
    }
}