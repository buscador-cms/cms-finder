﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AuthorEditor : System.Web.UI.Page
{
    private static long id = 0;
    private LinqToSql.FinderSchemaDataContext finder;
    private LinqToSql.Author author;
    protected static string action;
    protected static string url;
    private StringBuilder error = new StringBuilder();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            ResetProperties();

            if (action == "" && !IsPostBack)
                ParseParams();

            if (finder == null)
            {
                finder = new LinqToSql.FinderSchemaDataContext();
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error <b>antes</b> de cargar la página: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (action == "delete")
            {
                Delete();
            }
            else
            {
                AddEvents();
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error <b>al cargar</b> la página: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        author = (from a in finder.Authors
                  where a.AuthorID == id
                  select a).FirstOrDefault();

        if (action == "edit")
        {
            FillFields(author);
        }

        author = null;

        if (error.Length > 0)
        {
            Messages.InnerHtml = error.ToString();
            error.Clear();
        }
    }

    private void ParseParams()
    {
        if (Request["action"] != null && Regex.IsMatch(Request["action"], @"^(edit|new|delete)$", RegexOptions.IgnoreCase))
        {
            if (Request["action"].ToLower() != action)
            {
                ResetProperties(true);
                action = Request["action"].ToLower();
            }
        }
        else
        {
            Response.Redirect(url);
        }

        if (Request["redirect"] != null)
        {
            url = Request["redirect"];
        }

        if (action == "edit" || action == "delete")
        {
            if (Request["id"] != null)
            {
                if (!Helper.IsNumeric(Request["id"]))
                {
                    Response.Redirect(url);
                }

                id = long.Parse(Request["id"]);
            }
            else
            {
                Response.Redirect(url);
            }
        }
    }

    private void ResetProperties(bool force = false)
    {
        if (!IsPostBack || force)
        {
            finder = null;
            action = "";
            url = "/";
            author = null;
        }
    }

    private void AddEvents()
    {
        if (action == "edit")
        {
            SubmitBtn.Text = "Guardar Cambios";
            SubmitBtn.Click += OnUpdate;
            SubmitBtn.Click -= OnCreate;
        }
        else
        {
            Title = "Crear Autor";
            SubmitBtn.Text = "Listo";
            SubmitBtn.Click += OnCreate;
            SubmitBtn.Click -= OnUpdate;
        }
    }

    private void FillFields(LinqToSql.Author author)
    {
        if (author != null)
        {
            AuthorFName.Text = author.AuthorFName.Trim();
            AuthorMName.Text = author.AuthorMName.Trim();
            AuthorName.Text = author.AuthorName.Trim();
            string fullName = string.Format("{0} {1} {2}", author.AuthorFName, author.AuthorMName, author.AuthorName).Trim();
            Title = string.Format("Editando al autor '{0}'", fullName);
        }
    }

    protected void OnCreate(object sender, EventArgs e)
    {
        try
        {
            if (!DBSearch.AuthorExists(AuthorFName.Text, AuthorMName.Text, AuthorName.Text))
            {
                if (ValidateData())
                {
                    LinqToSql.Author newAuthor = new LinqToSql.Author();
                    newAuthor.AuthorFName = AuthorFName.Text.Trim();
                    newAuthor.AuthorMName = AuthorMName.Text.Trim();
                    newAuthor.AuthorName = AuthorName.Text.Trim();
                    finder.Authors.InsertOnSubmit(newAuthor);
                    finder.SubmitChanges();

                    if (newAuthor != null && newAuthor.AuthorID > 0)
                    {
                        error.Append("<p class='success'>Autor creado sin problemas.</p>");
                        RefreshCache();
                    }
                    else
                    {
                        error.Append("<p class='error'>No se pudo crear el autor.</p>");
                    }
                }
            }
            else
            {
                error.Append("<p class='error'>El autor ya existe!</p>");
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error intentado crear el autor: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void OnUpdate(object sender, EventArgs e)
    {
        try
        {
            if (!DBSearch.AuthorExists(AuthorFName.Text, AuthorMName.Text, AuthorName.Text))
            {
                if (ValidateData())
                {
                    author = (from a in finder.Authors
                              where a.AuthorID == id
                              select a).Single();

                    author.AuthorFName = AuthorFName.Text.Trim();
                    author.AuthorMName = AuthorMName.Text.Trim();
                    author.AuthorName = AuthorName.Text.Trim();
                    finder.SubmitChanges();
                    error.Append("<p class='success'>Cambios Guardados!</p>");
                    RefreshCache();
                }
            }
            else
            {
                error.Append("<p class='error'>El autor ya existe!</p>");
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error intentado actualizar el autor: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    private void Delete()
    {
        try
        {
            author = (from a in finder.Authors
                      where a.AuthorID == id
                      select a).Single();

            foreach (LinqToSql.ArticleAuthor aa in author.ArticleAuthors)
            {
                finder.ArticleAuthors.DeleteOnSubmit(aa);
            }

            finder.Authors.DeleteOnSubmit(author);
            finder.SubmitChanges();
            RefreshCache();
            Response.Redirect(url);
        }
        catch (Exception e)
        {
            error.AppendFormat("<p class='error'>No se pudo eliminar el artículo: {0}<br />Trace: {1}<br /><br /><a class='go-back-link' href='{2}'>Volver</a></p>", e.Message, e.StackTrace, url);
        }
    }

    private bool ValidateData()
    {
        string name = AuthorName.Text,
            fname = AuthorFName.Text,
            mname = AuthorMName.Text;

        if (name.Length <= 0 && fname.Length <= 0 && mname.Length <= 0)
        {
            error.Append("<p class='error'>- Por favor, ingrese al menos el apellido paterno.</p>");
            return false;
        }
        else
        {
            if (fname.Length <= 0)
            {
                error.Append("<p class='error'>- Por favor, ingrese un apellido paterno. El <i>nombre</i> y el <i>apellido materno</i> son opcionales.</p>");
                return false;
            }

            Regex noInterDots = new Regex(@"[a-záéíóúäëïöüñ\-]\.[a-záéíóúäëïöüñ\-]", RegexOptions.IgnoreCase);

            if (noInterDots.IsMatch(name) || noInterDots.IsMatch(fname) || noInterDots.IsMatch(mname))
            {
                error.Append("<p class='error'>- Por favor, no utilice puntos entre las letras.</p>");
                return false;
            }
        }

        return true;
    }

    private void RefreshCache()
    {
        try
        {
            CacheHelper c = new CacheHelper();
            c.DeleteAllCache(string.Format(CacheHelper.AUTHOR_CACHE_FILE, "\\d*", "\\w*"));
        }
        catch(Exception e)
        {
            error.AppendFormat("<p class='error'>Error borrando cache: {0}<br />{1}.</p>", e.Message, e.StackTrace);
        }
    }
}