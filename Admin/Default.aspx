﻿<%@ Page Title="Manual de Usuario" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Default" %>

<asp:Content ContentPlaceHolderID="Stylesheets" runat="server">
    <style type="text/css">
        #main-right * {
            font-family: 'Times New Roman', sans-serif;
        }

        #main-right a {
            color: magenta;
        }

        
        #main-right h2 a,
        #main-right h3 a,
        #main-right h4 a,
        #main-right h5 a {
            color: black;
        }

        h2 {
            font-size: 1.4em;
        }

        .index,
        .content-wrap {
            background-color: snow;
            padding: 3px 15px 10px 15px;
        }

        .titles {
            -webkit-padding-start: 0;
        }

        .titles > li {
            background-color: honeydew;
            border: solid black 2px;
            margin: 15px 0;
            padding: 3px 8px 8px 8px;
            list-style-position: inside;
        }
        
            .titles h3,
            .index h3 {
                font-size: 1.3em;
                text-align: left;
            }

            .index h3 {
                text-align: center;
            }

            .titles h4 {
                font-size: 1.2em;
            }

            .titles h5 {
                font-size: 1em;
            }

            .titles ul, .titles ol {
                -webkit-padding-start: 0;
            }

            .titles ul li,
            .titles ol li {
                margin: 1em 0;
            }

                .titles ul ol,
                .titles ol ol {
                    list-style-type: lower-roman;
                }

            .titles li ol,
            .titles li ul {
                margin: 0 0 0 1em;
                list-style-position: inside;
            }

        .volume-label, .year-label {
            font-weight: bold;
        }

        .volume-label {
            color: orangered;
        }

        .year-label {
            color: #b200ff;
        }

        .active {
            background-color: yellow;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="HeadScripts" runat="server">
    <script>
        jQuery(document).ready(function ($) {
            $('.index a, titles .a').click(function (e) {
                var href = $(this).attr('href');
                //$('.active').removeClass('active');
                $(href).addClass('active');
                
                var timeout = setTimeout(function () {
                    $(href).removeClass('active');
                }, 2000);
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Bienvenido</h2>
    <p>Esta es la página principal del <b>administrador</b>, donde podrá aprender <i>cómo funciona</i>.</p>
    <p>El administrador le permite modificar su contraseña, preferencias del sitio web y editar artículos, autores, palabras clave y volumenes. 
        Desde aquí controla todo lo que el usuario verá al momento de utilizar el <a href="/Buscar">buscador</a>.
    </p>
    <div class="index">
        <h3>Índice</h3>
        <ol class="index-list">
            <li><a href="#admin-nav" title="Explicación de la navegación del administrador">¿Cómo Desplazarse por el Administrador?</a></li>
            <li><a href="#server-msgs" title="Cómo distinguir los mensajes que envía el servidor">Mensajes del Servidor</a></li>
            <li><a href="#account" title="Explica cómo utilizar el administrador de su cuenta">Administrar Sitio</a></li>
            <li>
                <a href="#list-page" title="Qué es lo que verá al hacer clic sobre los enlaces de la sección de Administración">Administradores</a>
                <ol>
                    <li><a href="#article-search" title="Aprenda cómo filtrar los resultados de los artículos en el administrador de artículos">¿Cómo Filtrar Artículos?</a></li>
                    <li><a href="#volume-search" title="Explica la sintaxis para filtrar los volumenes en el administrador de volumenes">¿Cómo Filtrar Volumenes?</a></li>
                    <li><a href="#author-search" title="Enseña cómo filtrar los autores en el administrador de los mismos">¿Cómo Filtrar Autores?</a></li>
                    <li><a href="#keyword-search">¿Cómo Filtrar Palabras Clave?</a></li>
                </ol>
            </li>
            <li><a href="#" title="Muestra como funcionan las distintas páginas donde se modifica la información de artículos, autores o volumenes">Página de Edición</a></li>
            <li><a href="#article-edition" title="Aprenda cómo crear o modificar artículos">Administración de Artículos</a>
                <ol>
                    <li><a href="#new-article">Crear Artículos</a></li>
                    <li><a href="#edit-article">Modificar Artículos</a></li>
                    <li><a href="#delete-article">Eliminar Artículos</a></li>
                </ol>
            </li>
            <li><a href="#volume-edition" title="Aprenda cómo crear o modificar volumenes">Administración de Volumenes</a>
                <ol>
                    <li><a href="#new-volume">Crear Volumen</a></li>
                    <li><a href="#edit-volume">Modificar Volumen</a></li>
                    <li><a href="#delete-volume">Eliminar Volumen</a></li>
                </ol>
            </li>
            <li><a href="#author-edition" title="Aprenda cómo crear o modificar autores">Administración de Autores</a>
                <ol>
                    <li><a href="#new-author">Crear Autores</a></li>
                    <li><a href="#edit-author">Modificar Autores</a></li>
                    <li><a href="#delete-author">Eliminar Autores</a></li>
                </ol>
            </li>
            <li><a href="#keyword-edition" title="Aprenda cómo crear o modificar palabras clave">Administración de Palabras Clave</a>
                <ol>
                    <li><a href="#new-keyword">Crear Palabras Clave</a></li>
                    <li><a href="#edit-keyword">Modificar Palabras Clave</a></li>
                    <li><a href="#delete-keyword">Eliminar Palabras Clave</a></li>
                </ol>
            </li>
        </ol>
    </div>
    <div class="content-wrap">
        <ol class="titles">
            <li>
                <h3><a  id="admin-nav">¿Cómo Desplazarse por el Administrador?</a></h3>
                <p>En la <i>barra lateral izquierda</i> podrá ver lo siguiente, comenzando desde arriba:</p>
                <ul>
                    <li>
                        <h4>Bloque de Sesión</h4>
                        <p>Este bloque muestra la información básica de la cuenta que se está utilizando. Lo que ve, desde arriba hacia abajo, es:</p>
                        <ol>
                            <li><b>Mensaje de Bienvenida</b>: muestra el nombre de usuario.</li>
                            <li><b>Salir</b>: le permitirá cerrar la sesión para que nadie (que no tenga la contraseña) puede utilizarla.</li>
                            <li><b>Modificar Cuenta</b>: lo llevará al editor de su cuenta, donde podrá cambiar la contraseña y algunas preferencias del sitio web.</li>
                            <li><b>Manual de Usuario</b>: un enlace a esta página web.</li>
                        </ol>
                    </li>
                    <li>
                        <h4>Bloque de Administración</h4>
                        <p>Aquí están los enlaces para administrar los artículos, autores, palabras clave y volumenes:</p>
                        <ol>
                            <li>
                                <a href="/Admin/AuthorList.aspx"><%:section %> Autores</a>: le permite <i>ver la lista</i> de autores existentes, <i>modificarlos</i>, <i>eliminarlos y/o crear nuevos autores</i>.<br />
                                Para relacionar los autores con un artículo predefinido, debe hacerlo desde la sección <a href="/Admin/ArticleList.aspx"><%:section %> Artículos</a>. Para saber cómo hacerlo, lea la sección <a href="#author-linking">¿Cómo Relacionar Autores</a>
                            </li>
                            <li><a href="/Admin/VolumeList.aspx"><%:section %> Volumenes</a>: lo llevará a lista de volumenes donde podrá ver todos los volumenes existentes, <i>modificar</i> y/o <i>crear</i> nuevos volumenes.</li>
                            <li>
                                <a href="/Admin/KeywordList.aspx"><%:section %> Palabras Claves</a>: aquí podrá <i>ver la lista</i> de palabras clave, <i>modificar</i>, <i>eliminar</i> y/o <i>crear</i> las palabras clave.<br />
                                Para relacionar las palabras clave a un artículo predeterminado, debe hacerlo en la sección <a href="/Admin/ArticleList.aspx"><%:section %> Artículo</a>. Para saber cómo, lea la sección <a href="#keyword-linking">¿Cómo Relacionar Palabras Clave?</a></li>
                            <li><a href="/Admin/ArticleList.aspx"><%:section %> Artículos</a>: podrá <i>ver la lista</i> de artículos, <i>modificar</i>, <i>eliminar</i> y/o <i>crear</i> artículos.</li>
                        </ol>
                    </li>
                </ul>
            </li>
            <li>
                <h3><a id="server-msgs">Mensajes del Servidor</a></h3>
                <p>Cada acción que realice tendrá una respuesta por parte del servidor, la que se diferencia por colores:</p>
                <p class="success">Los mensajes en color verde implican que la acción se realizó sin problemas.</p>
                <p class="warning">El color amarillo representa una <b>alerta</b>, pero no es un error, por lo que no causará problemas en el futuro. Por ejemplo, puede que haya eliminado un artículo pero que el archivo PDF no exista, lo que generará una advertencia de color amarillo.</p>
                <p class="error">Este mensaje, con color rojo, significa que ocurrió un error, el cual podría causar problemas en el futuro si no se corrige de inmediato.</p>
            </li>
            <li>
                <h3><a id="account">Administrar Sitio</a></h3>
                <p>Cuando entra a la sección <a href="/Admin/Account.aspx">Modificar Cuenta</a>, podrá cambiar su contraseña y modificar algunas opciones del buscador (la parte que ve el usuario en la <a href="/">página principal</a>).</p>
                <p>Al ingresar al administrador verá lo siguiente:</p>
                <ol>
                    <li><h4>Operaciones Sobre su Cuenta</h4>
                        <ol>
                            <li><b>Usuario Actual</b>: es el nombre de usuario que se usa para iniciar sesión. Actualmente no se puede modificar.</li> 
                            <li><b>Contraseña antigua / Nueva Contraseña / Repita Contraseña</b>: si desea modificar su contraseña, debe llenar estos campos como corresponde.</li>                       
                        </ol>
                    </li>
                    <li>
                        <h4>Preferencias del Sitio</h4>
                        <ol>
                            <li><b>Resultados por Página</b>: este número respresenta la cantidad de resultados que se muestran por página al realizar un búsqueda. Esta opción afecta las búsquedas en la administración y las búsquedas en la página inicial.</li>
                            <li><b>Rango de Páginas</b>: es la cantidad de páginas que se muestran hacia la derecha e izquierda en el paginador. Por ejemplo, si ingresa el número <b>5</b>, entonces se mostrarán 5 páginas hacia la derecha y 5 hacia la izquierda de la página actual, siempre y cuando la página actual + el rango de páginas sea mayor a 0 y menor al total de páginas.</li>
                        </ol>
                    </li>
                    <li>
                        <h4>Operaciones</h4>
                        <ol>
                            <li><b>Vaciar Caché</b>: este botón permite limpiar los resultados guardados. Los resultados del buscador en la página de inicio (y sólo en esta) son guardados para acelerar el procesdo de carga de contenido.
                                La caché se limpia automáticamente cuando se modifica, elimina o crea un autor, artículo, volumen o palabra clave. Sin embargo, si cree que en el <a href="/">buscador</a> no se ven reflejados los cambios que ha hecho, 
                                este botón puede ayudar a resolver el problema. Si el problema persiste, también puede intentar presionando la combinación de teclas <b>Control</b> + <b>F5</b> para recargar la página (esto limpia la caché del navegador, que es independiente
                                de la caché del sitio web).
                            </li>
                        </ol>
                    </li>
                </ol>
            </li>
            <li>
                <h3><a id="list-page">Administradores</a></h3>
                <p>Al hacer clic sobre <i><%:section %> Volumenes</i>, <i><%:section %> Artículos</i>, <i><%:section %> Autores</i> o <i><%:section %> Palabras Claves</i>, aparecerá una lista de lo que haya seleccionado (lista de artículos, volumenes, palabras clave o autores).</p>
                <p>Todas las <u>listas</u> son similares, pues los controles son iguales en casi todas. Los elementos que tienen en común, viendo la página desde arriba hacia abajo, son:</p>
                <ol>
                    <li>Título</li>
                    <li>Campo de Búsqueda: le permitirá reducir la cantidad de resultados mostrados.</li>
                    <li>Paginador: los números de página. Sólo aparecerá si hay 4 páginas o más.</li>
                    <li>Botón <b>Nuevo</b>: le permitirá crear otro elemento (artículo, volumen, autor o palabra clave)</li>
                    <li>
                        <p>Lo que aparece ahora dependerá de la lista:</p>
                        <ul>
                            <li>
                                <p>
                                    Para las listas de autor, volumen y palabra clave, verá una tabla con una cantidad variable de columnas, pero hay una columna que siempre existirá: <b>Acciones</b>.
                                </p>
                                <p>En la columna <b>Acciones</b> habrán 2 controles: <b>Borrar</b> y <b>Modificar</b>. <b>Sin embargo</b>, en la lista de volumenes, el botón <b>Borrar</b> no aparecerá, pues <b>no se permite eliminar un volumen</b> (sólo se pueden modificar)</p>
                            </li>
                            <li>
                                <p>
                                    Para la lista de artículos, la tabla es diferente. La tabla presenta lo siguiente (viendo la tabla desde arriba hacia abajo):
                                </p>
                                <ul>
                                    <li>Título: en la franja de color negro con letras de color blanco</li>
                                    <li>
                                        <p>En el lado izquierdo:</p>
                                        <ul>
                                            <li>Número: representa el número al que pertenece este artículo, el cual es <b>independiente del número de volumen</b> al que pertenece.</li>
                                            <li>Archivo PDF: un enlace hacia el archivo actual del artículo.</li>
                                            <li>Volumen al que pertenece.</li>
                                        </ul>
                                        <p>A la derecha de la tabla verá:</p>
                                        <ul>
                                            <li>Otra información: muestra los autores (si es que hay) y la descripción.</li>
                                            <li>Páginas: la cantidad de páginas que tiene el artículo.</li>
                                        </ul>
                                    </li>
                                    <li>
                                        <p>La sección inferior de la tabla muestra los controles. Al lado izquierdo encontrará el botón <b>Modificar</b>, que le permitirá editar el artículo, y a la derecha verá el botón <b>Borrar</b>, para eliminar el artículo correspondiente.</p>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>Botón <b>Nuevo</b>: aparece también aquí para que no tenga que volver al comienzo de la página.</li>
                    <li>Paginador: también aparece dos veces (por comodidad para el usuario).</li>
                </ol>
                <div>
                    <h4><a id="article-search">¿Cómo Filtrar la Lista de Artículos?</a></h4>
                    <p>El buscador de artículos tiene varias formas de buscar:</p>
                    <ul>
                        <li>Por el nombre del archivo PDF,</li>
                        <li>Título del artículo o</li>
                        <li>El número o el año del volumen al que pertenece.</li>
                    </ul>
                    <p>Las primeras dos opciones no son complejas, pues basta con escribir lo que busca. Sin embargo, la búsqueda por volumen utiliza una sintaxis especial.</p>
                    <h5>Filtrar en Base al Año de Publicación</h5>
                    <p>Para filtrar los artículos en base al <b>año de publicación</b> del volumen, debe escribir la letra <b>y</b> (i griega) seguida por el año de publicación (que debe ser una cifra de 4 números).</p>
                    <p><i><u>Ejemplo</u>: quiere encontrar los artículos de todos los volumenes del año 1990; para encontrarlo, escriba <b>y1990</b> en el cuadro <b>Buscar</b></i>.</p>
                    <h5>Filtrar en Base al Número de Volumen</h5>
                    <p>Similar al año de publicación, debe escribir la letra <b>v</b> seguida por el <b>número de volumen</b> que desea.</p>
                    <p><i><u>Ejemplo</u>: quiere encontrar los artículos de todos los volumenes que hay en el volumen 30; para encontrarlos, escriba <b>v30</b> en cuadro <b>Buscar</b></i>.</p>
                    <h5>Filtrar en Base al Número y Año del Volumen</h5>
                    <p>Esta opción es una combinación de las primeras dos. Debe escribir <b>yAÑO vNÚMERO</b>, donde AÑO es el año de publicación y NÚMERO el número del volumen.</p>
                    <p><i><u>Ejemplo</u>: para encontrar los artículos del volumen 10 del año 1998, debería escribir <b>y1998 v10</b></i>.</p>
                    <p><i><u>Ejemplo</u>: para encontrar los artículos del volumen 30 del año 2000, podría escribir <b>v30 y2000</b></i>.</p>
                    <p>El orden en que aparecen las funciones ni el espacio son importantes, vale decir, los siguientes ejemplos son todos válidos si buscaramos el <i>volumen 30 del año 1990</i>:</p>
                    <ul>
                        <li><span class="volume-label">v30</span><span class="year-label">y1990</span></li>
                        <li><span class="volume-label">v30</span> <span class="year-label">y1990</span></li>
                        <li><span class="year-label">y1990</span><span class="volume-label">v30</span></li>
                        <li><span class="year-label">y1990</span> <span class="volume-label">v30</span></li>
                    </ul>
                </div>
                <div>
                    <h4><a id="volume-search">¿Cómo Filtrar la Lista de Volumenes?</a></h4>
                    <p>Para filtrar los volumenes, basta con escribir el número del volumen o el año de publicación.</p>
                    <p><i><u>Ejemplo</u>: para buscar el volumen número 30, escriba <b>30</b> en cuadro <b>Buscar</b>.</i></p>
                    <p><i><u>Ejemplo</u>: para buscar todos los volumenes del año 2000, escriba <b>2000</b> en cuadro <b>Buscar</b>.</i></p>
                </div>
                <div>
                    <h4><a id="author-search">¿Cómo Filtrar la Lista de Autores?</a></h4>
                    <p>Escriba el nombre, apellido paterno, apellido materno o todos los anteriores en el cuadro <b>Buscar</b>.</p>
                    <p><i><u>Ejemplo</u>: para buscar a Arturo Lopez, escriba <b>Arturo Lopez</b> en el cuadro <b>Buscar</b></i>.</p>
                </div>
                <div>
                    <h4><a id="keyword-search">¿Cómo Filtrar la Lista de Palabras Clave?</a></h4>
                    <p>Basta con escribir la palabra clave que busca o parte de esta.</p>
                </div>
            </li>
            <li>
                <h3><a id="editor-page">Página de Edición</a></h3>
                <p>Las páginas de edición, a las que se accede haciendo clic sobre el botón <b>Modificar</b> del <a href="#list-page">Administrador</a> correspondiente, también son similares. Cada <i>editor</i> se compone de:</p>
                <ol>
                    <li><b>Título de la página</b></li>
                    <li><b>Enlace para volver</b>: este enlace lo devolverá a la página anterior.</li>
                    <li><b>Mensajes del servidor</b>: son las respuestas que enviará el servidor después de guardar los cambios. Es posible que no esté visible inicialmente.</li>
                    <li><b>Campos de llenado</b>: la información necesaria para crear o modificar el elemento (artículo, volumen, palabra clave o autor).</li>
                    <li>
                        <b>Controles</b>: le permitirán realizar las acciones. Los botones son:
                        <ul>
                            <li><i>Guardar</i>: este botón puede llamarse <b>Listo</b> o <b>Guardar Cambios</b>.</li>
                            <li><i>Reiniciar</i>: reemplaza los valores modificados por los originales.</li>
                            <li><i>Cancelar</i>: vuelve a la página anterior descartando todos los cambios hechos.</li>
                        </ul>
                    </li>
                </ol>
            </li>
            <li>
                <h3><a id="article-edition">Administrar Artículos</a></h3>
                <p>Cada volumen se compone por uno o más artículos, por ende, se podría decir los artículos son la base. Puesto que cada artículo fue divido en varios archivos, cada una de estas lleva asociado un número de artículo, independiente del número de volumen al que pertenece.</p>
                <p>Para crear un artículo, primero debe asegurarse de que el volumen al que pertencerá el artículo existe, de lo contrario no podrá crearlo (a no ser de que lo asigne a un volumen existente). Tomando esto en cuenta, los pasos son los siguientes:</p>
                <ol>
                    <li>Crear un volumen.</li>
                    <li>Opcionalmente, crear el o los autor/es que desea asociar a este artículo.</li>
                    <li>Crear el artículo.</li>
                </ol>
                <p>Una vez que los pre-requisitos se cumplen, debe hacer lo siguiente:</p>
                <h4><a id="new-article">Crear un Artículo</a></h4>
                <ol>
                    <li>Diríjase al <a href="/Admin/ArticleList.aspx">Administrador de Artículos</a>.</li>
                    <li>Haga clic sobre el botón <b>Nuevo</b> (véase <a href="#list-page">Administradores</a>).</li>
                    <li><a id="article-required-info">Llene la información solicitada</a>:
                        <ol>
                            <li><i>Número</i> al que pertenece el artículo (obligatorio): ingrese sólo números.</li>
                            <li><i>Título del artículo</i> (opcional): si bien es opcional, tenga en cuenta que, si no se especifica, este artículo no aparecerá al realizar una búsqueda en la <a href="/">página de inicio</a>.</li>
                            <li><i>Archivo PDF</i> (obligatorio): seleccione el archivo que desea utilizar con este artículo (el nombre no es importante, pues se genera automáticamente, pero debe ser un archivo PDF).</li>
                            <li><i>Descripción</i> (opcional).</li>
                            <li><i>Volumen</i> (obligatorio): seleccione el volumen al que pertencerá el artículo (véase <a href="#volume-edition">¿Cómo crear un volumen?</a>).</li>
                            <li><i>Autores</i> (opcional):
                                <p>Para especificar uno o más autores a este artículo, debe hacer lo siguiente:</p>
                                <ol>
                                    <li>Escriba el nombre (completo o parcial) o, si desea listar los autores que comiencen por una letra en particular, escriba una sola letra.</li>
                                    <li>Haga clic sobre <b>Buscar</b>.</li>
                                    <li>Si hubieron resultados, entonces simplemente haga clic sobre el autor que desee. De no haber resultados, realice otra búsqueda o cree un nuevo autor (véase <a href="#author-edition">¿Cómo crear autores?</a></li>
                                </ol>
                                <p>Para limpiar la lista de resultados, haga clic sobre <b>Limpiar Resultados</b>.</p>
                            </li>
                            <li>
                                <i>Palabras Clave</i> (opcional):
                                <p>Para especificar una o más palabras clave, haga lo siguiente:</p>
                                <ol>
                                    <li>Comience a escribir la palabra que desea asociar (por favor, no utilice signos de puntuación; 
                                        solo utilice letras con tildes, espacios, guiones (-) y/o guiones bajos (_). Además, la palabra clave 
                                        <b>debe</b> comenzar por una letra) y, si existe una palabra ya creada, aparecerá
                                        en el bloque siguiente.
                                    </li>
                                    <li>Si apareció la palabra clave que buscaba, entonces haga clic sobre esta, de lo contrario, haga clic sobre el botón <b>Ok</b>.</li>
                                </ol>
                                <p>Para eliminar una palabra clave, basta con hacer clic sobre la palabra clave dentro del cuadro <b>Palabras Clave Actuales</b>.</p>
                            </li>
                            <li><i>Artículo completo</i> (opcional): es el archivo PDF que no ha sido cortado. Este archivo se sube solo una vez y, de hacerlo más veces, las siguientes reemplazarán al que ya se subió.</li>
                        </ol>
                    </li>
                    <li>Cuando haya terminado, haga clic sobre <b>Guardar Cambios</b>.</li>
                </ol>
                <p>Si todo salió bien, aparecerá uno o más mensajes de <i>color verde</i>, de lo contrario serán de <i>color rojo</i>.</p>
                <h4><a id="edit-article">Modificar un Artículo</a></h4>
                <ol>
                    <li>Diríjase al <a href="/Admin/ArticleList.aspx"><%:section %> Artículos</a>.</li>
                    <li>Busque el artículo que desea modificar y luego haga clic sobre el botón <b>Modificar</b> (véase <a href="#list-page">Administradores</a>) que está en la misma fila.</li>
                    <li><a href="#article-required-info">Llene la información solicitada.</a></li>
                    <li>Cuando haya terminado, haga clic sobre <b>Guardar Cambios</b>. Si se arrepiente, haga clic sobre el botón <b>Cancelar</b> para no guardar los cambios.</li>
                </ol>
                <h4><a id="delete-article">Eliminar un Artículo</a></h4>
                <p class="warning"><b>¡Advertencia!</b> Al eliminar un artículo, también estará eliminando el archivo PDF asociado a este, por lo que se aconseja tener un respaldo.</p>
                <ol>
                    <li>Diríjase al <a href="/Admin/ArticleList.aspx"><%:section %> Artículos</a>.</li>
                    <li>Busque el artículo que desea modificar y luego haga clic sobre el botón <b>Borrar</b> (véase <a href="#list-page">Administradores</a>) que está en la misma fila.</li>
                    <li>Aparecerá una ventana preguntando si desea eliminar el artículo.</li>
                    <li>Si responde que sí, entonces se intentará eliminar el artículo, de lo contrario no se hará nada.</li>
                </ol>
            </li>
            <li>
                <h3><a id="volume-edition">Administrador de Volumenes</a></h3>
                <p>Un volumen es el contenedor de uno o varios artículos. Cada volumen consta de un <i>año de publicación</i> (obligatorio) y un <i>número de volumen</i> (obligatorio) que lo identifican. Opcionalmente, pueden tener un título (existe la opción, pero no se utiliza en ningún lugar actualmente).</p>
                <h4><a id="new-volume">Crear un Volumen</a></h4>
                <ol>
                    <li>Si es que no está en la sección llamada <b>Lista de Volumenes</b>, haga clic sobre el enlace <a href="/Admin/VolumeList.aspx"><%:section %> Volumenes</a>.</li>
                    <li>En esta sección, bajo los números de página (o, de no haber ningún número, bajo el cuadro de búsqueda), haga clic sobre <b>Nuevo</b>.</li>
                    <li><a id="magazine-required-info">Ingrese los datos obligatorios</a>:
                        <ol>
                            <li><b>Número</b> (obligatorio): se refiere al número de volumen. Ingrese sólo números.</li>
                            <li><b>Año de Publicación</b> (obligatorio): este es el año en que se publicó el volumen. Ingrese un dígito de 4 cifras.</li>
                            <li><b>Título</b> (opcional): no se utiliza actualmente.</li>
                        </ol>
                    </li>
                    <li>Cuando haya terminado de definir el volumen, haga clic sobre el botón <b>Listo</b>. Si se arrepiente, haga clic sobre el botón <b>Cancelar</b>.</li>
                </ol>
                <h4><a id="edit-volume">Editar un Volumen</a></h4>
                <ol>
                    <li>Si es que no está en la sección llamada <b>Lista de Volumenes</b>, haga clic sobre el enlace <a href="/Admin/VolumeList.aspx"><%:section %> Volumenes</a>.</li>
                    <li>Busque el volumen que desea editar y haga clic sobre <b>Modificar</b> (véase <a href="#list-page">Administradores</a>) que esta en la misma fila.</li>
                    <li><a href="#magazine-required-info">Ingrese los datos obligatorios</a></li>
                    <li>Cuando haya terminado, haga clic sobre <b>Listo</b>. Si cambia de opinión y quiere descartar los cambios, haga clic sobre <b>Cancelar</b>.</li>
                </ol>
                <h4><a id="delete-volume">Eliminar Volumen</a></h4>
                <p>Para evitar perdída de información, los volumenes <b>no se pueden eliminar</b>. Eliminar un volumen implicaría borrar todos los archivos PDF asociados.</p>
            </li>
            <li>
                <h3><a id="author-edition">Administrar Autores</a></h3>
                <p>Los autores son los creadores de los artículos y cada artículo puede estar relacionado con uno o varios autores.</p>
                <h4><a id="new-author">Crear un Autor</a></h4>
                <ol>
                    <li>Diríjase a la sección <a href="/Admin/AuthorList.aspx"><%:section %> Autores</a>.</li>
                    <li>Haga clic sobre el botón <b>Nuevo</b> (véase <a href="#list-page">Administradores</a>).</li>
                    <li><a id="author-required-info">Ingrese la información necesaria</a>:
                        <ol>
                            <li><b>Apellido Paterno</b> (obligatorio)</li>
                            <li><b>Apellido Materno</b> (opcional)</li>
                            <li><b>Nombre</b> (opcional)</li>
                        </ol>
                        Si el autor que necesita crear es una institución, ingréselo en el campo Apellido Paterno y el resto déjelo en blanco.
                    </li>
                    <li>Si está seguro, haga clic sobre <b>Listo</b>, de lo contrario haga clic sobre <b>Cancelar</b>.</li>
                </ol>
                <h4><a id="edit-author">Modificar un Autor</a></h4>
                <ol>
                    <li>Diríjase a la sección <a href="/Admin/AuthorList.aspx"><%:section %> Autores</a>.</li>
                    <li>Busque el autor que quiere editar y luego haga clic sobre el botón <b>Modificar</b> de la misma fila.</li>
                    <li><a href="#author-required-info">Modifque la información</a>.</li>
                    <li>Cuando haya terminado, presione <b>Listo</b> o presione <b>Cancelar</b> para deshacer los cambios.</li>
                </ol>
                <h4><a id="delete-author">Eliminar un Autor</a></h4>
                <ol>
                    <li>Diríjase a la sección <a href="/Admin/AuthorList.aspx"><%:section %> Autores</a>.</li>
                    <li>Busque el autor que quiere eliminar y luego haga clic sobre el botón <b>Borrar</b> de la misma fila.</li>
                    <li>Aparecerá una ventana preguntando si desea eliminar el autor.</li>
                    <li>Si responde que sí, entonces se intentará eliminar el autor, de lo contrario no se hará nada.</li>
                </ol>
            </li>
            <li>
                <h3><a id="keyword-edition">Administrar Palabras Clave</a></h3>
                <p>
                    Las palabras clave, son términos específicos que se utilizan al realizar una búsqueda. Por ejemplo, un artículo que trata sobre <b>sida</b>, podría asociarse a esta palabra para generalizar una búsqueda.
                    Cada palabra clave puede estar asociada a muchos artículos, por ende, se podría considerar como una <i>categoría</i>.
                </p>
                <h4><a id="new-keyword">Crear una Palabra Clave</a></h4>
                <p>Hay dos formas de crear una palabra clave:</p>
                <ul>
                    <li>Desde el <a href="#new-article">editor de artículos</a>, ya sea al momento de crear un nuevo artículo o al editar uno existente, o</li>
                    <li>Desde el administrador de palabras clave</li>
                </ul>
                <p>Para crear una palabra clave desde el administrador de estas, haga lo siguiente:</p>
                <ol>
                    <li>Diríjase a la sección <a href="/Admin/KeywordList.aspx"><%:section %> Palabras Clave</a>.</li>
                    <li>Haga clic sobre el botón <b>Nuevo</b>.</li>
                    <li id="keyword-required-info">En el <b>Editor de Palabra Clave</b>, ingrese el término que desee (cáncer, por ejemplo).</li>
                    <li>Si esta seguro, haga clic en <b>Listo</b> para guardar los cambios, de lo contrario edite el término o haga clic sobre <b>Cancelar</b> para descartar los cambios.</li>
                </ol>
                <h4><a id="edit-keyword">Modificar una Palabra Clave</a></h4>
                <p>La modificación, a diferencia de la creación de palabas clave, sólo se puede hacer desde el administrador de estas. Para <i>modificar</i> una palabra clave:</p>
                <ol>
                    <li>Diríjase a la sección <a href="/Admin/KeywordList.aspx"><%:section %> Palabras Clave</a>.</li>
                    <li>Busque la palabra clave que desea modificar con el buscador.</li>
                    <li>Haga clic sobre el botón <b>Modificar</b> en la misma fila de la palabra clave.</li>
                    <li>Ingrese la <a href="#keyword-required-info">información solicitada</a>.</li>
                    <li>Si esta seguro, haga clic en <b>Listo</b> para guardar los cambios, de lo contrario edite el término o haga clic sobre <b>Cancelar</b> para descartar los cambios.</li>
                </ol>
                <h4><a href="delete-keyword">Eliminar una Palabra Clave</a></h4>
                <p>Al igual que la modificación, sólo se pueden eliminar palabras clave desde el administrador de estas. Para eliminarlas, haga lo siguiente:</p>
                <ol>
                    <li>Diríjase a la sección <a href="/Admin/KeywordList.aspx"><%:section %> Palabras Clave</a>.</li>
                    <li>Busque la palabra clave que desea modificar con el buscador.</li>
                    <li>Haga clic sobre el botón <b>Borrar</b> de la misma fila.</li>
                    <li>En la ventana de confirmación, haga clic sobre <b>Aceptar</b> para eliminar la palabra clave o, sobre <b>Cancelar</b> si se arrepiente.</li>
                </ol>
            </li>
        </ol>
    </div>
</asp:Content>

