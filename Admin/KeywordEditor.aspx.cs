﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_KeywordEditor : System.Web.UI.Page
{
    private static long id = 0;
    private LinqToSql.FinderSchemaDataContext finder;
    private LinqToSql.Keyword kw;
    protected static string action;
    protected static string url;
    private StringBuilder error = new StringBuilder();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
            ResetProperties();

            if (action == "" && !IsPostBack)
                ParseParams();

            if (finder == null)
            {
                finder = new LinqToSql.FinderSchemaDataContext();
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error <b>antes</b> de cargar la página: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (action == "delete")
            {
                Delete();
            }
            else
            {
                AddEvents();
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error <b>al cargar</b> la página: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (action == "edit")
        {
            kw = (from a in finder.Keywords
                  where a.KeywordID == id
                  select a).FirstOrDefault();
            FillFields(kw);
        }

        kw = null;

        if (error.Length > 0)
        {
            Messages.InnerHtml = error.ToString();
            error.Clear();
        }
    }

    private void ParseParams()
    {
        if (Request["action"] != null && Regex.IsMatch(Request["action"], @"^(edit|new|delete)$", RegexOptions.IgnoreCase))
        {
            if (Request["action"].ToLower() != action)
            {
                ResetProperties(true);
                action = Request["action"].ToLower();
            }
        }
        else
        {
            Response.Redirect(url);
        }

        if (Request["redirect"] != null)
        {
            url = Request["redirect"];
        }

        if (action == "edit" || action == "delete")
        {
            if (Request["id"] != null)
            {
                if (!Helper.IsNumeric(Request["id"]))
                {
                    Response.Redirect(url);
                }

                id = long.Parse(Request["id"]);
            }
            else
            {
                Response.Redirect(url);
            }
        }
    }

    private void ResetProperties(bool force = false)
    {
        if (!IsPostBack || force)
        {
            finder = null;
            action = "";
            url = "/";
            kw = null;
        }
    }

    private void AddEvents()
    {
        if (action == "edit")
        {
            SubmitBtn.Text = "Guardar Cambios";
            SubmitBtn.Click += OnUpdate;
            SubmitBtn.Click -= OnCreate;
        }
        else
        {
            Title = "Crear Palabra Clave";
            SubmitBtn.Text = "Listo";
            SubmitBtn.Click += OnCreate;
            SubmitBtn.Click -= OnUpdate;
        }
    }

    private void FillFields(LinqToSql.Keyword kw)
    {
        if (kw != null)
        {
            KeywordValue.Text = kw.KeywordValue.Trim();
            Title = string.Format("Palabra clave: '{0}'", kw.KeywordValue.Trim());
        }
    }

    protected void OnCreate(object sender, EventArgs e)
    {
        try
        {
            if (!DBSearch.KeywordExists(KeywordValue.Text))
            {
                if (ValidateData())
                {
                    LinqToSql.Keyword nkw = new LinqToSql.Keyword();
                    nkw.KeywordValue = KeywordValue.Text;
                    finder.Keywords.InsertOnSubmit(nkw);
                    finder.SubmitChanges();

                    if (nkw != null && nkw.KeywordID > 0)
                    {
                        error.Append("<p class='success'>Palabra clave guardada!</p>");
                    }
                    else
                    {
                        error.Append("<p class='error'>No se pudo crear la palabra clave.</p>");
                    }
                }
            }
            else
            {
                error.Append("<p class='error'>La palabra clave ingresada ya existe.</p>");
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error intentado crear la palabra clave: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    protected void OnUpdate(object sender, EventArgs e)
    {
        try
        {
            if (!DBSearch.KeywordExists(KeywordValue.Text))
            {
                if (ValidateData())
                {
                    kw = (from a in finder.Keywords
                          where a.KeywordID == id
                          select a).Single();

                    kw.KeywordValue = KeywordValue.Text;
                    finder.SubmitChanges();
                    error.Append("<p class='success'>Cambios Guardados!</p>");
                }
            }
            else
            {
                error.Append("<p class='error'>La palabra clave ingresada ya existe.</p>");
            }
        }
        catch (Exception ex)
        {
            error.AppendFormat("<p class='error'>Ocurrió un error intentado actualizar la palabra clave: {0}<br />{1}</p>", ex.Message, ex.StackTrace);
        }
    }

    private void Delete()
    {
        try
        {
            kw = (from a in finder.Keywords
                  where a.KeywordID == id
                  select a).Single();

            foreach (LinqToSql.ArticleKeyword aa in kw.ArticleKeywords)
            {
                finder.ArticleKeywords.DeleteOnSubmit(aa);
            }

            finder.Keywords.DeleteOnSubmit(kw);
            finder.SubmitChanges();
            Response.Redirect(url);
        }
        catch (Exception e)
        {
            error.AppendFormat("<p class='error'>No se pudo eliminar la palabra clave: {0}<br />Trace: {1}<br /><br /><a class='go-back-link' href='{2}'>Volver</a></p>", e.Message, e.StackTrace, url);
        }
    }

    private bool ValidateData()
    {
        string val = KeywordValue.Text;

        if (!Regex.IsMatch(val, @"^[a-zñáéíóúäëïöü][a-z0-9ñáéíóúäëïöü\-_.\s]{2,}$", RegexOptions.IgnoreCase))
        {
            error.Append("<p class='error'>Por favor, ingrese al menos 3 caracteres. El término <b>debe</b> comenzar por una letra (tildada o no) y sólo puede utilizar letras (con o sin tilde), números, espacios, guiones (-) y guiones bajos (_).</p>");
            return false;
        }

        return true;
    }
}