﻿<%@ Page Title="Lista de Artículos" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="ArticleList.aspx.cs" Inherits="Admin_ArticleList" %>

<asp:Content ID="ArticleListHScripts" ContentPlaceHolderID="HeadScripts" runat="server">
    <script>
        jQuery(document).ready(function ($) {
            var ca = new CustomAlert();

            $('.meta-authors > a').click(function (e) {
                e.preventDefault();
                ca.show($(this).next().find('ul'), "Autores");
            });

            $('.description > a').click(function (e) {
                e.preventDefault();
                var next = $(this).next();
                ca.show($(this).next().text(), "Descripción");
            });
        });
    </script>
</asp:Content>

<asp:Content ID="ArticleList" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2><%= Title %></h2>
    <div id="article-filter">
        Buscar: <asp:TextBox ID="ArticleFilterTxt" runat="server" ToolTip="Ingrese el término que busca. Haga clic sobre ¿Cómo filtrar los resultados? para entender cómo buscar"></asp:TextBox><asp:Button ID="AuthorFilterBtn" runat="server" Text="Filtrar" />
        <a class="collapser align-right" href="#" title="Haga clic aquí para mostrar u ocultar la ayuda">¿Cómo filtrar los resultados?</a>
        <div class="collapse-panel">
            <p>Para mostrar todos los resultados, borre el contenido de la búsqueda o utilice un asterisco (*).</p>
            <p>Puede buscar usando el nombre del archivo PDF (la extensión no es necesaria), el número o el título (si no tiene título, entonces no aparecerá) del artículo o el volumen al que pertenece el artículo que busca.</p>
            <p>Si busca por volumen, entonces debe utilizar un formato especial.</p>
            <ul>
                <li>
                    <h5>Filtrar por número de volumen</h5>
                    <p>Para filtrar usando el número del volumen, escriba <b>v</b> seguido del número de este. Por ejemplo:<br />
                        <b>v11</b> - mostrará todos los volumenes cuyo número sea 11.<br />
                        <b>v30</b> - mostrará todos los volumenes cuyo número sea 30
                    </p>
                </li>
                <li>
                    <h5>Filtrar por año de publicación</h5>
                    <p>Para filtrar usando el año de publicación, escriba <b>y</b> seguido del año que busca. Por ejemplo:<br />
                        <b>y1990</b> - mostrará resultados de todos los volumenes cuyo año de publicación sea 1990.<br />
                        <b>v2010</b> - mostrará resultados de todos los volumenes cuyo año de publicación sea 2010
                    </p>
                </li>
                <li>
                    <h5>Filtrar por año y número</h5>
                    <p>
                        También puede filtrar los volumenes usando una combinación de ambas funciones. Por ejemplo, si desea buscar el volumen 30
                        del año 1990, entonces debe escribir lo siguiente: <b>v30 y1990</b>. El orden en que aparecen los valores no es importante (<i>v11 y1990</i>
                        es lo mismo que <i>y1990 v11</i>) y tampoco el espacio que hay entre estos.
                    </p>
                </li>
            </ul>
            <p><b>Tenga en cuenta que</b> estos filtros sólo permiten números y los caracteres de control <i>v</i> e <i>y</i>, por lo que, una búsqueda como <b>ya1990</b> o <b>va28</b> no será tomada en cuenta (en este caso, se intentará una búsqueda en los títulos de los artículos).</p>
        </div><!-- .collapse-panel -->
    </div>
    <h5>Mensajes del Servidor</h5>
    <div id="Messages" runat="server"></div><!-- #Messages -->
    <% if(totalPages > 10)
       { %>
    <div class="go-to-page">
        <p>Ir a la página <asp:TextBox ID="GTTPageTxt" runat="server" CssClass="number-input" /><asp:Button ID="GTTPageBtn" runat="server" Text="Ir" /> de <%= totalPages %></p>
    </div>
    <% } %>
    <%=pagination %>
    <%=html.ToString() %>
    <%=pagination %>
    <% if(totalPages > 10)
       { %>
    <div class="go-to-page">
        <p>Ir a la página <asp:TextBox ID="GTBPageTxt" runat="server" CssClass="number-input" /><asp:Button ID="GTBPageBtn" runat="server" Text="Ir" /> de <%= totalPages %></p>
    </div>
    <% } %>
</asp:Content>

