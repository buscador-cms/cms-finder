﻿<%@ Page Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="KeywordEditor.aspx.cs" Inherits="Admin_KeywordEditor" %>

<asp:Content ID="KeywordEditor" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Editor de Palabra Clave</h2>
    <a class="go-back-link" href='<%:url %>'>Volver</a>
    <h4>Mensajes del Servidor</h4>
    <div id="Messages" runat="server"></div>
    <% if(action != "delete")
       { %>
    <fieldset>
        <legend><%= Title %></legend>
        <div>
            <asp:Label runat="server" Text="Palabra Clave" />
            <asp:TextBox runat="server" ID="KeywordValue" />
        </div>
    </fieldset>
    <fieldset class="controls">
        <legend>Controles</legend>
        <asp:Button ID="SubmitBtn" runat="server" CssClass="control-submit control" />
        <input type="reset" value="Reiniciar" class="control-reset control" />
        <a href='<%:url %>' class="control-cancel control">Cancelar</a>
    </fieldset>
    <% } %>
</asp:Content>

