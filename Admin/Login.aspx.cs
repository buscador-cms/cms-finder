﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Web.Security;
using System.Security.Principal;

public partial class LoginPage : System.Web.UI.Page
{
    private SqlConnection conn = new SqlConnection();
    private SQLWrapper wrap = new SQLWrapper();
    private SqlCommand cmd;
    private StringBuilder msg = new StringBuilder();

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (User.Identity.IsAuthenticated)

        conn = wrap.Connect();

        if (conn == null)
        {
            Messages.InnerHtml = wrap.getLastError();
        }
        else
        {
            cmd = new SqlCommand();
            cmd.Connection = conn;
        }
    }

    protected void LoginBtn_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                cmd.CommandText = "SELECT * FROM FinderSchema.Account WHERE AccountUser = @User";
                cmd.Parameters.Add("@User", System.Data.SqlDbType.VarChar).Value = LoginUser.Text;
                ValidateLogin(cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow));
            }
            catch (Exception ex)
            {
                Messages.InnerHtml = ex.Message;
            }
        }
    }

    private void ValidateLogin(SqlDataReader reader)
    {
        try
        {
            if (reader.HasRows)
            {
                SessionHelper sh = new SessionHelper();
                reader.Read();
                byte[] salt = (byte[])reader["AccountSalt"];
                byte[] hash = sh.MakeHash(LoginPass.Text, salt);
                byte[] pass = (byte[])reader["AccountPassword"];

                if (sh.CompareHash(pass, hash))
                {
                    UserAccount ua = new UserAccount(
                        reader.GetString(reader.GetOrdinal("AccountUser")),
                        reader.GetString(reader.GetOrdinal("AccountRoles"))
                    );

                    StartSession(ua);
                }
                else
                {
                    Messages.InnerHtml = "Contraseña incorrecta";
                }
            }
            else
            {
                Messages.InnerHtml = "Usuario inexistente.";
            }

            reader.Close();
        }
        catch (Exception ex)
        {
            Messages.InnerHtml = ex.Message;
        }
    }

    private void StartSession(UserAccount ua)
    {
        /*FormsAuthentication.SetAuthCookie(ua.UserName, false);
        string returnURL = Request.QueryString["ReturnUrl"] as string;

        if (returnURL != null)
        {
            Response.Redirect(returnURL);
        }
        else
        {
            Response.Redirect("/");
        }*/

        FormsAuthentication.SetAuthCookie(ua.UserName, KeepOpen.Checked);
        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
            ua.UserName,
            DateTime.Now,
            DateTime.Now.AddMinutes(30),
            KeepOpen.Checked,
            ua.UserRole
        );
        string cookieStr = FormsAuthentication.Encrypt(ticket);
        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieStr);

        if (KeepOpen.Checked)
        {
            cookie.Expires = ticket.Expiration;
        }

        cookie.Path = FormsAuthentication.FormsCookiePath;
        Response.Cookies.Add(cookie);

        string returnURL = Request.QueryString["ReturnUrl"] as string;

        if (returnURL != null)
        {
            Response.Redirect(returnURL);
        }
        else
        {
            Response.Redirect("/");
        }
    }
}