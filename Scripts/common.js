﻿/*
 * This file is loaded on every Finder Page, which means:
 * - Variables declared before the jQuery(document).ready() will be global and can be used
 * everywhere.
 */
var debug = true;

jQuery(document).ready(function ($) {
    l('--- common LOADED ---');

    $().UItoTop({
        easingType: 'easeOutQuart',
        text: 'Subir',
        title: 'Subir'
    });

    $(function () {
        $(document).tooltip();
    });

    $('span.no-js-warning').each(function () {
        $(this).next().show();
        $(this).remove();
    });

    $('body').on('click', '.collapser', function (e) {
        e.preventDefault();
        var $panel = $(this).parent().find('.collapse-panel').first();

        if ($panel.is(':visible')) {
            $panel.hide();
            $(this).addClass('plus-icon16x16').removeClass('minus-icon16x16');
        } else {
            $panel.show();
            $(this).removeClass('plus-icon16x16').addClass('minus-icon16x16');
        }
    });

    var $list = null,
        removed = new Array(),
        tips = {
            selectedOptions: {
                message: "Los elementos seleccionados después de haber sido filtrados, quedarán al final de la lista.",
                shown: false
            }
        };

    $('body').on('keyup', '.search-in-list', onKeyupSearch);

    function onKeyupSearch() {
        var $parent = $(this).parent(),
            $this = $(this);

        if ($list == null || $list == undefined) {
            $list = $parent.find('.list-to-search');
            l('--- LIST LENGTH ' + $list.children().length + ' ---');
        }

        var search = $(this).val();

        if (search.length > 0) {
            $(this).prop('disabled', true);
            $(this).off('keyup', onKeyupSearch);

            var text = "";
            var regex = new RegExp('.*' + search + ".*", "i");

            $list.children().each(function () {
                text = $(this).text();

                if (!regex.test(text)) {
                    popItem($(this));
                }
            });

            $(this).on('keyup', '.search-in-list', onKeyupSearch);
            $(this).prop('disabled', false);
        } else {
            clearRemoved();
        }

        function clearRemoved() {
            if (removed.length > 0) {
                $list.children().each(function () {
                    if ($(this).is(':selected')) {
                        removed.push($(this));
                        $(this).remove();
                    }
                });

                $.each(removed, function (index, $value) {
                    $list.append($value);
                });

                removed = new Array();
                l('### Restored items ' + $list.children().length + ' ###');
            }

            if (!tips.selectedOptions.shown) {
                $('<div class="tmp-message">' + tips.selectedOptions.message + '</div>')
                    .css({
                        position: 'absolute',
                        top: ($this.position().top - 10) + 'px',
                        left: ($this.position().left + $this.outerWidth() + 20) + 'px'
                    })
                    .insertAfter($this)
                    .delay(4000)
                    .fadeOut('slow', function () {
                        $(this).remove();
                    });

                tips.selectedOptions.shown = true;
            }
        }

        function popItem($item) {
            var index = $item.index();
            removed.push($item);
            $item.remove();
        }
    }

    $('body').on('keydown keyup', '.number-input', function (e) {
        var code = e.keyCode;
        l('code: ' + e.keyCode);

        if (code >= 65 && code <= 90 || code > 185) {
            var val = $(this).val();
            $(this).val(val.replace(/[^\d]/g, ''));
        }
    });

    $('body').on('click', '.control-delete', function (e) {
        if (!confirm("¿Realmente desea eliminar el ítem seleccionado?"))
            e.preventDefault();
    });
});

function trim(string, char) {
    var regex = new RegExp('(^\\' + char + '*)|(\\' + char + '$)', 'gmi');
    return string.replace(regex, '');
}

function l(msg) {
    if (debug)
        console.log(msg);
}

function searchInList(element) {
    console.log(element);
}

function getLastPart(path) {
    l('--- GetLastPath(' + path + ') ---');
    var regex = /\\|\//;
    var sep = regex.exec(path);
    var tmp = path.replace(new RegExp(sep + '$', 'g'), '');
    var index = tmp.lastIndexOf(sep) + 1;
    return tmp.substring(index);
}

function CustomAlert() {
    var $ = jQuery;
    this.actions = {
        OK: 1,
        CANCEL: 2,
        CLOSED: 3
    };

    this.action = 0;
    var _this = this;

    if ($('body').find('#ca-wall').length <= 0)
        addHtml($);

    $('#ca-wall #custom-alert .close').on('click', onClose);
    $('#ca-wall #custom-alert .controls .cancel').on('click', onCancel);
    $('#ca-wall #custom-alert .controls .confirm').on('click', onConfirm);

    this.show = function (msg, title, controls) {
        //$('#custom-alert').css({
        //    width: '50%'
        //});

        if (controls != undefined) {
            $('#ca-wall .controls').children().hide();
            var hide = true;

            for (var control in controls) {
                switch (controls[control]) {
                    case 'confirm':
                        $('#ca-wall .controls .confirm').show();
                        hide = false;
                        break;

                    case 'cancel':
                        $('#ca-wall .controls .cancel').show();
                        hide = false;
                        break;

                    case 'close':
                        $('#ca-wall .controls .close').show();
                        hide = false;
                        break;
                }
            }

            if(hide)
                $('#ca-wall .controls').hide();
        }
        else {
            $('#ca-wall .controls').hide();
        }

        if (title != undefined)
            $('#custom-alert .ca-title').text(title);

        $('#custom-alert #ca-content').html(msg);

        if ($('#ca-wall').is(':hidden'))
            $('#ca-wall').show().focus();
    }

    this.close = function () {
        $('#ca-wall').hide();
    }

    function onClose(e) {
        e.preventDefault();
        _this.action = _this.actions.CLOSED;
        $('#ca-wall').trigger('close', { action: _this.action });
        $('#ca-wall').hide();
    }

    function onConfirm(e) {
        e.preventDefault();
        _this.action = _this.actions.CONFIRM;
        $('#ca-wall').hide();
        $('#ca-wall').trigger('confirm', { action: _this.action });
    }

    function onCancel(e) {
        e.preventDefault();
        _this.action = _this.actions.CANCEL;
        $('#ca-wall').hide();
        $('#ca-wall').trigger('cancel', { action: _this.action });
    }

    function addHtml($) {
        $('body').prepend('<div id="ca-wall">' +
            '<div id="custom-alert">' +
                '<h3 class="ca-title"></h3><a href="#" class="close-dialog close button" title="Cierra esta ventana y cancela la acción">X</a>' +
                '<div id="ca-content"></div>' +
                '<div class="controls">' +
                    '<a href="#" class="confirm button" title="Confirmar la acción">Confirmar</a>' +
                    '<a href="#" class="cancel button" title="Cancelar la acción">Cancelar</a>' +
                    '<a href="#" class="close button" title="Cerrar la ventana y cancelar la acción">Cerrar</a>' +
                '</div>' +
            '</div>' +
        '</div>');
    }
}