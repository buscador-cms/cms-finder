﻿jQuery(document).ready(function ($) {
    var part = getLastPart(location.href);

    l('Part: ' + part);
    if (/^p\d+$/gi.test(part)) {
        $('.pagination .page-jump input[type=number]').val(part.replace('p', ''));
    }
    else {
        $('.pagination .page-jump input[type=number]').val(1);
    }

    var url = '/Buscar/Default.aspx/Search';
    var searchBtn = false;

    var searchfor = {
        $title: $('#search-for span').first(),
        $text: $('#search-for span').last(),
        clear: function () {
            this.$title.text('');
            this.$text.text('');
        },
        title: function (string) {
            this.$title.text(string);
        },
        text: function (string) {
            this.$text.text(string);
        }
    };

    var $cflist = $('#filter-list'),
        $fcontrols = $('#filter-controls');

    function resetControls() {
        $('#search-in-files').prop('checked', '').prop('aria-checked', false);
        $('#results').empty();
        searchfor.clear();
    }

    function stripDomain(href) {
        return href.replace(/(https?:\/\/)?.+\.\w+(:\d+)?\//, '');
    }

    // Handles the search button click - otherwise it does not work ¬¬
    $('#MainContent_SearchBtn').click(function (e) {
        e.preventDefault();
        var url = '';

        if ($('#MainContent_SearchInTitles').is(':checked')) {
            url = '/mostrar/titulo/';
        } else if ($('#MainContent_SearchInAuthors').is(':checked')) {
            url = '/mostrar/autor/';
        } else {
            url = '/buscar/termino/';
        }

        url += $('#MainContent_SearchTxt').val() + "/";
        window.location.href = url;
    });

    $('.pagination .page-jump-submit').click(function (e) {
        e.preventDefault();
        var max = parseInt($(this).parent().find('span').text()),
            val = parseInt($(this).parent().find('input[type=number]').val());

        l('Part: ' + part);
        l('Val: ' + val);
        l('Max value: ' + max);
        l(/^p\d+$/gi.test(part));

        if (/^\d+$/gi.test(val) && val >= 1 && val <= max) {
            if (/^p\d+$/gi.test(part))
                location.href = location.href.replace(part, "p" + val);
            else
                location.href += "p" + val + "/";
        } else {
            l('The page entered was not a number in the required range!');
        }
    });
});