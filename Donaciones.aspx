﻿<%@ Page Title="Donaciones" Language="C#" MasterPageFile="~/Finder.master" AutoEventWireup="true" CodeFile="Donaciones.aspx.cs" Inherits="Donaciones" %>

<asp:Content ContentPlaceHolderID="Meta" runat="server">
    <meta name="description" content="Donaciones para Cuadernos Médico Sociales" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Stylesheets" runat="server">
    <style>
        #content {
            min-height: 500px;
            background-color: #5d94ef;
            padding-top: 3em;
            text-align: center;
        }

        #content h2 {
            color: white;
            text-transform: uppercase;
            margin-bottom: 1em;
        }

        #main-left, #main-right { float: none; }
        #main-right { float: none; width: 100%; }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Sidebar" runat="server"></asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" Runat="Server">
    <div id="content">
        <h2><%= Title %></h2>
        <p>Colegio Médico de Chile A.G</>
        <p>RUT 82.621.700-6</p>
        <p>Cuenta corriente Banco Itau N° 1200026315.</p>
    </div>
</asp:Content>